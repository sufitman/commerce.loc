<?php

namespace app\models\user;

/**
 * @property int role_id
 */
class Admin extends BaseUser
{
    public function beforeValidate()
    {
        $this->role_id = parent::ROLE_ADMIN;

        return parent::beforeValidate();
    }
}
