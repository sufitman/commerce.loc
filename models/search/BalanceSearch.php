<?php

namespace app\models\search;

use app\models\Expense;
use app\models\Incoming;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;

/**
 * IncomingSearch represents the model behind the search form about `app\models\Incoming`.
 */
class BalanceSearch extends Incoming
{
    /**
     * @var $operation_id
     * has to be in result for actions
     */
    public $operation_id;

    /**
     * @var $from_bill
     * has to be in table after Expense join
     */
    public $from_bill;

    /**
     * @var $theBill
     * created to output $from_bill or $to_bill Bill title
     */
    public $theBill;

    /**
     * @var $category_id
     * has to be in table after Expense join
     */
    public $category_id;

    /**
     * @var $category_title
     * created to output $category_id Category title
     */
    public $category_title;

    /**
     * @var $user_name
     * created to output $createdBy full name
     */
    public $user_name;

    /**
     * @var $project_title
     * created to output $project_id Project title
     */
    public $project_title;

    /**
     * @var $balance_sum
     * created to output $sum depending on whether income it is or expense
     */
    public $balance_sum;

    /**
     * @var $type
     * created to output whether income it is or expense
     */
    public $type;

    public $min_sum;

    public $max_sum;

    public $start_time;

    public $end_time;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'balance_sum',
                'min_sum',
                'max_sum',
            ], 'integer'],
            [[
                'description',
                'type',
                'theBill',
                'datetime',
                'user_name',
                'project_title',
                'category_title',
                'balance_sum',
                'min_sum',
                'max_sum',
                'start_time',
                'end_time',
            ], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return parent::attributeLabels() + [
            'project_title'  => 'Проект',
            'user_name'      => 'Оформил',
            'theBill'        => 'Cчет',
            'category_title' => 'Категория (Только для расходов)',
            'balance_sum'    => 'Сумма',
            'type'           => 'Тип',
            'min_sum'        => 'Сумма от',
            'max_sum'        => 'Сумма до',
            'start_time'     => 'Начиная с...',
            'end_time'       => 'Заканчивая...',
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$is_employee = false)
    {
        $related_to = !$is_employee
            ? [
                'or',
                ['created_by' => Yii::$app->user->id],
                ['incoming.company_id' => Yii::$app->user->id],
            ]
            : ['created_by' => Yii::$app->user->id];
        $incomes = Incoming::find()
            ->select([new Expression(Incoming::tableName() . '.id AS operation_id,
                 project.title AS project_title, 
                 CONCAT(user.first_name, " ", user.second_name) as user_name,
                 sum AS balance_sum, 
                 incoming.description, 
                 datetime,
                 category.title AS category_title,
                 bill.title AS theBill,
                 comment,"+" AS type')])
                ->where([
                    'and',
                    $related_to,
                    ['is_balance' => false]
                ])
                ->joinWith(['project','createdBy','theBill','category']);

        $related_to = !$is_employee
            ? [
                'or',
                ['created_by' => Yii::$app->user->id],
                ['expense.company_id' => Yii::$app->user->id],
            ]
            : ['created_by' => Yii::$app->user->id];
        $expenses = Expense::find()
            ->select(new Expression(Expense::tableName() . '.id AS operation_id,
                project.title AS project_title, 
                CONCAT(user.first_name, " ", user.second_name) as user_name,
                sum AS balance_sum, 
                expense.description,
                datetime,
                category.title AS category_title,
                bill.title AS theBill, 
                "-" AS comment, 
                "-" AS type'))
            ->where($related_to)
            ->joinWith(['project','createdBy','theBill','category'])
            ->union($incomes,true);

        $query = new Query();
        $query->select([
                'operation_id',
                'project_title',
                'user_name',
                'theBill',
                'category_title',
                'balance_sum',
                'description',
                'datetime',
                'comment',
                'type',
            ])
            ->from(['balance' => $expenses]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        Yii::$container->set(\yii\data\Sort::className(),[
            'defaultOrder' => [
                'datetime' => SORT_DESC,
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'project_title',
                'user_name',
                'category_title',
                'balance_sum',
                'datetime',
                'theBill',
                'description',
                'type',
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'createdBy'     => $this->createdBy,
            'theBill'       => $this->theBill,
            'type'          => $this->type,
        ]);


        $query->andFilterWhere(['in', 'project_title', $this->project_title])
              ->andFilterWhere(['in', 'user_name', $this->user_name])
              ->andFilterWhere(['in', 'category_title', $this->category_title   ])
              ->andFilterWhere(['like', 'description', $this->description])
              ->andFilterWhere(['like', 'comment', $this->comment])
              ->andFilterWhere(['>=', 'balance_sum', $this->min_sum])
              ->andFilterWhere(['<=', 'balance_sum', $this->max_sum])
              ->andFilterWhere(['>=', 'datetime', $this->start_time])
              ->andFilterWhere(['<=', 'datetime', $this->end_time]);

        return $dataProvider;
    }

}
