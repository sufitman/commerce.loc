<?php

namespace app\controllers\company;

use app\controllers\SiteController;
use app\models\Bill;
use app\models\Category;
use app\models\Project;
use app\models\search\BalanceSearch;
use app\models\search\FundsBalanceSearch;
use app\models\search\ProjectBalanceSearch;
use app\models\search\ReserveBalanceSearch;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use app\models\user\BaseUser;

class StatisticsController extends SiteController
{
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'         => true,
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isCompany();
                        },
                    ],
                    [
                        'allow'         => true,
                        'actions'       => [
                            'balance',
                            'reserve-balance',
                            'funds',
                            'projects',
                        ],
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isCompany() ||  $identity->isAdmin();
                        },
                    ],
                    [
                        'allow'   => true,
                        'actions' => [],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }
    public function actionBalance()
    {
        $searchModel = new BalanceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $uid = Yii::$app->user->id;

        $users = ArrayHelper::getColumn(BaseUser::find()
            ->select(['id','full_name'  => 'CONCAT(first_name, " ", second_name)'])
            ->where(['or',['company_id' => $uid],['id' => $uid]])
            ->asArray(true)
            ->indexBy('full_name')
            ->all(),'full_name');

        $projects = ArrayHelper::getColumn(Project::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('title')
            ->all(),'title');

        $bills = ArrayHelper::getColumn(Bill::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('title')
            ->all(),'title');

        $categories = ArrayHelper::getColumn(Category::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('title')
            ->all(),'title');

        $show_all = Yii::$app->request->get('show_all',false);
        if($show_all) {
            $dataProvider->pagination->pageSize=0;
        }

        return $this->render('balance/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'users' => $users,
            'projects' => $projects,
            'categories' => $categories,
            'bills' => $bills,
            'show_all' => $show_all,
        ]);
    }

    public function actionReserveBalance()
    {
        $searchModel = new ReserveBalanceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $uid = Yii::$app->user->id;

        $users = ArrayHelper::getColumn(BaseUser::find()
            ->select(['id','full_name'  => 'CONCAT(first_name, " ", second_name)'])
            ->where(['or',['company_id' => $uid],['id' => $uid]])
            ->asArray(true)
            ->indexBy('full_name')
            ->all(),'full_name');

        $projects = ArrayHelper::getColumn(Project::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('title')
            ->all(),'title');

        $bills = ArrayHelper::getColumn(Bill::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('title')
            ->all(),'title');

        $categories = ArrayHelper::getColumn(Category::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('title')
            ->all(),'title');

        $show_all = Yii::$app->request->get('show_all',false);
        if($show_all) {
            $dataProvider->pagination->pageSize=0;
        }

        return $this->render('reserve-balance/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'users' => $users,
            'projects' => $projects,
            'categories' => $categories,
            'bills' => $bills,
            'show_all' => $show_all,
        ]);
    }

    public function actionFunds()
    {
        $searchModel = new FundsBalanceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $uid = Yii::$app->user->id;

        $users = ArrayHelper::getColumn(BaseUser::find()
            ->select(['id','full_name'  => 'CONCAT(first_name, " ", second_name)'])
            ->where(['or',['company_id' => $uid],['id' => $uid]])
            ->asArray(true)
            ->indexBy('full_name')
            ->all(),'full_name');

        $projects = ArrayHelper::getColumn(Project::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('title')
            ->all(),'title');

        $bills = ArrayHelper::getColumn(Bill::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('title')
            ->all(),'title');

        $categories = ArrayHelper::getColumn(Category::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('title')
            ->all(),'title');

        $show_all = Yii::$app->request->get('show_all',false);
        if($show_all) {
            $dataProvider->pagination->pageSize=0;
        }

        return $this->render('funds/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'users' => $users,
            'projects' => $projects,
            'categories' => $categories,
            'bills' => $bills,
            'show_all' => $show_all,
        ]);
    }

    public function actionProjects()
    {
        $searchModel = new ProjectBalanceSearch();
        $params = Yii::$app->request->queryParams;
        $dataProvider = $searchModel
            ->search($params);

        $show_all = Yii::$app->request->get('show_all',false);

        return $this->render('projects/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'show_all' => $show_all,
        ]);
    }
}