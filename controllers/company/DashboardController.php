<?php

namespace app\controllers\company;

use app\controllers\SiteController;

use app\models\Bill;
use app\models\Project;
use app\models\Summary;
use Yii;
use yii\filters\AccessControl;
use app\models\user\BaseUser;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class DashboardController extends SiteController
{
    /** @inheritdoc */
    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'         => true,
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isCompany();
                        },
                    ],
                ],
            ],
        ];
    }

    public function actionGetBillsBalances()
    {
        if(Yii::$app->request->isAjax){
            $this->layout = 'ajax';
            $bills_balances = [];
            $uid = Yii::$app->user->id;
            $bills = ArrayHelper::getColumn(Bill::find()
                ->select(['id','title'])
                ->where(['and',['company_id' => $uid],['deleted' => false]])
                ->indexBy('id')
                ->asArray(true)
                ->all(),'title');

            $bills_transference = Summary::getBillsTransferences($bills);

            foreach($bills as $bill_id => $title){
                $bill_incomes = Summary::getTotalIncomes($uid,$bill_id,false,false,true);
                $bill_expenses = Summary::getTotalExpenses($uid,$bill_id);
                $bills_balances[$title] = $bill_incomes['total_sum'] +
                    $bill_expenses['total_sum'] +
                    $bills_transference[$title]['plus'] -
                    $bills_transference[$title]['minus'];
            }

            return $this->render('bills-balances', ['bills_balances' => $bills_balances]);
        }
        throw new NotFoundHttpException();
    }

    public function actionGetProjectsBalances()
    {
        if(Yii::$app->request->isAjax){
            $this->layout = 'ajax';
            $bills_balances = [];
            $uid = Yii::$app->user->id;
            $projects = ArrayHelper::getColumn(Project::find()
                ->select(['id','title'])
                ->where(['and',['company_id' => $uid],['deleted' => false]])
                ->indexBy('id')
                ->asArray(true)
                ->all(),'title');
            $projects_balances = [];
            foreach($projects as $project_id => $title){
                $project_incomes = Summary::getTotalIncomes($uid,false,$project_id);
                $project_expenses = Summary::getTotalExpenses($uid,false,$project_id);
                $projects_balances[$title] = $project_incomes['total_sum'] + $project_expenses['total_sum'];
            }

            return $this->render('projects-balances', ['projects_balances' => $projects_balances]);
        }
        throw new NotFoundHttpException();
    }

    public function actionGetOperationsSum()
    {
        if(Yii::$app->request->isAjax){
            $this->layout = 'ajax';
            $period = Yii::$app->request->get('period',7);

            $last_days_incomes = Summary::getLastDaysIncomes($period,'total_sum');
            $last_days_expenses = Summary::getLastDaysExpenses($period,'total_sum');
            $last_days_balance = array();

            foreach ($last_days_incomes as $key => $value) {
                $last_days_balance[$key] = $last_days_expenses[$key] + $value;
            }

            return $this->renderAjax('ops-sum', [
                'chart' => [
                    'days' => Summary::getLastDays($period ),
                    'incomes_sum' => $last_days_incomes,
                    'expenses_sum' => $last_days_expenses,
                    'balance' => $last_days_balance
                ]
            ]);
        }
        throw new NotFoundHttpException();
    }

    public function actionGetOperationsCount()
    {
        if(Yii::$app->request->isAjax){
            $this->layout = 'ajax';

            $period = Yii::$app->request->get('period',7);

            $last_days_incomes = Summary::getLastDaysIncomes($period,'incomes_count');
            $last_days_expenses = Summary::getLastDaysExpenses($period,'expenses_count');

            return $this->renderAjax('ops-count', [
                'chart' => [
                    'days' => Summary::getLastDays($period ),
                    'incomes_count' => $last_days_incomes,
                    'expenses_count' => $last_days_expenses,
                ]
            ]);
        }
        throw new NotFoundHttpException();
    }

    public function actionIndex()
    {
        $incomes = Summary::getTotalIncomes(); // always positive
        $day_incomes = Summary::getTotalIncomes(false,false,false,1); // always positive
        $expenses = Summary::getTotalExpenses(); // always negative
        $day_expenses = Summary::getTotalExpenses(false,false,false,1); // always negative
        $reserves = Summary::getTotalReserves();
        $users = Summary::getUsers();
        $projects = Summary::getProjects();
        $bills = Summary::getBills();

        return $this->render('index',[
            'stat' => [
                'incomes_sum' => $incomes['total_sum'],
                'day_incomes_sum' => $day_incomes['total_sum'],
                'expenses_sum' => $expenses['total_sum'],
                'day_expenses_sum' => $day_expenses['total_sum'],
                'reserves_sum' => $reserves['total_sum'],
                'balance' => $incomes['total_sum'] + $expenses['total_sum'],
                'operations_count' => $incomes['incomes_count'] + $expenses['expenses_count'],

                'users_count' => $users['users_count'],
                'projects_count' => $projects['projects_count'],
                'bills_count' => $bills['bills_count'],
            ],

        ]);
    }

}