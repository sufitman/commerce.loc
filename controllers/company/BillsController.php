<?php

namespace app\controllers\company;

use app\controllers\SiteController;
use app\models\Bill;
use app\models\Reserve;
use app\models\search\BillSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\user\BaseUser;

class BillsController extends SiteController
{
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'edit'            => ['post'],
                    'delete'         => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'         => true,
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isCompany();
                        },
                    ],
                    [
                        'allow'         => true,
                        'actions'       => [
                            'delete',
                            'edit',
                        ],
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isCompany() ||  $identity->isAdmin();
                        },
                    ],
                    [
                        'allow'   => true,
                        'actions' => [],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new BillSearch();
        $params = Yii::$app->request->queryParams;
        $uid = Yii::$app->user->id;
        $dataProvider = $searchModel
            ->search($params);

        $bill = new Bill();
        $bill->company_id = $uid;
        $request = Yii::$app->request;
        if($request->isAjax){
            $loaded = $bill->load($request->post());
            if($loaded) {
                $bill->save();
                Yii::$app->end();
            }
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'bill' => $bill,

        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->_findModel(Bill::className(),$id);
        $model->deleted = 1;
        $model->save();

        return $this->redirect(['index']);
    }

    public function actionEdit($id)
    {
        $model = $this->_findModel(Bill::className(),$id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionValidate(){
        return $this->_validate(Bill::className());
    }

}