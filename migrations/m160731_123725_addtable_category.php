<?php

use yii\db\Migration;

class m160731_123725_addtable_category extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'company_id' => $this->integer(),
            'deleted' => $this->smallInteger(1)->defaultValue(0),
        ], $tableOptions);
        $this->createIndex('company_id_idx','category','company_id');
        $this->addForeignKey('users_categories','category','company_id','user','id','CASCADE');
        $this->addForeignKey('expenses_category','expense','category_id','category','id');
    }

    public function down()
    {
        $this->dropForeignKey('expenses_category','expense');
        $this->dropForeignKey('users_categories','category');
        $this->dropIndex('company_id_idx','category');
        $this->dropTable('category');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
