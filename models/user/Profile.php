<?php

namespace app\models\user;

/*use yii\helpers\FileHelper;
use yii\imagine\Image;
use yii\helpers\Json;
use Imagine\Image\Box;
use Imagine\Image\Point;*/

/**
 * @property int role_id
 */
class Profile extends \dektrium\user\models\Profile
{
    public function behaviors()
    {
        return [
            [
                'class' => \maxmirazh33\image\Behavior::className(),
                'savePathAlias' => '@webroot/upload/',
                'urlPrefix' => '/upload/',
                'crop' => true,
                'attributes' => [
                    'avatar' => [
                        'savePathAlias' => '@webroot/upload/avatars/',
                        'urlPrefix' => '/upload/avatars/',
                        'width' => 300,
                        'height' => 300,
                    ],
                ],
            ],
        ];
    }

    public function rules()
    {
        return [
            'bioString' => ['bio', 'string'],
            'publicEmailPattern' => ['public_email', 'email'],
            'gravatarEmailPattern' => ['gravatar_email', 'email'],
            'websiteUrl' => ['website', 'url'],
            'nameLength' => ['name', 'string', 'max' => 255],
            'publicEmailLength' => ['public_email', 'string', 'max' => 255],
            'gravatarEmailLength' => ['gravatar_email', 'string', 'max' => 255],
            'locationLength' => ['location', 'string', 'max' => 255],
            'websiteLength' => ['website', 'string', 'max' => 255],
            //['avatar','file','extensions' => 'jpg, jpeg, png','mimeTypes' => 'image/jpeg, image/png'],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return [
            'name'           => \Yii::t('user', 'Name'),
            'public_email'   => \Yii::t('user', 'Email (public)'),
            'gravatar_email' => \Yii::t('user', 'Gravatar email'),
            'location'       => \Yii::t('user', 'Location'),
            'website'        => \Yii::t('user', 'Website'),
            'bio'            => \Yii::t('user', 'Bio'),
            'avatar'         => 'Изменить аватар',
        ];
    }
}