<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \kartik\datecontrol\DateControl;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\search\ExpenseSearch */
/* @var $form yii\widgets\ActiveForm */
/* @var  $users array */
/* @var  $projects array */
/* @var  $categories array */
/* @var  $bills array */
?>

<div class="expense-search row">
    <?php $form = ActiveForm::begin([
        'action' => ['reserve-balance'],
        'method' => 'get',
    ]); ?>

    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <?= $form->field($model, 'type')->dropDownList([
            NULL => 'Тип',
            '+'  => 'Приход',
            '-'  => 'Расход',
            '?'  => 'Резерв',
        ])->label(false) ?>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <?= $form->field($model, 'project_title')->widget(Select2::classname(), [
            'data' => $projects,
            /*'language' => 'de',*/
            'options' => ['multiple' => true,'placeholder' => 'Проект(ы) ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])->label(false) ?>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <?= $form->field($model, 'user_name')->dropDownList([
                NULL => 'Добавил',
            ] + $users) ->label(false) ?>
    </div>


    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <?= $form->field($model, 'category_title')->dropDownList([
                NULL => 'Категория расхода',
            ] + $categories)->label(false) ?>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <?= $form->field($model, 'theBill')->dropDownList([
                NULL => 'Счет',
            ] + $bills)->label(false) ?>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <?= $form->field($model, 'min_sum', [
            'template' => '<div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-rub"></i>
                </span>{input}{error}</div>'
        ])->textInput(['placeholder' => $model->getAttributeLabel('min_sum')])->label(false); ?>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <?= $form->field($model, 'max_sum', [
            'template' => '<div class="input-group">
                <span class="input-group-addon">
                    <i class="fa fa-rub"></i>
                </span>{input}{error}</div>'
        ])->textInput(['placeholder' => $model->getAttributeLabel('max_sum')])->label(false);?>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <?= $form->field($model, 'start_time',['options' => ['class' => 'time_picker']])->widget(DateControl::className(), [
            'ajaxConversion' => false,
            'options' => [
                'options' => ['placeholder' => 'Время c']
            ],
            'value' => time(),
            'type' => DateControl::FORMAT_DATE,
            //'autoWidget' => false,
            'displayFormat' => 'php:D, d-M-Y',
            'saveFormat' => 'php:U'
        ])->label(false) ?>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <?= $form->field($model, 'end_time',['options' => ['class' => 'time_picker']])->widget(DateControl::className(), [
            'ajaxConversion' => false,
            'options' => [
                'options' => ['placeholder' => 'Время по']
            ],
            'value' => time(),
            'type' => DateControl::FORMAT_DATE,
            //'autoWidget' => false,
            'displayFormat' => 'php:D, d-M-Y',
            'saveFormat' => 'php:U'
        ])->label(false)?>
    </div>

</div>


<div class="row">

    <div class="col-lg-12">
        <div class="form-group text-right">
            <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Сброс', ['reserve-balance'],['class' => 'btn btn-default']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
