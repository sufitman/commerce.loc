<?php

namespace app\models\search;

use app\models\Bill;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Transference;
use yii\helpers\ArrayHelper;

/**
 * TransferenceSearch represents the model behind the search form about `app\models\Transference`.
 */
class TransferenceSearch extends Transference
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'from_bill', 'to_bill', 'datetime'], 'integer'],
            [['sum'], 'number'],
            [['comment'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $bills = ArrayHelper::getColumn(Bill::find()
            ->select(['id'])
            ->where([
                'and',
                ['in','company_id',array_filter([Yii::$app->user->id,Yii::$app->user->identity->company_id])                ],
                ['deleted' => false]
            ])
            ->asArray(true)
            ->indexBy('id')
            ->all(),'id');

        $query = Transference::find()->where([
            'and',
            ['in','to_bill',$bills],
            ['in','from_bill',$bills],
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'from_bill' => $this->from_bill,
            'to_bill' => $this->to_bill,
            'sum' => $this->sum,
            'datetime' => $this->datetime,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
