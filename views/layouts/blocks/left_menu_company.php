<?
$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;

$uid = Yii::$app->user->id;
?>
<aside class="main-sidebar">

	<section class="sidebar">
		<ul class="sidebar-menu">

			<li <?= ($controller == 'company/dashboard' && $action == 'index') ? ' class="active"' :'';?>>
				<a href="/">
					<i class="fa fa-dashboard"></i> <span>Главная</span>
				</a>
			</li>

			<li class="treeview<?= (in_array($controller,[
					'company/incomes',
					'company/expenses',
					'company/transferences',
				]))
				? '  active'
				: '';?>">

				<a href="#">
					<i class="fa fa-rub"></i><span>Деньги</span>
				</a>
				<ul class="treeview-menu">

					<li <?= ($controller == 'company/incomes') ? ' class="active"' :'';?>>
						<a href="<?= Yii::$app->getUrlManager()->createUrl(['company/incomes']);?>">
							<i class="fa fa-plus-square"></i> <span>Приходы</span>
						</a>
					</li>

					<li <?= ($controller == 'company/expenses') ? ' class="active"' :'';?>>
						<a href="<?= Yii::$app->getUrlManager()->createUrl(['company/expenses']);?>">
							<i class="fa fa-minus-square"></i> <span>Расходы</span>
						</a>
					</li>


					<li <?= ($controller == 'company/transferences') ? ' class="active"' :'';?>>
						<a href="<?= Yii::$app->getUrlManager()->createUrl(['company/transferences']);?>">
							<i class="fa fa-exchange"></i> <span>Переводы</span>
						</a>
					</li>

				</ul>
			</li>

			<li <?= ($controller == 'company/reserves') ? ' class="active"' :'';?>>
				<a href="<?= Yii::$app->getUrlManager()->createUrl(['company/reserves']);?>">
					<i class="fa fa-umbrella"></i> <span>Резервы</span>
				</a>
			</li>

			<li class="treeview<?= ($controller == 'company/statistics')
				? '  active'
				: '';?>">

				<a href="#">
					<i class="fa fa-file-text-o"></i><span>Отчеты</span>
				</a>
				<ul class="treeview-menu">
					<li<?= ($controller == 'company/statistics' && $action == 'balance') ? ' class="active"' :'';?>>
						<a href="<?= Yii::$app->getUrlManager()->createUrl(['company/statistics/balance']);?>">
							<i class="fa fa-file-excel-o"></i> <span>Остаток</span>
						</a>
					</li>
					<li<?= ($controller == 'company/statistics' && $action == 'reserve-balance') ? ' class="active"' :'';?>>
						<a href="<?= Yii::$app->getUrlManager()->createUrl(['company/statistics/reserve-balance']);?>">
							<i class="fa fa-file-excel-o"></i> <span>Остаток (резерв)</span>
						</a>
					</li>

					<li<?= ($controller == 'company/statistics' && $action == 'funds') ? ' class="active"' :'';?>>
						<a href="<?= Yii::$app->getUrlManager()->createUrl(['company/statistics/funds']);?>">
							<i class="fa fa-file-excel-o"></i> <span>Движение средств</span>
						</a>
					</li>

					<li<?= ($controller == 'company/statistics' && $action == 'projects') ? ' class="active"' :'';?>>
						<a href="<?= Yii::$app->getUrlManager()->createUrl(['company/statistics/projects']);?>">
							<i class="fa fa-file-excel-o"></i> <span>Отчет по проектам</span>
						</a>
					</li>
				</ul>
			</li>
			<li class="treeview<?= ((in_array($controller,[
					'company/categories',
					'company/bills',
					'company/employees',
					'company/projects',
					'company/backups',
				])) || in_array($action,['account','profile']))
				? '  active'
				: '';?>">

				<a href="#">
					<i class="fa fa-cogs"></i><span>Настройки</span>
				</a>
				<ul class="treeview-menu">
					<li<?= ($controller == 'company/employees') ? ' class="active"' :'';?>>
						<a href="<?= Yii::$app->getUrlManager()->createUrl(['company/employees']);?>">
							<i class="fa fa-users"></i><span>Сотрудники</span>
						</a>
					</li>
					<li<?= ($controller == 'company/bills') ? ' class="active"' :'';?>>
						<a href="<?= Yii::$app->getUrlManager()->createUrl(['company/bills']);?>">
							<i class="fa fa-credit-card"></i> <span>Счета</span>
						</a>
					</li>
					<li<?= ($controller == 'company/projects') ? ' class="active"' :'';?>>
						<a href="<?= Yii::$app->getUrlManager()->createUrl(['company/projects']);?>">
							<i class="fa fa-folder-open"></i><span>Проекты</span>
						</a>
					</li>
					<li<?= ($controller == 'company/categories') ? ' class="active"' :'';?>>
						<a href="<?= Yii::$app->getUrlManager()->createUrl(['company/categories']);?>">
							<i class="fa fa-bars"></i> <span>Категории операций</span>
						</a>
					</li>
					<li<?= ($controller == 'company/backups') ? ' class="active"' :'';?>>
						<a href="<?= Yii::$app->getUrlManager()->createUrl(['company/backups']);?>">
							<i class="fa fa-save"></i> <span>Бэкапы</span>
						</a>
					</li>
					<li<?= (in_array($action,['account','profile'])) ? ' class="active"' :'';?>>
						<a href="<?= Yii::$app->getUrlManager()->createUrl(['user/settings']);?>">
							<i class="fa fa-user"></i> <span>Профиль</span>
						</a>
					</li>
				</ul>
			</li>

		</ul>
	</section>
</aside>