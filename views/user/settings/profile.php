<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
//use bupy7\cropbox\Cropbox;
/*
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\Profile $model
 */

$this->title = Yii::t('user', 'Profile settings');
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss('
    /*#remove-photo{position:absolute;top:0px;left:285px;font-size: 200%;color:#fff}
    #remove-photo:hover{color:red}*/
    .current-avatar{padding-bottom:15px;position:relative;}
    @media all and (min-width: 1200px) {
        .current-avatar{padding-left:22px;}
    }
');

$this->registerJs("
   $('input[name=\"Profile[avatar]\"]').on('change',function(){
        $('.current-avatar').remove();
    });
");

?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<div class="row">
    <div class="col-md-3">
        <?= $this->render('_menu') ?>
    </div>
    <div class="col-md-9 col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <?= Html::encode($this->title) ?>
            </div>
            <div class="panel-body">
                <?php $form = \yii\widgets\ActiveForm::begin([
                    'id' => 'profile-form',
                    'options' => ['class' => 'form-horizontal','enctype'=>'multipart/form-data'],
                    'fieldConfig' => [
                        'template' => "{label}\n<div class=\"col-lg-9\">{input}</div>\n<div class=\"col-sm-offset-3 col-lg-9\">{error}\n{hint}</div>",
                        'labelOptions' => ['class' => 'col-lg-3 control-label'],
                    ],
                    'enableAjaxValidation'   => true,
                    'enableClientValidation' => false,
                    'validateOnBlur'         => false,
                ]); ?>
                <? if(!empty($model->avatar)) :?>
                <div class="col-lg-3 current-avatar">

                </div>
                <div class="col-lg-9 current-avatar">
                    <div class="form-group"><?= Html::img($model->getImageUrl('avatar'));?></div>
                    <!--<i class="fa fa-trash" id="remove-photo"></i>-->
                </div>
                <? endif;?>
                <?= $form->field($model, 'avatar')->widget('maxmirazh33\image\Widget');?>

                <?= $form->field($model, 'public_email') ?>

                <?= $form->field($model, 'website') ?>

                <?= $form->field($model, 'location') ?>

                <?//= $form->field($model, 'gravatar_email')->hint(\yii\helpers\Html::a(Yii::t('user', 'Change your avatar at Gravatar.com'), 'http://gravatar.com')) ?>

                <?= $form->field($model, 'bio')->textarea() ?>

                <div class="form-group">
                    <div class="col-lg-3 pull-right">
                        <?= \yii\helpers\Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-block btn-success']) ?><br>
                    </div>
                </div>

                <?php \yii\widgets\ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
