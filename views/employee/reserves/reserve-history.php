<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ReserveHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'История резерва';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="reserve-history-index modal-body">

    <h3><?= Html::encode($this->title) ?></h3>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>-->
<!--        --><?//= Html::a('Create Reserve History', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->


    <div class="history">
        <?= $this->render('history-grid', [
            'dataProvider' => $dataProvider,
        ]); ?>
    </div>

</div>