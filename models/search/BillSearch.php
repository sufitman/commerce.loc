<?php

namespace app\models\search;

use app\models\Summary;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Bill;

/**
 * BillSearch represents the model behind the search form about `app\models\Bill`.
 */
class BillSearch extends Bill
{
    public $total_incomes;

    public $total_expenses;

    public $total_transferences;

    public $balance;

    public $start_time;

    public $end_time;

    public function afterFind()
    {
        $filter = Yii::$app->request->getQueryParam('BillSearch',false);
        $this->start_time = $filter ? $filter['start_time'] : false;
        $this->end_time = $filter ? $filter['end_time'] : false;
        $this->total_incomes = Summary::getTotalIncomes(
            Yii::$app->user->id,
            $this->id,
            false,
            false,
            false,
            $this->start_time,
            $this->end_time
        )['total_sum'];
        $this->total_expenses = Summary::getTotalExpenses(
            Yii::$app->user->id,
            $this->id,
            false,
            false,
            $this->start_time,
            $this->end_time
        )['total_sum'];



        $transferences = Summary::getBillsTransferences(
            [$this->id => $this->title],
            $this->start_time,
            $this->end_time
        );

        $this->total_transferences = $transferences[$this->title]['plus'] -  $transferences[$this->title]['minus'];

        $this->balance = $this->total_incomes + $this->total_expenses + $this->total_transferences;

        parent::afterFind();
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id'], 'integer'],
            [[
                'title',
                'start_time',
                'end_time',
            ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()->where([
            'and',[
                'or',
                ['company_id' => Yii::$app->user->identity->company_id],
                ['company_id' => Yii::$app->user->id],
            ],['deleted' => false]
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);

        return $dataProvider;
    }
}
