<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 10.08.2016
 * Time: 15:34
 * @var $this yii\web\View */
/* @var $bills_balances array */
?>


<div class="box-header with-border">
    <h3 class="box-title">Баланс на счетах</h3>

    <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
    </div>
</div>
<!-- /.box-header -->
<div class="box-body">
    <div class="table-responsive">
        <table class="table no-margin">
            <thead>
            <tr>
                <th>Счет</th>
                <th>Баланс</th>
            </tr>
            </thead>
            <tbody>
                <? foreach($bills_balances as $bill => $bill_balance) : ?>
                    <tr>
                        <td class="text-info"><?=$bill?></td>
                        <td><?=Yii::$app->formatter->asDecimal($bill_balance,2)?></td>
                    </tr>
                <? endforeach?>

            </tbody>
        </table>
    </div>
    <!-- /.table-responsive -->
</div>
<!-- /.box-body -->
<div class="box-footer clearfix">
    <a href="<?= Yii::$app->urlManager->createUrl('company/bills')?>" class="btn btn-sm btn-info btn-flat pull-left"><i class="fa fa-plus"></i> Добавить счет</a>
    <a href="<?= Yii::$app->urlManager->createUrl('company/bills')?>" class="btn btn-sm btn-default btn-flat pull-right">Просмотреть детально</a>
</div>
<!-- /.box-footer -->