<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model \app\models\user\BaseUser */
/* @var $form yii\widgets\ActiveForm */

$i = 0;
?>

<div class="user-table-form">

    <div class="alert alert-warning">Для входа в систему вновь созданному сотруднику
        нужно использовать указанный вами <strong>e-mail</strong>
        и <strong>ваш логин (<span class="text-red"><?= \Yii::$app->user->identity->username; ?></span>)</strong>, в качестве пароля</div>

    <?php $form = ActiveForm::begin([
        'id' => 'add_employees',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'validateOnChange' => true,
        'validationUrl' => ['validate'],
        'options' => [
            'class' => 'action-form',
        ],
    ]);


    $fields = str_replace("\n", '',
        '<div class="users u-' . $i . ' row">' .
            '<div class="col-lg-4 col-md-4">' .
                $form->field($model, '[' . $i . ']email', [

                    'template' => '<div class="input-group">
                        <span class="input-group-addon">@</span>{input}
                    </div>{error}'
                ])->textInput([
                    'maxlength' => true ,
                    'placeholder' => $model->getAttributeLabel( 'email' )
                ])->label(false) .
                $form->field($model,'[' . $i . ']idx')->hiddenInput(['value' => $i])->label(false) .
            '</div>' .
            '<div class="col-lg-4 col-md-4">' .
                $form->field($model, '[' . $i . ']first_name', [
                    'template' => '<div class="input-group">
                        <!--<span class="input-group-addon">Имя</span>-->{input}
                    </div>{error}',
                ])->textInput(['maxlength' => true,'placeholder' => 'Имя'])->label(false) .
            '</div>' .
            '<div class="col-lg-4 col-md-4">' .
                $form->field($model, '[' . $i . ']second_name', [
                    'template' => '<div class="input-group">
                        {input}<span class="input-group-addon btn btn-red drop_this" data-minus="' . $i . '"><i class="fa fa-close text-danger"></i></span>
                    </div>{error}',
                ])->textInput(['maxlength' => true,'placeholder' => 'Фамилия'])->label(false) .
            '</div>' .
        '</div>'
    );

    $this->registerJs("
        var i = $i;
        var fields = '$fields';
        var activeFormObj = '[{\"id\":\"baseuser-0-email\",\"name\":\"[0]email\",\"container\":\".field-baseuser-0-email\",\"input\":\"#baseuser-0-email\",\"enableAjaxValidation\":true},{\"id\":\"baseuser-0-first_name\",\"name\":\"[0]first_name\",\"container\":\".field-baseuser-0-first_name\",\"input\":\"#baseuser-0-first_name\",\"enableAjaxValidation\":true},{\"id\":\"baseuser-0-second_name\",\"name\":\"[0]second_name\",\"container\":\".field-baseuser-0-second_name\",\"input\":\"#baseuser-0-second_name\",\"enableAjaxValidation\":true}]';
        $(document).on('click','.add_more',function(){
            i++;
            var new_fields = fields.replace(new RegExp($i, 'g'), i);
            newObj = activeFormObj.replace(new RegExp($i, 'g'),i).replace(new RegExp( 'Z'+i, 'g'), 'Z$i');

            $('#fields').append(new_fields);

            $.each(eval(newObj),function(){
                $('#add_employees').yiiActiveForm('add',$(this)[0]);
            })
        });
        $(document).on('click','.drop_this',function(){
            var number = $(this).attr('data-minus');
            var u_to_drop = '.u-' + number;
            if($(document).find('.users').length > 1) $(u_to_drop).remove();
            else return false;
        });
    ",$this::POS_END);
    ?>

    <div id="fields">
        <?= $fields ?>
    </div>



    <div class="form-group">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
