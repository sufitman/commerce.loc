<?php

namespace app\models\user;

use app\models\Group;
use app\models\GroupStudent;
use app\models\StudentCourse;
use app\models\UserTable;
use Yii;
use dektrium\user\models\User;
use dektrium\user\models\Token;
use dektrium\user\helpers\Password;
use app\models\UserNotification;
use yii\web\IdentityInterface;

/**
 * @property int role_id
 * @property int company_id
 * @property string first_name
 * @property string second_name
 * @property string company_name
 * @inheritdoc
 */
class BaseUser extends User
{
    public $idx;

    public $employee_search;

    public $full_name;

    const ROLE_ADMIN = 3;

    const ROLE_COMPANY = 2;

    const ROLE_EMPLOYEE = 1;

    public static $registrable = [
        'company' => self::ROLE_COMPANY,
    ];

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'][]      = 'first_name';
        $scenarios['create'][]      = 'second_name';
        $scenarios['update'][]      = 'first_name';
        $scenarios['update'][]      = 'second_name';
        $scenarios['update'][]      = 'new_password';
        $scenarios['update'][]      = 'password_confirmation';
        $scenarios['default']       = ['last_sign_in','confirmed_at'];
        $scenarios['add_employee']  = ['first_name','second_name','email'];
        $scenarios['register'][]    = 'first_name';
        $scenarios['register'][]    = 'second_name';
        $scenarios['register'][]    = 'role_id';
        $scenarios['register'][]    = 'company_name';
        return $scenarios;
    }


    public function rules()
    {
        $rules = parent::rules();
        $rules['first_nameRequired']   = ['first_name', 'required'];
        $rules['second_nameRequired']  = ['second_name', 'required'];
        $rules['role_idRequired']      = ['role_id',  'default', 'value' => self::ROLE_COMPANY];
        $rules['role_id_registrable']  = ['role_id', 'registrable','on' => 'register'];
        $rules['company_nameRequired'] = ['company_name', 'required','on' => 'register'];
        $rules['last_sign_inDigit']    = ['last_sign_in', 'integer'];
        $rules['add_first_name']       = ['first_name','required', 'on' => 'add_employee'];
        $rules['add_second_name']      = ['second_name','required', 'on' => 'add_employee'];
        $rules['add_email_unique']     = [
            'email',
            'unique',
            'on' => 'add_employee',
            'message' => 'Пользователь с таким email уже есть в системе!'
        ];
        $rules['add_email']            = ['email','required', 'on' => 'add_employee'];
        return $rules;
    }

    public function attributeLabels()
    {
        return parent::attributeLabels() + [
            'first_name' => 'Имя',
            'second_name' => 'Фамилия',
        ];
    }

    public function registrable($attribute, $params)
    {
        if (!in_array($this->$attribute, self::$registrable)) {
            $this->addError($attribute, 'Введены неверные параметры!');
        }
    }

    public function isAdmin()
    {
        return $this->role_id == self::ROLE_ADMIN;
    }

    public function isCompany()
    {
        return $this->role_id == self::ROLE_COMPANY;
    }

    public function isEmployee()
    {
        return $this->role_id == self::ROLE_EMPLOYEE;
    }

    public function getRoleHome()
    {
        switch($this->role_id)
        {
            case self::ROLE_ADMIN :
                return \Yii::$app->getUrlManager()->createUrl(['admin/index']);
            case self::ROLE_COMPANY :
                return \Yii::$app->getUrlManager()->createUrl(['company/dashboard/index']);
            case self::ROLE_EMPLOYEE :
                return \Yii::$app->getUrlManager()->createUrl(['employee/dashboard/index']);
            default :
                return \Yii::$app->getUrlManager()->createUrl(['user/login']);
        }
    }

    public function getRoleName()
    {
        switch($this->role_id)
        {
            case self::ROLE_ADMIN :
                return 'admin';
            case self::ROLE_COMPANY :
                return 'company';
            case self::ROLE_EMPLOYEE :
                return 'employee';
            default :
                return 'unsigned';
        }
    }

    public function getRole($end = '')
    {
        switch($this->role_id)
        {
            case self::ROLE_ADMIN :
                return 'администратор' . $end;
            case self::ROLE_COMPANY :
                return 'компани' . $end ? $end : 'я';
            case self::ROLE_EMPLOYEE :
                return 'сотрудник' . $end;
            default :
                return 'есть такой';
        }
    }

    public function register()
    {
        if ($this->getIsNewRecord() == false) {
            throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on existing user');
        }

        $this->confirmed_at = $this->module->enableConfirmation ? null : time();
        $this->password     = $this->module->enableGeneratingPassword ? Password::generate(8) : $this->password;

        $this->trigger(self::BEFORE_REGISTER);

        if (!$this->save()) {
            return false;
        }

        if ($this->module->enableConfirmation) {
            /** @var Token $token */
            $token = Yii::createObject(['class' => Token::className(), 'type' => Token::TYPE_CONFIRMATION]);
            $token->link('user', $this);
        }

        $this->mailer->sendWelcomeMessage($this, isset($token) ? $token : null);
        $this->trigger(self::AFTER_REGISTER);

        return true;
    }

    public static function add_new_user($name,$email,$role,$company_id)
    {
        $exist = (BaseUser::findOne(['email' => $email]));

        if(!$exist){
            $full_name = self::make_full_name($name);
            if(!$full_name) return 'Указано не корректное имя!';

            $username = explode('@',$email)[0];
            if(BaseUser::findOne(['username' => $username]) || strlen($username) < 3)
                $username = $username . '_' . rand(0,999);
            $current_time = time();

            $user = new self;
            $user->setScenario('create');

            $user->setAttributes([
                'created_at'    => $current_time,
                'updated_at'    => $current_time,
                'confirmed_at'  => $current_time,
                'registered_by' => Yii::$app->user->getId(),
                'password_hash' => Password::hash(Yii::$app->user->identity->username),
                'email'         => $email,
                'company_id'    => $company_id,
                'username'      => $username,
                'first_name'    => (!empty($full_name)) ? $full_name['first_name'] : '',
                'second_name'   => (!empty($full_name)) ? $full_name['second_name'] : '',
                'role_id'       => $role,
            ],false);


            if($user->validate() && $user->save()) {
                return $user->id;
            }else return $user->errors;
        }

        return 'Пользователь уже существует';
    }

    public static function make_full_name($string)
    {
        $words = preg_split("/[\s*]+/",trim($string));
        if(count($words) < 2) return false;

        if(count($words) > 2){
            return [
                'first_name' => $words[1] . ' ' . $words[2],
                'second_name' => $words[0],
            ];
        }else{
            return [
                'first_name' => $words[0],
                'second_name' => $words[1],
            ];
        }
    }

}
