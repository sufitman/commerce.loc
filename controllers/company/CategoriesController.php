<?php

namespace app\controllers\company;

use app\controllers\SiteController;
use app\models\Category;
use app\models\search\CategorySearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\user\BaseUser;

class CategoriesController extends SiteController
{
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'      => ['post'],
                    'edit'        => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'         => true,
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isCompany();
                        },
                    ],
                    [
                        'allow'         => true,
                        'actions'       => [
                            'delete',
                            'edit',
                        ],
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isCompany() ||  $identity->isAdmin();
                        },
                    ],
                    [
                        'allow'   => true,
                        'actions' => [],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $params = Yii::$app->request->queryParams;
        $uid = Yii::$app->user->id;
        $dataProvider = $searchModel
            ->search($params);

        $category = new Category();

        $request = Yii::$app->request;

        $loaded = $category->load($request->post());

        if($loaded && $request->isAjax) {
            $category->company_id = $uid;
            $category->save();
            Yii::$app->end();
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'category' => $category,
        ]);
    }

    public function actionEdit($id)
    {
        $model = $this->_findModel(Category::className(),$id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $type = Yii::$app->request->getQueryParam('type',false);
        $model = $this->_findModel(Category::className(),$id);
        $model->deleted = 1;
        $model->save();

        return $this->redirect(['index', 'type' => $type ? $type : '']);
    }

    public function actionValidate(){
        return $this->_validate(Category::className());
    }
}