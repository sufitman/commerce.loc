<?php

/* @var $this yii\web\View */
/* @var $stat array */
/* @var $chart array */

rmrevin\yii\ionicon\AssetBundle::register($this);

$this->registerJs("
    $('#ops-sum').load('" . Yii::$app->urlManager->createUrl('company/dashboard/get-operations-sum') . "');
    $('#ops-count').load('" . Yii::$app->urlManager->createUrl('company/dashboard/get-operations-count') . "');
    $('#bills-list').load('" . Yii::$app->urlManager->createUrl('company/dashboard/get-bills-balances') . "');
    $('#projects-list').load('" . Yii::$app->urlManager->createUrl('company/dashboard/get-projects-balances') . "');
   
    $(document).on('click','.chart-sum',function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        $('#ops-sum').load(url);
    }).on('click','.chart-count',function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        $('#ops-count').load(url);
    });
",$this::POS_END);
$this->registerCss("
    .box{min-height:77px}
    .small-box{overflow:hidden}
");
$this->title = 'Главная';
?>

<div class="row">

    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <a href="<?= Yii::$app->urlManager->createUrl(['company/employees'])?>">
            <div class="info-box bg-maroon">
                <span class="info-box-icon"><i class="fa fa-users"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Сотрудники</span>
                    <span class="info-box-number"><?=$stat['users_count']?></span>

                    <!--<div class="progress">-->
                        <!--<div class="progress-bar" style="width:....%"></div>-->
                    <!--</div>-->
                    <!--<span class="progress-description"></span>-->
                </div>
            </div>
        </a>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <a href="<?= Yii::$app->urlManager->createUrl(['company/bills'])?>">
            <div class="info-box bg-green-active">
                <span class="info-box-icon"><i class="fa fa-university"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Счета</span>
                    <span class="info-box-number"><?=$stat['bills_count']?></span>

                    <!--<div class="progress">-->
                        <!--<div class="progress-bar" style="width:....%"></div>-->
                    <!--</div>-->
                    <!--<span class="progress-description"></span>-->
                </div>
            </div>
        </a>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <a href="<?= Yii::$app->urlManager->createUrl(['company/statistics/funds'])?>">
            <div class="info-box bg-yellow">
                <span class="info-box-icon"><i class="fa fa-bar-chart"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Операции</span>
                    <span class="info-box-number"><?=$stat['operations_count']?></span>

                    <!--<div class="progress">-->
                        <!--<div class="progress-bar" style="width:....%"></div>-->
                    <!--</div>-->
                    <!--<span class="progress-description"></span>-->
                </div>
            </div>
        </a>
    </div>

    <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
        <a href="<?= Yii::$app->urlManager->createUrl(['company/statistics/projects'])?>">
            <div class="info-box bg-purple">
                <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Проекты</span>
                    <span class="info-box-number"><?=$stat['projects_count']?></span>

                    <!--<div class="progress">-->
                        <!--<div class="progress-bar" style="width:....%"></div>-->
                    <!--</div>-->
                    <!--<span class="progress-description"></span>-->
                </div>
            </div>
        </a>
    </div>

</div>

<div class="row">
    <div class="col-lg-6 col-md-12">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3><?=$stat['expenses_sum']
                        ? Yii::$app->formatter->asDecimal($stat['expenses_sum'],2)
                        : '0,00'?> <i class="fa fa-rub"></i> / <?=$stat['day_expenses_sum']
                        ? Yii::$app->formatter->asDecimal($stat['day_expenses_sum'],2)
                        : '0,00'?> <i class="fa fa-rub"></i></h3>

                <p>Расходы (общие / за день)</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <a href="<?= Yii::$app->urlManager->createUrl(['company/expenses'])?>" class="small-box-footer">Подробнее <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-6 col-md-12">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3><?=$stat['incomes_sum']
                        ? Yii::$app->formatter->asDecimal($stat['incomes_sum'],2)
                        : '0,00' ?> <i class="fa fa-rub"></i> / <?= $stat['day_incomes_sum']
                        ? Yii::$app->formatter->asDecimal($stat['day_incomes_sum'],2)
                        : '0,00'?> <i class="fa fa-rub"></i></h3>

                <p>Приходы (общие / за день)</p>
            </div>
            <div class="icon">
                <i class="ion ion-cash"></i>
            </div>
            <a href="<?= Yii::$app->urlManager->createUrl(['company/incomings'])?>" class="small-box-footer">Подробнее <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-6 col-md-12">
        <!-- small box -->
        <div class="small-box bg-orange">
            <div class="inner">
                <h3><?=$stat['reserves_sum']
                        ? Yii::$app->formatter->asDecimal($stat['reserves_sum'],2)
                        : '0,00' ?> <i class="fa fa-rub"></i></h3>

                <p>Резервы</p>
            </div>
            <div class="icon">
                <i class="ion ion-umbrella"></i>
            </div>
            <a href="<?= Yii::$app->urlManager->createUrl(['company/reserves'])?>" class="small-box-footer">Подробнее <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-6 col-md-12">
        <!-- small box -->
        <div class="small-box bg-blue">
            <div class="inner">
                <h3><?=$stat['balance']
                        ? Yii::$app->formatter->asDecimal($stat['balance'],2)
                        : '0,00'?> <i class="fa fa-rub"></i></h3>

                <p>Баланс</p>
            </div>
            <div class="icon">
                <i class="ion ion-<?= ($stat['balance'] > 0) ? 'happy' : 'sad'?>-outline"></i>
            </div>
            <a href="<?= Yii::$app->urlManager->createUrl(['company/statistics/balance'])?>" class="small-box-footer">Подробнее <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="box" id="ops-sum">
            <div class="overlay">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="box" id="ops-count">
            <div class="overlay">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="box box-info" id="bills-list">
            <div class="overlay">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="box box-warning" id="projects-list">
            <div class="overlay">
                <i class="fa fa-refresh fa-spin"></i>
            </div>
        </div>
    </div>
</div>
