<?php

use yii\db\Migration;

class m160816_112939_add_balance_incoming extends Migration
{
    public function up()
    {
        $this->addColumn('incoming','is_balance',$this->smallInteger(1)->defaultValue(0));
    }

    public function down()
    {
        $this->dropColumn('incoming','is_balance');
    }
}
