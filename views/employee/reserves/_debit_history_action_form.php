<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ReserveHistory */
/* @var $form yii\widgets\ActiveForm */
/* @var $reserve_id  */
/* @var $categories array */
?>

<div class="reserve-history-form row">

    <?php $form = ActiveForm::begin([

        'action' => ['debit','reserve_id' => $reserve_id],
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'validateOnChange' => true,
        'validationUrl' => ['validate-reserve-history','scenario' => 'debit'],
        'options' => [
            'class' => 'action-form debit-form',
            'id' => 'debit-reserve-history-action-' . $reserve_id,
        ],
    ]); ?>



    <?= $form->field($model, 'reserve_id',[
        'options' => ['id' => 'debit_reserve_history']
    ])->hiddenInput()->label(false) ?>

    <div class="col-lg-6 col-md-6 col-sm-6">
        <?= $form->field($model, 'category_id')->dropDownList([NULL => 'Выберите категорию'] + $categories)->label(false) ?>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-6">
        <?= $form->field($model, 'from_bill')->dropDownList([NULL => 'Выберите счет'] + $bills)->label(false) ?>
    </div>

    <div class="col-lg-12">
        <?= $form->field($model, 'sum')->textInput([
            'maxlength' => true,
            'placeholder' => $model->getAttributeLabel( 'sum' )
        ])->label(false) ?>
    </div>

    <div class="col-lg-12">
        <?= $form->field($model, 'description')->textarea([
            'placeholder' => $model->getAttributeLabel( 'description' )
        ])->label(false) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Добавить', ['class' => 'btn btn-success container-full']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
