<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Incoming;

/**
 * IncomingSearch represents the model behind the search form about `app\models\Incoming`.
 */
class IncomingSearch extends Incoming
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'project_id', 'created_by', 'to_bill', 'datetime'], 'integer'],
            [[ 'description', 'comment'], 'safe'],
            [['sum'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$employee = false)
    {
        $related_to = !$employee
            ? [
                'or',
                ['created_by' => Yii::$app->user->id],
                ['company_id' => Yii::$app->user->id],
            ]
            : ['created_by' => Yii::$app->user->id];
        $where = [
            'and',
            $related_to,
            ['is_balance' => false]
        ];
        $query = Incoming::find()->where($where);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            'project_id' => $this->project_id,
            'created_by' => $this->created_by,
            'sum' => $this->sum,
            'to_bill' => $this->to_bill,
            'datetime' => $this->datetime,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
