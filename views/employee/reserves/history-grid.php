<?
use yii\grid\GridView;

/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'sum',
            'content' => function($data){
                return Yii::$app->formatter->asDecimal($data->sum,2);
            }
        ],
        'datetime:datetime',
    ],
]); ?>