<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\widgets\Menu;

/** @var dektrium\user\models\User $user */
$user = Yii::$app->user->identity;
$networksVisible = count(Yii::$app->authClientCollection->clients) > 0;
$this->registerCss('
    .bg-white{background:#fff;padding:7px; margin:3px -10px 10px 10px;border-radius:6px;}
    .supersmall{font-size:70%}
    .uname{margin-bottom:3px}
    #field-profile-avatar{padding-left:15px}
');
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 bg-white">
                <img src="/upload/<?= !is_null($user->profile->avatar)
                    ? 'avatars/' . $user->profile->avatar
                    : 'no-avatar-ff.png';?>"
                     class="img-rounded" alt="<?= $user->username ?>" style="width:100%"/>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 text-center">
                <h4 class="uname">
                    <?= $user->first_name . ' ' . $user->second_name?>
                </h4>
                <small class="supersmall">
                    <?= $user->getRole();?>
                </small>
            </div>
        </div>

    </div>
    <div class="panel-body">
        <?= Menu::widget([
            'options' => [
                'class' => 'nav nav-pills nav-stacked',
            ],
            'items' => [
                ['label' => Yii::t('user', 'Account'), 'url' => ['/user/settings/account']],
                ['label' => Yii::t('user', 'Profile'), 'url' => ['/user/settings/profile']],
                ['label' => Yii::t('user', 'Networks'), 'url' => ['/user/settings/networks'], 'visible' => $networksVisible],
            ],
        ]) ?>
    </div>
</div>
