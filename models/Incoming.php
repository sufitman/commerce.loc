<?php

namespace app\models;

use Yii;
use app\models\user\BaseUser as User;
/**
 * This is the model class for table "incoming".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $project_id
 * @property integer $created_by
 * @property double $sum
 * @property integer $to_bill
 * @property string $description
 * @property string $comment
 * @property integer $datetime
 * @property bool $is_balance
 * @property integer $category_id
 *
 * @property User $createdBy
 * @property Bill $toBill
 * @property Bill $theBill in order to calculate balance
 * @property Project $project
 * @property User $company
 * @property Category $category
 */
class Incoming extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'incoming';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'project_id', 'created_by', 'to_bill', 'datetime'], 'integer'],
            [['project_id','description'], 'required','when' => function ($model) {
                return !$model->is_balance;
            }, 'enableClientValidation' => false],
            [['category_id'], 'safe'],
            [['to_bill', 'datetime','sum','is_balance'], 'required'],
            [['sum'],  'match', 'pattern'=>'/^[0-9]{1,12}([.,][0-9]{0,2})?$/', 'message' => 'Значение должно быть числовым, с точностью до сотых'],
            [['description', 'comment'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['to_bill'], 'exist', 'skipOnError' => true, 'targetClass' => Bill::className(), 'targetAttribute' => ['to_bill' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    public function checkNotBalance($attribute, $params)
    {
        if(empty($this->is_balance) && empty($this->$attribute))
            $this->addError($attribute,'Поле должно быть заполнено');
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->sum = str_replace(",", ".", $this->sum);
            return true;
        } else {
            return false;
        }
    }

    public function beforeValidate()
    {
        $this->is_balance = (int) $this->is_balance;
        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'             => 'ID',
            'company_id'     => 'Компания',
            'project_id'     => 'Проект',
            'created_by'     => 'Добавлен',
            'sum'            => 'Сумма',
            'to_bill'        => 'На счет',
            'description'    => 'Описание',
            'comment'        => 'Комментарий',
            'datetime'       => 'Время',
            'category_id'    => 'Категория',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToBill()
    {
        return $this->hasOne(Bill::className(), ['id' => 'to_bill']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTheBill()
    {
        return $this->hasOne(Bill::className(), ['id' => 'to_bill']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(User::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}
