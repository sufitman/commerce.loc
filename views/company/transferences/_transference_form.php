<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model \app\models\Transference */
/* @var $form yii\widgets\ActiveForm */

/* @var $bills array */

$this->registerCss(".btn-100{width:100%}");

$model->datetime = time();
?>

<div class="user-table-form">
    <? if (!empty($bills) && count($bills) > 1) :?>
    <?php $form = ActiveForm::begin([
        'id' => 'add_category',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'validateOnChange' => true,
        'validationUrl' => ['validate'],
        'options' => [
            'class' => 'action-form',
        ],
    ]); ?>

    <div id="fields">

        <div class="users row">

            <div class="col-lg-6 col-md-6">
                <?= $form->field($model, 'from_bill')->dropDownList([NULL => 'Cо счета'] + $bills)->label(false)?>
            </div>

            <div class="col-lg-6 col-md-6">
                <?= $form->field($model, 'to_bill')->dropDownList([NULL => 'На счет'] + $bills)->label(false)?>
            </div>

            <div class="col-lg-6 col-md-6">
                <?= $form->field($model, 'sum')->textInput([
                    'maxlength' => true,
                    'placeholder' => $model->getAttributeLabel( 'sum' )
                ])->label(false)?>
            </div>

            <div class="col-lg-6 col-md-6">
                <?= $form->field($model, 'datetime')->widget(DateControl::classname(), [
                    'ajaxConversion' => false,
                    'options' => [],
                    'type' => DateControl::FORMAT_DATETIME,
                    //'autoWidget' => false,
                    'displayFormat' => 'php:D, d-M-Y H:i:s',
                    'saveFormat' => 'php:U'
                ])->label(false)?>

            </div>

            <div class="col-lg-12 col-md-12">
                <?= $form->field($model, 'comment')->textarea([
                    'placeholder' => $model->getAttributeLabel( 'comment' )
                ])->label(false)?>
            </div>

            <div class="col-lg-12 col-md-12">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => 'btn btn-100  btn-success']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
    <? else :?>
        <div class="note note-warning">
            <p>Чтобы указывать на перевод средств в системе должны быть созданы по меньшей мере два
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['company/bills'])?>"><strong> счета</strong></a>
            </p>
        </div>
    <? endif;?>
</div>
