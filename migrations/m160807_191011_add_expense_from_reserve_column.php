<?php

use yii\db\Migration;

class m160807_191011_add_expense_from_reserve_column extends Migration
{
    public function up()
    {
        $this->addColumn('expense','from_reserve',$this->integer()->defaultValue(NULL));
        $this->addForeignKey('expense_reserve_fk','expense','from_reserve','reserve','id','CASCADE');
        $this->createIndex('expense_reserve_idx','expense','from_reserve');
        $this->addColumn('reserve','deleted',$this->smallInteger(1)->defaultValue(0));
        $this->createIndex('reserve_deleted_idx','reserve','deleted');
    }

    public function down()
    {
        $this->dropIndex('reserve_deleted_idx','reserve');
        $this->dropColumn('reserve','deleted');
        $this->dropForeignKey('expense_reserve_fk','expense');
        $this->dropIndex('expense_reserve_idx','expense');
        $this->dropColumn('expense','from_reserve');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
