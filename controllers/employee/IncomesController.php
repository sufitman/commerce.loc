<?php

namespace app\controllers\employee;

use app\controllers\SiteController;
use app\models\Bill;
use app\models\Category;
use app\models\Incoming;
use app\models\Project;
use app\models\search\IncomingSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\user\BaseUser;
use yii\web\NotFoundHttpException;

class IncomesController extends SiteController
{
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'      => ['post'],
                    'edit'        => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'         => true,
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isEmployee();
                        },
                    ],
                    [
                        'allow'         => true,
                        'actions'       => [
                            'delete',
                            'edit',
                        ],
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isEmployee() ||  $identity->isAdmin();
                        },
                    ],
                    [
                        'allow'   => true,
                        'actions' => [],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }
    public $defaultAction = 'index';

    public function actionIndex()
    {
        $searchModel = new IncomingSearch();
        $params = Yii::$app->request->queryParams;
        $uid = Yii::$app->user->identity->company_id;
        $dataProvider = $searchModel
            ->search($params,true);

        $projects = ArrayHelper::getColumn(Project::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('id')
            ->all(),'title');

        $bills = ArrayHelper::getColumn(Bill::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('id')
            ->all(),'title');

        $categories = ArrayHelper::getColumn(Category::find()
            ->select(['id','title'])
            ->where(['and',['and',['company_id' => $uid],['deleted' => false]],['type' => Category::TYPE_INCOME]])
            ->asArray(true)
            ->indexBy('id')
            ->all(),'title');

        $incoming = new Incoming();

        $request = Yii::$app->request;

        $loaded = $incoming->load($request->post());

        if($loaded && $request->isAjax) {
            $incoming->company_id = $uid;
            $incoming->created_by = Yii::$app->user->id;
            $incoming->save();

            Yii::$app->end();
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'incoming' => $incoming,
            'projects' => $projects,
            'bills' => $bills,
            'categories' => $categories,
        ]);
    }

    public function actionEdit($id)
    {
        $model = $this->_findModel(Incoming::className(),$id);

        if ($model->created_by != Yii::$app->user->id)
            throw new NotFoundHttpException();

        $uid = Yii::$app->user->identity->company_id;
        $projects = ArrayHelper::getColumn(Project::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('id')
            ->all(),'title');

        $bills = ArrayHelper::getColumn(Bill::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('id')
            ->all(),'title');

        $categories = ArrayHelper::getColumn(Category::find()
            ->select(['id','title'])
            ->where(['and',['and',['company_id' => $uid],['deleted' => false]],['type' => Category::TYPE_INCOME]])
            ->asArray(true)
            ->indexBy('id')
            ->all(),'title');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'projects' => $projects,
                'bills' => $bills,
                'categories' => $categories,
            ]);
        }
    }

    public function actionDelete($id,$go_back = false)
    {
        $model = $this->_findModel(Incoming::className(),$id);
        if ($model->created_by == Yii::$app->user->id)
            $model->delete();
        else throw new NotFoundHttpException();

        return $this->redirect([!$go_back ? 'index' : $go_back]);
    }

    public function actionValidate(){
        return $this->_validate(Incoming::className());
    }

}