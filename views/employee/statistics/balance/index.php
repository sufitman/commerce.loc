<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use app\models\Summary;

//use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\BalanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var  $projects array */
/* @var  $categories array */
/* @var  $bills array */
/* @var  $show_all bool */

$this->registerCss('
    .kv-checkbox-list{color:#000}
');

$url = ['employee/statistics/balance'];

if(!$show_all){
    $url['show_all'] =  true;
}

$gridColumns = [
    [
        'class' => 'yii\grid\SerialColumn',
        'footer'=> 'Итого',
    ],
    [
        'label' => 'Тип',
        'attribute' => 'type',
        'value'  => function($data){
            return ($data['type'] == '+' ? 'Приход' : 'Расход');
        }
    ],
    [
        'label' => 'Проект',
        'attribute' => 'project_title',
        'value' => function($data){
            return !empty($data['project_title']) ? $data['project_title'] : '-';
        }
    ],
    [
        'label' => 'Счет',
        'attribute' => 'theBill',
    ],
    [
        'label' => 'Категория (расходов)',
        'attribute' => 'category_title',
        'value'  => function($data){
            return ($data['category_title'] ? $data['category_title'] : '-');
        }
    ],
    [
        'label' => 'Сумма (руб.)',
        'attribute' => 'balance_sum',
        'format' => 'raw',
        'value'  => function($data){
            $color = $data['type'] == '+' ? 'green' : ($data['type'] == '-' ? 'red' : 'default');
            return Html::tag('span', Yii::$app->formatter->asDecimal($data['balance_sum'],2),['class' => 'text-' .$color]);
        },
        'footer'=> Yii::$app->formatter
            ->asDecimal(Summary::getTotalBalanceSum($dataProvider->models,'balance_sum'),2) . ' руб.',
    ],
    [
        'label' => 'Описание',
        'attribute' => 'description',
    ],
    [
        'label' => 'Время',
        'attribute' => 'datetime',
        'value' => function($data){
            return date('d.m.Y H:i',$data['datetime']);
        }
    ],
    [
        'label' => 'Комментарий',
        'attribute' => 'comment',
    ],

];

$this->title = 'Отчеты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="expense-index">
    <div class="panel panel-primary">
        <div class="panel-heading">Остаток без учета резерва</div>
        <div class="panel-body">

            <?= $this->render('_search', [
                'model' => $searchModel,
                'projects' => $projects,
                'categories' => $categories,
                'bills' => $bills,
            ]); ?>

<!--            --><?//= ExportMenu::widget([
//                'dataProvider' => $dataProvider,
//                'columns' => $gridColumns,
//            ]);?>
            <!--    <p>-->
            <!--        --><?//= Html::a('Create Expense', ['create'], ['class' => 'btn btn-success']) ?>
            <!--    </p>-->
            <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'showFooter' => TRUE,
                    'footerRowOptions' => ['style'=>'font-weight:bold;text-decoration: underline;'],
                    'columns' => $gridColumns + [
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'buttons' => [
                                    'delete' => function($url, $model){
                                        $custom_url = Yii::$app->getUrlManager()->createUrl([
                                            $model['type'] == '+'
                                                ? 'company/delete-incoming'
                                                : 'company/delete-expense',
                                            'id' => $model['operation_id'],
                                            'go_back' => 'balance',
                                        ]);

                                        return Html::a(
                                            '<span class="btn btn-xs btn-default action-btn">
                                                    <i class="fa fa-trash"></i>
                                                        <span class="btn-title">&nbsp;Удалить</span>
                                                </span>&nbsp;',
                                            $custom_url,
                                            ['title' => 'Удалить', 'class' => 'action-link', 'data-method' => 'post']
                                        );
                                    }
                                ],
                                'template' => '{delete}',
                            ],
                    ],
                    'panel' => [
                        'before' => Html::a('<i class="fa fa-angle-double-' . ($show_all ? 'up' : 'down') . '"></i> ' .
                                ($show_all ? 'Показать первую страницу' : 'Показать все'), $url, [
                                'class' => 'btn btn-default',
                                'title' => $show_all ? 'Показать первую страницу' : 'Показать все'
                            ]) . \kartik\export\ExportMenu::widget([
                                'dataProvider' => $dataProvider,
                                'columns' => $gridColumns,
                                'target' => \kartik\export\ExportMenu::TARGET_BLANK,
                                'showConfirmAlert' => false,
                                'dropdownOptions' => [
                                    'label' => 'Экспорт'
                                ]
                            ]),

                    ],
                    'pjax'=>true,
                    'toolbar' => [
                        [
                            'class' => 'pull-left',
                            'content' => ''
                        ],
                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>