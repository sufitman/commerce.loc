<?

/* @var $homeUrl string */

use yii\bootstrap\Nav;


$identity = Yii::$app->user->identity;
/* @var $identity \app\models\user\BaseUser */
$user_name = $identity->first_name . ' ' . $identity->second_name;
$company = $identity->company_name;

?>
<header class="main-header">
	<a href="/" class="logo">
		<!-- mini logo for sidebar mini 50x50 pixels -->
		<span class="logo-mini"><b>C</b>MR</span>
		<!-- logo for regular state and mobile devices -->
		<span class="logo-lg"><b>Comm</b>erce</span>
	</a>
	<nav class="navbar navbar-static-top">

		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
		</a>

		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">

				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<img src="/upload/<?= !is_null($identity->profile->avatar)
							? 'avatars/' . $identity->profile->avatar
							: 'no-avatar-ff.png';?>" class="user-image" alt="User Image">
						<span class="hidden-xs"><?=$user_name?></span>
					</a>
					<ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header">
							<img src="/upload/<?= !is_null($identity->profile->avatar)
								? 'avatars/' . $identity->profile->avatar
								: 'no-avatar-ff.png';?>" class="img-circle" alt="User Image">

							<p>
								<?=$user_name?> - <?=$company?>
							</p>
						</li>
						<!-- Menu Footer-->
						<li class="user-body">
							<div class="pull-left">
								<a href="<?= Yii::$app->getUrlManager()->createUrl(['user/settings/account']);?>" class="btn btn-default btn-flat">Профиль</a>
							</div>
							<div class="pull-right">
								<a href="<?= Yii::$app->getUrlManager()->createUrl(['user/logout']);?>" class="btn btn-default btn-flat" data-method="post">Выход</a>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
</header>