<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Backup */
/* @var $bills array */
/* @var $projects array */
/* @var $categories array */

$this->title = 'Обновить приход: #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Приходы', 'url' => ['company/incomes']];
$this->params['breadcrumbs'][] = ['label' => '#' .$model->id];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="user-table-index">

    <div class="panel panel-primary">
        <div class="panel-heading">Редактировать приход</div>
        <div class="panel-body">
            <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                <?= $this->render('_incoming_form', [
                    'model' => $model,
                    'projects' => $projects,
                    'bills' => $bills,
                    'categories' => $categories,
                ]) ?>
            </div>
        </div>
    </div>
</div>
