<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ReserveHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model app\models\ReserveHistory */
/* @var $categories array */
/* @var $bills array */

$this->title = 'Списать средства';
$this->params['breadcrumbs'][] = $this->title;
$reserve_id = $searchModel->reserve->id;
?>
<div class="reserve-history-index modal-body">

    <h3><?= Html::encode($this->title) ?></h3>
    <?= $this->render('_debit_history_action_form', [
        'model' => $model,
        'reserve_id' => $reserve_id,
        'categories' => $categories,
        'bills' => $bills,
    ]); ?>
    <!--    <p>-->
    <!--        --><?//= Html::a('Create Reserve History', ['create'], ['class' => 'btn btn-success']) ?>
    <!--    </p>-->

    <div class="history">
        <?= $this->render('history-grid', [
            'dataProvider' => $dataProvider,
        ]); ?>
    </div>

</div>
