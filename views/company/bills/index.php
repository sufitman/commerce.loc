<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use app\models\Summary;

/* @var $this yii\web\View */
/* @var $view */

/* @var $searchModel app\models\search\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/* @var $bill \app\models\Bill */

$this->title = 'Счета';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss('
	.action-link,.action-link:hover{text-decoration:none;}
');

$this->registerJs('
    $(document).on("beforeSubmit",".action-form",function(e){
        var form = $(this);
        var url = form.attr("action");
        $.ajax({
            url : url,
            data : form.serialize(),
            method : "post",
            success : function(data){
                $.pjax.reload({container:"#bls", async:true});
                form[0].reset();
            }
        });
    }).on("submit",".action-form",function(e){
        e.preventDefault();
    });
')
?>
<div class="user-table-index">

    <div class="panel panel-primary">
        <div class="panel-heading">Счета компании
            <?=Html::a(
                '<span class="btn btn-xs btn-default pull-right">
                    <i class="fa fa-plus"></i>
                        <span class="btn-title">&nbsp;Добавить счет</span>
                </span>&nbsp;',
                '#bill-modal',
                [
                    'title' => 'Добавить счет',
                    'class' => 'bill_modal',
                    'data-toggle' => 'modal',
                    'data-target' => '#bill-modal',
                    'data-pjax' => '0',
                ]
            );?>
        </div>
        <div class="panel-body">
            <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Инфо</h4>
                <p>Для внесения первоначального сальдо на счет, после его создания создайте новый приход с типом <strong>"Сальдо"</strong></p>
            </div>
            <?= $this->render('_search', [
                'model' => $searchModel,
            ]); ?>
            <div class="col-lg-12">
                <div class="table-responsive">
                    <?php Pjax::begin([
                        'id' => 'bls'
                    ]); ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'showFooter' => TRUE,
                        'footerRowOptions' => ['style'=>'font-weight:bold;text-decoration: underline;'],
                        'columns' => [
                            [
                                'class' => 'yii\grid\SerialColumn',
                                'footer'=> 'Итого',
                            ],
                            'title',
                            [
                                'attribute' => 'checking_account',
                                'content' => function($data){
                                    return $data->checking_account ? $data->checking_account : 'Не указан';
                                }
                            ],
                            [
                                'label' => 'Приходы',
                                'content' => function($data){
                                    $income = Yii::$app->formatter->asDecimal($data->total_incomes
                                        ? $data->total_incomes
                                        : 0,2);
                                    return Html::tag('span',$income,['class' => 'text-green']);
                                },
                                'footer'=> Yii::$app->formatter
                                        ->asDecimal(Summary::getTotalBalanceSum($dataProvider->models,'total_incomes'),2) . ' руб.',
                            ],
                            [
                                'label' => 'Расходы',
                                'content' => function($data){

                                    $expense = Yii::$app->formatter->asDecimal($data->total_expenses
                                        ? $data->total_expenses
                                        : 0,2);
                                    return Html::tag('span',$expense,['class' => 'text-red']);

                                },
                                'footer'=> Yii::$app->formatter
                                        ->asDecimal(Summary::getTotalBalanceSum($dataProvider->models,'total_expenses'),2) . ' руб.',
                            ],
                            [
                                'label' => 'Переводы',
                                'content' => function($data){

                                    $expense = Yii::$app->formatter->asDecimal($data->total_transferences
                                        ? $data->total_transferences
                                        : 0,2);
                                    return Html::tag('span',$expense,['class' => 'text-grey']);

                                },
                                'footer'=> Yii::$app->formatter
                                        ->asDecimal(Summary::getTotalBalanceSum($dataProvider->models,'total_transferences'),2) . ' руб.',
                            ],
                            [
                                'label' => 'Баланс',
                                'content' => function($data){
                                    return Yii::$app->formatter->asDecimal((int)$data->balance,2);
                                },
                                'footer'=> Yii::$app->formatter
                                        ->asDecimal(Summary::getTotalBalanceSum($dataProvider->models,'balance'),2) . ' руб.',
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'buttons' => [
                                    'delete' => function ($url, $model) {
                                        $custom_url = Yii::$app->getUrlManager()->createUrl([
                                            'company/bills/delete','id' => $model['id']
                                        ]);
                                        return Html::a(
                                            '<span class="btn btn-xs btn-default action-btn">
                                                <i class="fa fa-trash"></i>
                                                    <span class="btn-title">&nbsp;Удалить</span>
                                            </span>&nbsp;',
                                            $custom_url,
                                            ['title' => 'Удалить', 'class' => 'action-link', 'data-method' => 'post']
                                        );
                                    },
                                    'edit' => function ($url, $model) {
                                        $custom_url = Yii::$app->getUrlManager()->createUrl([
                                            'company/bills/edit','id' => $model['id']
                                        ]);
                                        return Html::a(
                                            '<span class="btn btn-xs btn-default action-btn">
                                                <i class="fa fa-edit"></i>
                                                    <span class="btn-title">&nbsp;Редактировать</span>
                                            </span>&nbsp;',
                                            $custom_url,
                                            ['title' => 'Редактировать', 'class' => 'action-link edit_bill', 'data-method' => 'post']
                                        );
                                    },
                                ],
                                'template' => '{delete}{edit}',
                            ],
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    'id' => 'bill-modal',
    'class' => 'modal',
    'size'=>'modal-sm',
    'footer' => ' &emsp;'
]); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <h4>Добавить новый счет</h4><br>
                <?= $this->render('_bill_form', [
                    'model' => $bill,
                ]); ?>
            </div>
        </div>
    </div>
<?php Modal::end()?>
