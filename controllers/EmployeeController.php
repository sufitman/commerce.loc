<?php

namespace app\controllers;

use app\models\Category;
use app\models\Coaching;
use app\models\Course;
use app\models\Group;
use app\models\GroupStudent;
use app\models\Homework;
use app\models\Material;
use app\models\Message;
use app\models\search\CourseSearch;
use app\models\search\GroupSearch;
use app\models\search\GroupStudentSearch;
use app\models\search\HomeworkSearch;
use app\models\search\MessageSearch;
use app\models\search\StudentCourseSearch;
use app\models\StudentCourse;
use app\models\user\BaseUser;
use app\models\UserTable;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;

class EmployeeController extends SiteController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'  => ['post'],
                    'confirm' => ['post'],
                    'block'   => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isEmployee();
                        },
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index',[
            'stat' => [],
        ]);
    }

}
