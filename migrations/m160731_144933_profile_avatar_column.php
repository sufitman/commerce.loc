<?php

use yii\db\Migration;

class m160731_144933_profile_avatar_column extends Migration
{
    public function up()
    {
        $this->addColumn('profile','avatar',$this->string());
        $this->dropColumn('reserve','deleted');
    }

    public function down()
    {
        $this->dropColumn('profile','avatar');
        $this->addColumn('reserve','deleted',$this->smallInteger(1));
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
