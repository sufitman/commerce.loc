<?php

namespace app\controllers;

use app\models\user\BaseUser;
use Yii;
use yii\db\ActiveRecord;
use yii\web\Response;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\widgets\ActiveForm;

class SiteController extends Controller
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testMe' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        if(Yii::$app->user->isGuest)
        {
            return $this->redirect(['user/login']);
        }else{
            return $this->redirect($this->goRoleHome());
        }
    }

    public function goRoleHome()
    {
        $user = Yii::$app->user->identity;
        /** @var $user \app\models\user\BaseUser*/
        return $user->getRoleHome();
    }

    protected function _findModel($modelName,$id,$has_company_property = true,$related_properties = [])
    {
        /** @var $modelName ActiveRecord */
        if (($model = $modelName::findOne($id)) !== null) {
            /** @var $user BaseUser */
            $user = Yii::$app->user->identity;
            $user_match = [$user->id,$user->company_id];
            $condition = $user->isAdmin();
            if($has_company_property)
                $condition = $condition || in_array($model->company_id,$user_match);
            else if(!empty($related_properties)){
                $sub_condition = true;
                foreach ($related_properties as $related_property){
                    $sub_condition = $sub_condition && in_array($model->$related_property->company_id, $user_match);
                }
                $condition = $condition || $sub_condition;
            }
            if($condition)
                return $model;
        }
        throw new NotFoundHttpException('Страницы не существует...');
    }

    protected function _validate($modelName, $multiple = false, $post = false, $scenario = false, $real_idx = false)
    {
        if(Yii::$app->request->isAjax){
            $result = [];
            $reflect = new \ReflectionClass($modelName);
            $shortName = $reflect->getShortName();
            if($multiple){

                foreach (Yii::$app->request->post()[$shortName] as $post)
                    $result += $this->_validate($modelName,false, [$shortName => $post], $scenario,$real_idx);
            }

            /** @var $model ActiveRecord*/

            $model = new $modelName();

            if($scenario) $model->setScenario($scenario);

            $post = $post ? $post :  Yii::$app->request->post();
            if ($model->load($post)) {
                Yii::$app->response->format = Response::FORMAT_JSON;
                $model_validation = ActiveForm::validate($model);
                foreach ($model_validation as $idx => $errors){
                    if($real_idx) {
                        $idx = explode('-',$idx,2);
                        $idx = $idx[0] . '-' . $post[$shortName]['idx'] . '-' . $idx[1];
                    }
                    $result[$idx] = $errors;
                }
            }

            return $result;
        }
        throw new NotFoundHttpException;
    }
}
