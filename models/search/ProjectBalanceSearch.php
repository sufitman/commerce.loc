<?php

namespace app\models\search;

use app\models\Summary;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Project;

/**
 * ProjectSearch represents the model behind the search form about `app\models\Project`.
 */
class ProjectBalanceSearch extends Project
{
    public $total_incomes;

    public $total_expenses;

    public $balance;

    public $profit;

    public $start_time;

    public $end_time;

    public function afterFind()
    {
        $filter = Yii::$app->request->getQueryParam('ProjectSearch',false);
        $this->start_time = $filter ? $filter['start_time'] : false;
        $this->end_time = $filter ? $filter['end_time'] : false;
        $this->total_incomes = Summary::getTotalIncomes(
            Yii::$app->user->id,
            false,
            $this->id,
            false,
            false,
            $this->start_time,
            $this->end_time
        )['total_sum'];
        $this->total_expenses = Summary::getTotalExpenses(
            Yii::$app->user->id,
            false,
            $this->id,
            false,
            $this->start_time,
            $this->end_time
        )['total_sum'];
        $this->balance = $this->total_incomes + $this->total_expenses;
        $this->profit = !empty($this->total_incomes) && !empty($this->total_expenses)
            ? ($this->total_incomes / ($this->total_expenses * -1))*100
            : 0;
        parent::afterFind();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'company_id'], 'integer'],
            [[
                'title',
                'description',
                'total_incomes',
                'total_expenses',
                'balance',
                'start_time',
                'end_time',
            ], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = self::find()->where([
            'and',[
                'or',
                ['company_id' => Yii::$app->user->identity->company_id],
                ['company_id' => Yii::$app->user->id],
            ],['deleted' => false]
        ]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
