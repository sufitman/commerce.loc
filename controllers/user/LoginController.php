<?php

namespace app\controllers\user;

use Yii;
use dektrium\user\controllers\SecurityController as BaseSecurityController;
use dektrium\user\models\LoginForm;

class LoginController extends BaseSecurityController
{
    /** @inheritdoc */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(Yii::$app->user->identity->getRoleHome());
        }

        /** @var LoginForm $model */
        $model = Yii::createObject(LoginForm::className());

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            Yii::$app->user->identity->last_sign_in = time();
            Yii::$app->user->identity->update();
            return $this->redirect(Yii::$app->user->identity->getRoleHome());
        }

        return $this->render('login', [
            'model' => $model,
            'module' => $this->module,
        ]);
    }
}
