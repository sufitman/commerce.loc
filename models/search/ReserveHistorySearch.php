<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ReserveHistory;
use yii\web\NotFoundHttpException;

/**
 * ReserveHistorySearch represents the model behind the search form about `app\models\ReserveHistory`.
 */
class ReserveHistorySearch extends ReserveHistory
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $reserve_id = Yii::$app->request->get('reserve_id',FALSE);
        if(!$reserve_id)
            throw new NotFoundHttpException();
        $this->reserve_id = $reserve_id;
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'reserve_id', 'datetime'], 'integer'],
            [['sum'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ReserveHistory::find()
            ->where(['reserve_id' => $this->reserve_id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'reserve_id' => $this->reserve_id,
            'sum' => $this->sum,
            'datetime' => $this->datetime,
        ]);

        return $dataProvider;
    }
}
