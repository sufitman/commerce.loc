<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\search\EmployeeSearch */
/* @var $form yii\widgets\ActiveForm */
/* @var $employees */

?>

<div class="student-course-search panel col-lg-12">
    <div class="panel-body">

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <div class="col-lg-10 col-md-10">
            <?= $form->field($model, 'employee_search')
                ->widget( \yii\jui\AutoComplete::className(),[
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'Имя или email',
                    ],
                    'clientOptions' => [
                        'source' => $employees,
                        'autoFill' => true,
                        'minLength' => '1',
                        'select' => new JsExpression("function( event, ui ) {
                                    $('#coachingsearch-coach_id').val(parseInt(ui.item.uid));
                                }"),
                        'response' => new JsExpression("function( event, ui ) {
                                    if(!ui.content.length){
                                        $('#coachingsearch-coach_id').val('');
                                    }
                                }"),
                    ],
                ])->label(false) ?>

            <?= $form->field($model, 'id')->hiddenInput()->label(false);?>
        </div>
        <?//= echo $form->field($model, 'payment_id') ?>

        <div class="col-lg-2 col-md-2">
            <div class="form-group btn-group btn-group-justified" role="group">
                <div class="btn-group" role="group">
                    <?= Html::submitButton('Поиск', ['class' => 'btn btn-grey']) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>

    </div>
</div>
