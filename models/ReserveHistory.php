<?php

namespace app\models;

use Yii;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "reserve_history".
 *
 * @property integer $id
 * @property integer $reserve_id
 * @property double $sum
 * @property integer $datetime
 *
 * @property Reserve $reserve
 */
class ReserveHistory extends \yii\db\ActiveRecord
{
    public $category_id;

    public $from_bill;

    public $description;
    /**
     * @inheritdoc
     */
//    public function init()
//    {
//        $reserve_id = Yii::$app->request->get('reserve_id',FALSE);
//
//        $this->reserve_id = $reserve_id;
//        parent::init();
//    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['debit'][] = 'category_id';
        $scenarios['debit'][] = 'from_bill';
        $scenarios['debit'][] = 'description';
        return $scenarios;
    }


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reserve_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reserve_id', 'datetime'], 'integer'],
            [['sum'],  'number'],
            [['sum'], 'required'],
            [['category_id','from_bill'], 'required', 'on' => 'debit'],
            [['description'], 'safe', 'on' => 'debit'],
            [['reserve_id'], 'exist', 'skipOnError' => true, 'targetClass' => Reserve::className(), 'targetAttribute' => ['reserve_id' => 'id']],
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->sum = str_replace(",", ".", $this->sum);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'reserve_id' => 'Reserve ID',
            'sum' => 'Сумма',
            'datetime' => 'Время',
            'category_id' => 'Категория',
            'from_bill' => 'Счет списания',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReserve()
    {
        return $this->hasOne(Reserve::className(), ['id' => 'reserve_id']);
    }
}
