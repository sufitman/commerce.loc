<?php

namespace app\models;
use app\models\user\BaseUser as User;
use Yii;

/**
 * This is the model class for table "expense".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $project_id
 * @property integer $created_by
 * @property integer $from_bill
 * @property integer $category_id
 * @property double $sum
 * @property string $description
 * @property integer $datetime
 *
 * @property User $createdBy
 * @property Bill $fromBill
 * @property Bill $theBill in order to calculate balance
 * @property Reserve $fromReserve
 * @property Project $project
 * @property User $company
 * @property Category $category
 */
class Expense extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expense';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['reserve'][] = 'from_reserve';
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'project_id', 'created_by', 'from_bill', 'category_id', 'datetime'], 'integer'],
            [['datetime','sum','category_id','description'], 'required'],
            [['from_bill'], 'required', 'except' => 'reserve'],
            [['from_reserve'], 'required', 'on' => 'reserve'],
            [['sum'],  'number'],
            [['description'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['from_bill'], 'exist', 'skipOnError' => true, 'targetClass' => Bill::className(), 'targetAttribute' => ['from_bill' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'project_id' => 'Проект',
            'created_by' => 'Созан',
            'from_bill' => 'Счет',
            'category_id' => 'Категория',
            'sum' => 'Сумма',
            'description' => 'Описание',
            'datetime' => 'Время',
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->sum = str_replace(",", ".", $this->sum);
            $this->sum *= -1;
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromBill()
    {
        return $this->hasOne(Bill::className(), ['id' => 'from_bill']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTheBill()
    {
        return $this->hasOne(Bill::className(), ['id' => 'from_bill']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromReserve()
    {
        return $this->hasOne(Reserve::className(), ['id' => 'from_reserve']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(User::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }
}
