<?php

namespace app\controllers\employee;

use app\controllers\SiteController;
use app\models\Bill;
use app\models\Category;
use app\models\Project;
use app\models\search\BalanceSearch;
use app\models\search\FundsBalanceSearch;
use app\models\search\ProjectBalanceSearch;
use app\models\search\ReserveBalanceSearch;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use app\models\user\BaseUser;

class StatisticsController extends SiteController
{
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'         => true,
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isCompany();
                        },
                    ],
                    [
                        'allow'         => true,
                        'actions'       => [
                            'balance',
                            'reserve-balance',
                            'funds',
                            'projects',
                        ],
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isCompany() ||  $identity->isAdmin();
                        },
                    ],
                    [
                        'allow'   => true,
                        'actions' => [],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }
    public function actionBalance()
    {
        $searchModel = new BalanceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,true);
        $uid = Yii::$app->user->identity->company_id;

        $projects = ArrayHelper::getColumn(Project::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('title')
            ->all(),'title');

        $bills = ArrayHelper::getColumn(Bill::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('title')
            ->all(),'title');

        $categories = ArrayHelper::getColumn(Category::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('title')
            ->all(),'title');

        $show_all = Yii::$app->request->get('show_all',false);
        if($show_all) {
            $dataProvider->pagination->pageSize=0;
        }

        return $this->render('balance/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'projects' => $projects,
            'categories' => $categories,
            'bills' => $bills,
            'show_all' => $show_all,
        ]);
    }

    public function actionReserveBalance()
    {
        $searchModel = new ReserveBalanceSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,true);
        $uid = Yii::$app->user->identity->company_id;

        $projects = ArrayHelper::getColumn(Project::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('title')
            ->all(),'title');

        $bills = ArrayHelper::getColumn(Bill::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('title')
            ->all(),'title');

        $categories = ArrayHelper::getColumn(Category::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('title')
            ->all(),'title');

        $show_all = Yii::$app->request->get('show_all',false);
        if($show_all) {
            $dataProvider->pagination->pageSize=0;
        }

        return $this->render('reserve-balance/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'projects' => $projects,
            'categories' => $categories,
            'bills' => $bills,
            'show_all' => $show_all,
        ]);
    }
}