<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ReserveHistory */
/* @var $form yii\widgets\ActiveForm */
/* @var $reserve_id  */

?>

<div class="reserve-history-form row">

    <?php $form = ActiveForm::begin([
        'action' => ['add','reserve_id' => $reserve_id],
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'validateOnChange' => true,
        'validationUrl' => ['validate-reserve-history'],
        'options' => [
            'class' => 'action-form add-form',
            'id' => 'add-reserve-history-action-' . $reserve_id,
        ],
    ]); ?>
    <div class="col-lg-9 col-md-9 col-sm-6">
        <?= $form->field($model, 'sum')->textInput([
            'maxlength' => true,
            'placeholder' => $model->getAttributeLabel( 'sum' )
        ])->label(false) ?>
    </div>


        <?= $form->field($model, 'reserve_id',[
            'options' => ['id' => 'add_reserve_history']
        ])->hiddenInput()->label(false) ?>

    <div class="col-lg-3 col-md-3 col-sm-6">
        <div class="form-group">
            <?= Html::submitButton('Добавить', ['class' => 'btn btn-success container-full']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
