<?php

namespace app\models;

use app\models\user\BaseUser;
use dektrium\user\models\Account;
use dektrium\user\models\Profile;
use dektrium\user\models\Token;
use yii\base\Model;
use yii\base\Object;
use Yii;
use yii\console\Exception;
use yii\db\ActiveRecord;
use yii\debug\models\search\Debug;
use yii\debug\models\search\Log;
use yii\helpers\Json;

class CompanyBackupFile extends Object
{
    public $fp;

    public $file_name;

    public $_path = '@runtime';

    public $back_temp_file = 'company_backup_';

    public static function instance()
    {
        return new self;
    }

    protected function getPath()
    {
        $this->_path = Yii::getAlias('@app/runtime') . '/_backup/';
        if (!file_exists($this->_path)) {
            mkdir($this->_path);
            chmod($this->_path, '777');
        }
        return $this->_path;
    }

    public function getUsers($company_id,$as_array = true)
    {
        return BaseUser::find()
            ->where(['or',['id' => $company_id],['company_id' => $company_id]])
            ->indexBy('id')
            ->asArray($as_array)
            ->all();
    }

    public function getProfiles($users_ids,$as_array = true)
    {
        return Profile::find()
            ->where(['in','user_id',$users_ids])
            ->indexBy('user_id')
            ->asArray($as_array)
            ->all();
    }

    public function getAccounts($users_ids,$as_array = true)
    {
        return Account::find()
            ->where(['in','user_id',$users_ids])
            ->indexBy('user_id')
            ->asArray($as_array)
            ->all();
    }

    public function getTokens($users_ids,$as_array = true)
    {
        return Token::find()
            ->where(['in','user_id',$users_ids])
            ->indexBy('user_id')
            ->asArray($as_array)
            ->all();
    }

    public function getBills($company_id,$as_array = true)
    {
        return Bill::find()
            ->where(['company_id' => $company_id])
            ->indexBy('id')
            ->asArray($as_array)
            ->all();
    }

    public function getTransferences($bills_ids,$as_array = true)
    {
        return Transference::find()
            ->where(['or',['in','from_bill',$bills_ids],['in','to_bill',$bills_ids]])
            ->indexBy('id')
            ->asArray($as_array)
            ->all();
    }

    public function getCategories($company_id,$as_array = true)
    {
        return Category::find()
            ->where(['company_id' => $company_id])
            ->indexBy('id')
            ->asArray($as_array)
            ->all();
    }

    public function getProjects($company_id,$as_array = true)
    {
        return Project::find()
            ->where(['company_id' => $company_id])
            ->indexBy('id')
            ->asArray($as_array)
            ->all();
    }

    public function getIncomings($company_id,$as_array = true)
    {
        return Incoming::find()
            ->where(['company_id' => $company_id])
            ->indexBy('id')
            ->asArray($as_array)
            ->all();
    }

    public function getExpenses($company_id,$as_array = true)
    {
        return Expense::find()
            ->where(['company_id' => $company_id])
            ->indexBy('id')
            ->asArray($as_array)
            ->all();
    }

    public function getReserves($company_id,$as_array = true)
    {
        return Reserve::find()
            ->where(['company_id' => $company_id])
            ->indexBy('id')
            ->asArray($as_array)
            ->all();
    }

    public function getReservesHistory($reserves_ids,$as_array = true)
    {
        return ReserveHistory::find()
            ->where(['in','reserve_id',$reserves_ids])
            ->indexBy('id')
            ->asArray($as_array)
            ->all();
    }

    public function getData($company_id)
    {
        $users = $this->getUsers($company_id);

        $users_ids = array_keys($users);

        $profiles = $this->getProfiles($users_ids);

        $social_accounts = $this->getAccounts($users_ids);

        $tokens = $this->getTokens($users_ids);

        $bills = $this->getBills($company_id);

        $bills_ids = array_keys($bills);

        $transferences = $this->getTransferences($bills_ids);

        $categories = $this->getCategories($company_id);

        $projects = $this->getProjects($company_id);

        $incomes = $this->getIncomings($company_id);

        $expenses = $this->getExpenses($company_id);

        $reserves = $this->getReserves($company_id);

        $reserves_ids = array_keys($reserves);

        $reserves_history = $this->getReservesHistory($reserves_ids);

        $backup = [
            'company_id' => $company_id,
            'User' => $users,
            'Profile' => $profiles,
            'Account' => $social_accounts,
            'Token' => $tokens,
            'Bill' => $bills,
            'Transference' => $transferences,
            'Category' => $categories,
            'Project' => $projects,
            'Incoming' => $incomes,
            'Expense' => $expenses,
            'Reserve' => $reserves,
            'ReserveHistory' => $reserves_history,
        ];
        return Json::encode($backup);
    }

    public function getBackupFile($filename)
    {
        return file_get_contents($this->getPath() . $filename);
    }

    protected function _restore_data($models,$restore_models,$modelClass,$pk_attr = 'id')
    {
        $restore_models_ids = array_keys($restore_models);
        foreach($restore_models as $pk => $restore_model){
            $model = $models[$pk];
            /** @var $model ActiveRecord*/

            if(!isset($models[$pk])){
                $model = new $modelClass();
            }
            $attributes = $model->attributes();
            $set = [];
            foreach($attributes as $attribute){
                $set[$attribute] = $restore_model[$attribute];
            }
            $model->setAttributes($set,false);
            $model->save(false);
        }
        foreach ($models as $model) {
            /** @var $model ActiveRecord*/
            if(!in_array($model->$pk_attr,$restore_models_ids)){
                $model->delete();
            }
        }
    }

    public function restoreCompanyBackup($filename)
    {
        try{
            $company_data = Json::decode($this->getBackupFile($filename));

            $company_id = $company_data['company_id'];

            $projects = $this->getProjects($company_id,false);

            $this->_restore_data($projects,$company_data['Project'],Project::className());

            $categories = $this->getCategories($company_id,false);
            $this->_restore_data($categories,$company_data['Category'],Category::className());

            $incomes = $this->getIncomings($company_id,false);
            $this->_restore_data($incomes,$company_data['Incoming'],Incoming::className());

            $expenses = $this->getExpenses($company_id,false);
            $this->_restore_data($expenses,$company_data['Expense'],Expense::className());

            $reserves = $this->getReserves($company_id,false);
            $this->_restore_data($reserves,$company_data['Reserve'],Reserve::className());

            $reserves_ids = array_keys($reserves);

            $reserves_history = $this->getReservesHistory($reserves_ids,false);
            $this->_restore_data($reserves_history,$company_data['ReserveHistory'],ReserveHistory::className());


            $bills = $this->getBills($company_id,false);
            $this->_restore_data($bills,$company_data['Bill'],Bill::className());

            $bills_ids = array_keys($bills);

            $transferences = $this->getTransferences($bills_ids,false);
            $this->_restore_data($transferences,$company_data['Transference'],Transference::className());


            $users = $this->getUsers($company_id,false);
            $this->_restore_data($users,$company_data['User'],BaseUser::className());

            $users_ids = array_keys($users);

            $profiles = $this->getProfiles($users_ids,false);
            $this->_restore_data($profiles,$company_data['Profile'],Profile::className(),'user_id');

            $social_accounts = $this->getAccounts($users_ids,false);
            $this->_restore_data($social_accounts,$company_data['Account'],Account::className());

            $tokens = $this->getTokens($users_ids,false);
            $this->_restore_data($tokens,$company_data['Token'],Token::className(),'user_id');

            return true;
        }catch (\yii\base\Exception $e){
            echo $e->getCode() . '<br>';
            echo $e->getMessage() . '<br>';
            echo $e->getName() . '<br>';
            echo $e->getFile() . '<br>';
            echo $e->getLine() . '<br>';
        }
        return false;
    }

    public function createCompanyBackup($company_id)
    {
        $filename = $company_id . '_' . time() . '.json';
        $this->file_name = $this->getPath() . $filename;
        $this->fp = fopen($this->file_name, 'w+');
        if ($this->fp == null)
            return false;

        $content = $this->getData($company_id);
        fwrite($this->fp, $content);
        fclose($this->fp);
        $this->fp = null;
        return $filename;
    }

    public function deleteBackupFile($filename)
    {
        unlink($this->getPath() . $filename);
    }
}