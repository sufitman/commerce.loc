<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model \app\models\Incoming */
/* @var $form yii\widgets\ActiveForm */
/* @var $projects array */
/* @var $bills array */
/* @var $categories array */

$this->registerCss(".btn-100{width:100%}");
$model->datetime = time();
?>

<div class="user-table-form">

    <? if(!empty($projects) && !empty($bills) && !empty($categories)) :?>
        <?php $form = ActiveForm::begin([
            'id' => 'add_expenses',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'validateOnChange' => true,
            'validationUrl' => ['validate'],
            'options' => [
                'class' => 'action-form',
            ],
        ]); ?>

        <div id="fields">

            <div class="users row">
                <div class="col-lg-6 col-md-6">
                    <?= $form->field($model, 'project_id')->dropDownList([NULL => 'Проект'] + $projects)->label(false)?>
                </div>

                <div class="col-lg-6 col-md-6">
                    <?= $form->field($model, 'from_bill')->dropDownList([NULL => 'Счет'] + $bills)->label(false)?>
                 </div>

                <div class="col-lg-6 col-md-6">
                    <?= $form->field($model, 'category_id')->dropDownList([NULL => 'Категория'] + $categories)->label(false)?>
                 </div>
                <div class="col-lg-6 col-md-6">
                    <?= $form->field($model, 'sum')->textInput([
                    'maxlength' => true,
                    'placeholder' => $model->getAttributeLabel( 'sum' )
                    ])->label(false)?>
                </div>
                <div class="col-lg-12 col-md-12">
                    <?= $form->field($model, 'datetime')->widget(DateControl::classname(), [
                        'ajaxConversion' => false,
                        'options' => [],
                        'type' => DateControl::FORMAT_DATETIME,
                        //'autoWidget' => false,
                        'displayFormat' => 'php:D, d-M-Y H:i:s',
                        'saveFormat' => 'php:U'
                    ])->label(false)?>

                 </div>
                <div class="col-lg-12 col-md-12">
                    <?= $form->field($model, 'description')->textarea([
                    'placeholder' => $model->getAttributeLabel( 'description' )
                    ])->label(false)?>
                </div>
                <div class="col-lg-12 col-md-12">
                    <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => 'btn btn-lg btn-100  btn-success']) ?>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    <? else :?>

        <div class="note note-warning">
            <p>Чтобы добавлять расходы в системе должны быть созданы по крайней мере по обному <a href="<?= Yii::$app->getUrlManager()->createUrl(['company/projects']);?>"><strong>проекту</strong></a>,
                <a href="<?= Yii::$app->getUrlManager()->createUrl(['company/bills']);?>"><strong>счету</strong></a> и <a href="<?= Yii::$app->getUrlManager()->createUrl(['company/categories']);?>"><strong>категории</strong></a></p>
        </div>

    <? endif ?>
</div>
