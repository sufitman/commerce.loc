<?php

use yii\db\Migration;

class m160805_055646_reserve_history_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('reserve_history',[
            'id' => $this->primaryKey(),
            'reserve_id' => $this->integer(),
            'sum' => $this->double(),
            'datetime' => $this->integer(),
        ],$tableOptions);

        $this->createIndex('reserve_idx','reserve_history','reserve_id');
        $this->addForeignKey('history_reserve_fk','reserve_history','reserve_id','reserve','id','CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('history_reserve_fk','reserve_history');
        $this->dropIndex('reserve_idx','reserve_history');
        $this->dropTable('reserve_history');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
