<?php

namespace app\controllers\employee;

use app\controllers\SiteController;
use app\models\Project;
use app\models\search\ProjectSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\user\BaseUser;

class ProjectsController extends SiteController
{
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'      => ['post'],
                    'edit'     => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'         => true,
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isEmployee();
                        },
                    ],
                    [
                        'allow'   => true,
                        'actions' => [],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ProjectSearch();
        $params = Yii::$app->request->queryParams;
        $uid = Yii::$app->user->identity->company_id;
        $dataProvider = $searchModel
            ->search($params);

        $project = new Project();

        $request = Yii::$app->request;

        $loaded = $project->load($request->post());

        if($loaded && $request->isAjax) {
            $project->company_id = $uid;
            $project->save();

            Yii::$app->end();
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'project' => $project,
        ]);
    }

    public function actionValidate(){
        return $this->_validate(Project::className());
    }
}