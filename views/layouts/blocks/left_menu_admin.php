<?
$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;

$uid = Yii::$app->user->id;
?>
<aside class="main-sidebar">

	<section class="sidebar">
		<ul class="sidebar-menu">

			<li <?= ($controller == 'company' && $action == 'index') ? ' class="active"' :'';?>>
				<a href="/">
					<i class="fa fa-dashboard"></i> <span>Главная</span>
				</a>
			</li>

		</ul>
	</section>
</aside>