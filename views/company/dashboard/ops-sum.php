<?php
use dosamigos\chartjs\ChartJs;
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 10.08.2016
 * Time: 22:28
 * @var $chart array
 */
?>
<div class="box-header with-border">Суммы операций за неделю</div>
<div class="box-body">
    <?= ChartJs::widget([
        'type' => 'line',
        'id' => 'a',
        'options' => [
            'height' => 300,
            'width' => 651,
        ],
        'data' => [
            'labels' => $chart['days'],
            'datasets' => [
                [
                    'label' => 'Сумма приходов',
                    'backgroundColor' => "rgba(0,166,90,0.5)",
                    'borderColor' => "rgba(0,166,90, 1)",
                    'pointColor' => "rgba(220,220,220,1)",
                    'data' => $chart['incomes_sum']
                ],
                [
                    'label' => 'Сумма расходов',
                    'backgroundColor' => "rgba(221,75,57,0.5)",
                    'borderColor' => "rgba(221,75,57,1)",
                    'pointColor' => "rgba(220,220,220,1)",
                    'data' => $chart['expenses_sum']
                ],
                [
                    'label' => 'Баланс',
                    'backgroundColor' => "rgba(0,115,183,0.5)",
                    'borderColor' => "rgba(0,115,183,1)",
                    'pointColor' => "rgba(220,220,220,1)",
                    'data' => $chart['balance']
                ],
            ]
        ]
    ]);
    ?>
</div>
<div class="box-footer text-center">
    <a class="chart-sum" href="<?= Yii::$app->urlManager->createUrl([
        'company/dashboard/get-operations-sum',
        'period' => 7
    ])?>">За неделю</a> / <a class="chart-sum" href="<?= Yii::$app->urlManager->createUrl([
        'company/dashboard/get-operations-sum',
        'period' => 30
    ])?>">За месяц</a> / <a class="chart-sum" href="<?= Yii::$app->urlManager->createUrl([
        'company/dashboard/get-operations-sum',
        'period' => 183
    ])?>">За пол года</a>
</div>
