<?php

namespace app\models\search;

use app\models\Expense;
use app\models\Incoming;
use app\models\Reserve;
use app\models\Transference;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;

/**
 * IncomingSearch represents the model behind the search form about `app\models\Incoming`.
 */
class FundsBalanceSearch extends Incoming
{
    /**
     * @var $operation_id
     * has to be in result for actions
     */
    public $operation_id;

    /**
     * @var $from_bill
     * has to be in table after Expense join
     */
    public $from_bill;

    /**
     * @var $theBill
     * created to output $from_bill or $to_bill Bill title
     */
    public $theBill;

    /**
     * @var $category_id
     * has to be in table after Expense join
     */
    public $category_id;

    /**
     * @var $category_title
     * created to output $category_id Category title
     */
    public $category_title;

    /**
     * @var $user_name
     * created to output $createdBy full name
     */
    public $user_name;

    /**
     * @var $project_title
     * created to output $project_id Project title
     */
    public $project_title;

    /**
     * @var $balance_sum
     * created to output $sum depending on whether income it is or expense
     */
    public $balance_sum;

    /**
     * @var $type
     * created to output whether income it is or expense
     */
    public $type;

    public $min_sum;

    public $max_sum;

    public $start_time;

    public $end_time;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[
                'balance_sum',
                'min_sum',
                'max_sum',
            ], 'number'],
            [[
                'description',
                'type',
                'theBill',
                'datetime',
                'user_name',
                'project_title',
                'category_title',
                'balance_sum',
                'min_sum',
                'max_sum',
                'start_time',
                'end_time',
            ], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return parent::attributeLabels() + [
            'project_title'  => 'Проект',
            'user_name'      => 'Оформил',
            'theBill'       => 'Счета ',
            'category_title' => 'Категория (Только для расходов)',
            'balance_sum'    => 'Сумма',
            'type'           => 'Тип',
            'min_sum'        => 'Сумма от',
            'max_sum'        => 'Сумма до',
            'start_time'     => 'Начиная с...',
            'end_time'       => 'Заканчивая...',
        ];
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $incomes = Incoming::find()
            ->select([new Expression(Incoming::tableName() . '.id AS operation_id,
                 project.title AS project_title, 
                 CONCAT(user.first_name, " ", user.second_name) as user_name,
                 sum AS balance_sum, 
                 incoming.description, 
                 incoming.datetime,
                 category.title AS category_title,
                 bill.title AS theBill,
                 comment,
                 "+" AS type')])
            ->where([
                'and',
                [
                    'or',
                    ['created_by' => Yii::$app->user->id],
                    ['incoming.company_id' => Yii::$app->user->id],
                ],
                ['is_balance' => false]
            ])
            ->joinWith(['project','createdBy','theBill','category']);

        $incomes_bal = Incoming::find()
            ->select([new Expression(Incoming::tableName() . '.id AS operation_id,
                 project.title AS project_title, 
                 CONCAT(user.first_name, " ", user.second_name) as user_name,
                 sum AS balance_sum, 
                 incoming.description, 
                 incoming.datetime,
                 category.title AS category_title,
                 bill.title AS theBill,
                 comment,
                 "*" AS type')])
            ->where([
                'and',
                [
                    'or',
                    ['created_by' => Yii::$app->user->id],
                    ['incoming.company_id' => Yii::$app->user->id],
                ],
                ['is_balance' => true]
            ])
            ->joinWith(['project','createdBy','theBill','category']);

        $expenses = Expense::find()
            ->select(new Expression(Expense::tableName() . '.id AS operation_id,
                project.title AS project_title, 
                CONCAT(user.first_name, " ", user.second_name) as user_name,
                sum AS balance_sum, 
                expense.description,
                expense.datetime,
                category.title AS category_title,
                bill.title AS theBill, 
                "-" AS comment, 
                "-" AS type'))
            ->where([
                'or',
                ['created_by' => Yii::$app->user->id],
                ['expense.company_id' => Yii::$app->user->id],
            ])
            ->joinWith(['project','createdBy','theBill','category']);

        $out_transferences = Transference::find()
            ->select(new Expression(Transference::tableName() . '.id AS operation_id,
                "-" AS project_title, 
                "-" as user_name,
                (sum * -1) AS balance_sum, 
                "-" AS description,
                transference.datetime,
                "-" AS category_title,
                bill.title AS theBill, 
                transference.comment, 
                "->" AS type'))
            ->where([
                'or',
                ['bill.company_id' => Yii::$app->user->id],
                ['bill.company_id' => Yii::$app->user->identity->company_id],
            ])
            ->joinWith(['fromBill']);

        $in_transferences = Transference::find()
            ->select(new Expression(Transference::tableName() . '.id AS operation_id,
                "-" AS project_title, 
                "-" as user_name,
                sum AS balance_sum, 
                "-" AS description,
                transference.datetime,
                "-" AS category_title,
                bill.title AS theBill, 
                transference.comment, 
                "<-" AS type'))
            ->where([
                'or',
                ['bill.company_id' => Yii::$app->user->id],
                ['bill.company_id' => Yii::$app->user->identity->company_id],
            ])
            ->joinWith(['toBill'])
            ->union($incomes,true)
            ->union($incomes_bal,true)
            ->union($expenses,true)
            ->union($out_transferences,true);

        $query = new Query();
        $query->select([
            'operation_id',
            'project_title',
            'user_name',
            'category_title',
            'balance_sum',
            'description',
            'datetime',
            'theBill',
            'comment',
            'type',
        ])
            ->from(['balance' => $in_transferences]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        Yii::$container->set(\yii\data\Sort::className(),[
            'defaultOrder' => [
                'datetime' => SORT_DESC,
            ],
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'datetime',
                'project_title',
                'user_name',
                'category_title',
                'balance_sum',
                'theBill',
                'description',
                'type',
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'createdBy'     => $this->createdBy,
            'theBill'      => $this->theBill,
            'type'          => $this->type,
        ]);


        $query->andFilterWhere(['in', 'project_title', $this->project_title])
            ->andFilterWhere(['in', 'user_name', $this->user_name])
            ->andFilterWhere(['in', 'category_title', $this->category_title   ])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['>=', 'balance_sum', $this->min_sum])
            ->andFilterWhere(['<=', 'balance_sum', $this->max_sum])
            ->andFilterWhere(['>=', 'datetime', $this->start_time])
            ->andFilterWhere(['<=', 'datetime', $this->end_time]);

        return $dataProvider;
    }

}
