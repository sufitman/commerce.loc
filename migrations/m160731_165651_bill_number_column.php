<?php

use yii\db\Migration;

class m160731_165651_bill_number_column extends Migration
{
    public function up()
    {
        $this->addColumn('bill','checking_account',$this->integer());
    }

    public function down()
    {
        $this->dropColumn('bill','checking_account');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
