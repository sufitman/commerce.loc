<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datecontrol\DateControl;
use \kartik\switchinput\SwitchInput;

/* @var $this yii\web\View */
/* @var $model \app\models\Incoming */
/* @var $form yii\widgets\ActiveForm */
/* @var $projects array */
/* @var $bills array */
/* @var $categories array  */

$this->registerCss("
.btn-100{width:100%}
/*.bill{
    float:right;
    -webkit-transition: all 0.5s ease;
    -moz-transition: all 0.5s ease;
    -o-transition: all 0.5s ease;
    transition: all 0.5s ease;
}*/
");


$model->datetime = time();
?>

<div class="user-table-form">

    <? if(!empty($projects) && !empty($bills)) :?>
        <?php $form = ActiveForm::begin([
            'id' => 'add_incoming',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'validateOnChange' => true,
            'validationUrl' => ['validate'],
            'options' => [
                'class' => 'action-form',
            ],

        ]); ?>

        <div id="fields">

            <div class="users row">
                <div class="alert alert-info alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h4><i class="icon fa fa-info"></i> Инфо</h4>
                    <p>Если приходу причислен тип <strong>"Сальдо"</strong>, то он не будет учитываться в списке приходов. Этот тип фигурирует в рассчетах только текущего состояния счета.</p>
                </div>
                <div class="col-lg-12 text-right">
                <?= $form->field($model, 'is_balance',['options' => ['id' => 'is_balance']])
                    ->widget(SwitchInput::classname(), [
                        'pluginOptions' => [
                            'onText' => 'Сальдо',
                            'offText' => 'Приход',
                        ],
                        'pluginEvents' => [
                            "switchChange.bootstrapSwitch" => "function() { 
                                var hidden = $('#is_balance').find('[type=\"hidden\"]');
                                var is_checked = $(this).prop('checked');
                                var bill = $('.bill');
                                hidden.val(is_checked ? 1 : 0);   
                                $('.not-is-balance').animate({opacity: 'toggle',height: 'toggle'});
                                /*bill.removeClass(is_checked ? 'col-lg-6 col-md-6' : 'col-lg-12 col-md-12');
                                bill.addClass(is_checked ? 'col-lg-12 col-md-12' : 'col-lg-6 col-md-6');*/
                            }",
                        ]
                    ])
                    ->label(false);?>
                </div>
                <div class="col-lg-6 col-md-6 not-is-balance">
                    <?= $form->field($model, 'category_id')->dropDownList([NULL => 'Категория'] + $categories,[
                        'disabled' => empty($categories) ? 'disabled' : false
                    ])->label(false)?>
                </div>

                <div class="col-lg-6 col-md-6 not-is-balance">
                    <?= $form->field($model, 'project_id')->dropDownList([NULL => 'Проект'] + $projects)->label(false)?>
                </div>

                <div class="col-lg-6 col-md-6 bill">
                    <?= $form->field($model, 'to_bill')->dropDownList([NULL => 'На счет'] + $bills)->label(false)?>
                 </div>
                <div class="col-lg-6 col-md-6">
                    <?= $form->field($model, 'sum')->textInput([
                    'maxlength' => true,
                    'placeholder' => $model->getAttributeLabel( 'sum' )
                    ])->label(false)?>
                </div>
                <div class="col-lg-12 col-md-12">
                    <?= $form->field($model, 'datetime')->widget(DateControl::classname(), [
                        'ajaxConversion' => false,
                        'options' => [],
                        'type' => DateControl::FORMAT_DATETIME,
                        //'autoWidget' => false,
                        'displayFormat' => 'php:D, d-M-Y H:i:s',
                        'saveFormat' => 'php:U'
                    ])->label(false)?>

                 </div>
                <div class="col-lg-12 col-md-12">
                    <?= $form->field($model, 'description')->textarea([
                    'placeholder' => $model->getAttributeLabel( 'description' )
                    ])->label(false)?>
                </div>
                <div class="col-lg-12 col-md-12">
                    <?= $form->field($model, 'comment')->textarea([
                    'placeholder' => $model->getAttributeLabel( 'comment' )
                    ])->label(false)?>
                </div>
                <div class="col-lg-12 col-md-12">
                    <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Обновить', ['class' => 'btn btn-lg btn-100  btn-success']) ?>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    <? else :?>

        <div class="note note-warning">
            <p>Чтобы добавлять приходы нужно создать по крайней мере один
                <?= (empty($projects))
                    ? ('<a href="' .
                        Yii::$app->getUrlManager()->createUrl(['company/projects']) .
                        '"><strong>проект</strong></a>')
                    : ('<a href="' .
                        Yii::$app->getUrlManager()->createUrl(['company/bills']) .
                        '"><strong>счет</strong></a>') ?>
            </p>
        </div>

    <? endif ?>
</div>
