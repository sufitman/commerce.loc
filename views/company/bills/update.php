<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Backup */

$this->title = 'Обновить счет: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Счета', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="user-table-index">

    <div class="panel panel-primary">
        <div class="panel-heading">Редактировать счет</div>
        <div class="panel-body">
            <div class="col-lg-offset-3 col-lg-6 col-md-offset-3 col-md-6">
                <?= $this->render('_bill_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
</div>
