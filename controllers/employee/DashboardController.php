<?php

namespace app\controllers\employee;

use app\controllers\SiteController;

use app\models\Bill;
use app\models\Project;
use app\models\Summary;
use Yii;
use yii\filters\AccessControl;
use app\models\user\BaseUser;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class DashboardController extends SiteController
{
    /** @inheritdoc */
    public function behaviors()
    {
        return [

            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'         => true,
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isEmployee();
                        },
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $incomes = Summary::getTotalIncomes(false,false,false,false,false,false,false,true); // always positive
        $day_incomes = Summary::getTotalIncomes(false,false,false,1,false,false,false,true); // always positive
        $expenses = Summary::getTotalExpenses(false,false,false,false,false,false,true); // always negative
        $day_expenses = Summary::getTotalExpenses(false,false,false,1,false,false,true); // always negative
        $reserves = Summary::getTotalReserves(false,true);

        return $this->render('index',[
            'stat' => [
                'incomes_sum' => $incomes['total_sum'],
                'day_incomes_sum' => $day_incomes['total_sum'],
                'expenses_sum' => $expenses['total_sum'],
                'day_expenses_sum' => $day_expenses['total_sum'],
                'reserves_sum' => $reserves['total_sum'],
                'balance' => $incomes['total_sum'] + $expenses['total_sum'],
            ],

        ]);
    }

}