<?php

namespace app\models;
use app\models\user\BaseUser;
use Yii;
use yii\db\Expression;

/**
 * summary model class
 */
class Summary
{
    public static function getTotalBalanceSum($items, $fieldName)
    {
        $total=0;
        foreach($items as $item){
            $total += $item[$fieldName];
        }
        return $total;
    }

    public static function getTotalIncomes($user_id = false,$to_bill = false,$project_id = false,$period = false,$is_balance = false,$start = false,$end = false,$is_employee = false)
    {
        $where = self::getOperationCondition($user_id,$to_bill,'to',false,$is_employee);

        if($to_bill) $where = ['and',['to_bill' => $to_bill],$where];
        if($project_id) $where = ['and',['project_id' => $project_id],$where];
        if($period) $where = ['and',['>=','datetime',strtotime(-1*$period . ' days')],$where];
        if($start) $where = ['and',['>=','datetime',$start],$where];
        if($end) $where = ['and',['<=','datetime',$end],$where];
        if(!$is_balance) $where = ['and',['is_balance' => false],$where];
        $incomes = Incoming::find()
            ->select([new Expression('SUM(sum) AS total_sum,COUNT(id) AS incomes_count')])
            ->where($where)
            ->asArray(true)
            ->all();
        return $incomes[0];
    }

    public static function getTotalExpenses($user_id = false,$from_bill = false,$project_id = false,$period = false,$start = false,$end = false,$is_employee = false)
    {
        $where = self::getOperationCondition($user_id,$from_bill,'from',false,$is_employee);
        if($project_id) $where = ['and',['project_id' => $project_id],$where];
        if($period) $where = ['and',['>=','datetime',strtotime(-1*$period . ' days')],$where];
        if($start) $where = ['and',['>=','datetime',$start],$where];
        if($end) $where = ['and',['<=','datetime',$end],$where];
        $expenses = Expense::find()
            ->select([new Expression('SUM(sum) AS total_sum,COUNT(id) AS expenses_count')])
            ->where($where)
            ->asArray(true)
            ->all();
        return $expenses[0];
    }

    public static function getTotalReserves($user_id = false,$is_employee = false)
    {
        $where = self::getOperationCondition($user_id,false,false,true,$is_employee);

        $expenses = Reserve::find()
            ->select([new Expression('SUM(sum) AS total_sum,COUNT(id) AS reserves_count')])
            ->where($where)
            ->asArray(true)
            ->all();
        return $expenses[0];
    }

    public static function getUsers($user_id = false)
    {
        $where = self::getAccountCondition($user_id);
        $users = BaseUser::find()
            ->select([new Expression('COUNT(id) AS users_count')])
            ->where($where)
            ->asArray(true)
            ->all();
        return $users[0];
    }

    public static function getProjects($user_id = false)
    {
        $where = self::getAccountCondition($user_id,true);
        $projects = Project::find()
            ->select([new Expression('COUNT(id) AS projects_count')])
            ->where($where)
            ->asArray(true)
            ->all();
        return $projects[0];
    }

    public static function getBills($user_id = false)
    {
        $where = self::getAccountCondition($user_id,true);
        $bills = Bill::find()
            ->select([new Expression('COUNT(id) AS bills_count')])
            ->where($where)
            ->asArray(true)
            ->all();
        return $bills[0];
    }

    protected static function getOperationCondition($user_id = false,$bill_id = false,$vector = 'to',$deleted = false,$is_employee = false)
    {
        if(!$user_id) $user_id = !is_null(Yii::$app->user->identity->company_id)
            ? Yii::$app->user->identity->company_id
            : Yii::$app->user->id;
        if(!$is_employee)
            $where = [
                'or',
                ['created_by' => $user_id],
                ['company_id' => $user_id],
            ];
        else
            $where = ['created_by' => Yii::$app->user->id];
        if($bill_id) $where = ['and',[ $vector . '_bill' => $bill_id],$where];
        if($deleted) $where = ['and',['deleted' => false],$where];

        return $where;
    }

    protected static function getAccountCondition($user_id = false,$deleted = false)
    {
        $deleted_key = $deleted ? 'deleted' : 'blocked_at';
        if(!$user_id) {
            $user_id = !is_null(Yii::$app->user->identity->company_id)
                ? Yii::$app->user->identity->company_id
                : Yii::$app->user->id;
        }else{
            $user = BaseUser::findOne($user_id);
            $user_id = !is_null($user->company_id)
                ? $user->company_id
                : $user->id;
        }
        $where = [
            'AND',
            ['company_id' => $user_id],
            [$deleted_key => $deleted ? false : NULL]
        ];
        return $where;
    }

    public static function getLastDays($days)
    {
        $dates = [];
        for ($i = 1; $i <= $days; $i++){
            $dates[] = date('d.m',strtotime(-1*($days - $i) . 'days'));
        }
        return $dates;
    }

    public static function getLastDaysIncomes($period,$part = false)
    {
        $incomes = [];
        $user = Yii::$app->user->identity;
        $user_id = $user->company_id ? $user->company_id : $user->id;

        for ($i = 1; $i <= $period; $i++){
            $where = self::getOperationCondition($user_id);
            $where = ['and', ['is_balance' => false], $where];
            $datetime_from = strtotime(-1 * ($period - $i) . ' days');
            $datetime_to = strtotime(-1 * ($period - $i - 1) . ' days');
            $where = [
                'and',
                ['between','datetime',$datetime_from,$datetime_to],
                $where
            ];
            $expression = !$part
                ? 'COALESCE(SUM(sum),0) AS total_sum,COUNT(id) AS expenses_count'
                : $part == 'incomes_count'
                    ? 'COUNT(id) AS incomes_count'
                    : 'COALESCE(SUM(sum),0) AS total_sum';
            $income = Incoming::find()
                ->select([new Expression($expression)])
                ->where($where)
                ->asArray(true)
                ->all();
            if(!$part) {
                $incomes['incomes_count'][] = (int)$income[0]['incomes_count'];
                $incomes['total_sum'][] = (int)$income[0]['total_sum'];
            }else{
                $incomes[$part][] = (int) $income[0][$part];
            }
        }
        return !$part ? $incomes : $incomes[$part];
    }

    public static function getLastDaysExpenses($period,$part = false)
    {
        $expenses = [];
        $user = Yii::$app->user->identity;
        $user_id = $user->company_id ? $user->company_id : $user->id;

        for ($i = 1; $i <= $period; $i++){
            $where = self::getOperationCondition($user_id);
            $datetime_from = strtotime(-1 * ($period - $i) . ' days');
            $datetime_to = strtotime(-1 * ($period - $i - 1) . ' days');
            $where = [
                'and',
                ['between','datetime',$datetime_from,$datetime_to],
                $where
            ];

            $expression = !$part
                ? 'COALESCE(SUM(sum),0) AS total_sum,COUNT(id) AS expenses_count'
                : $part == 'expenses_count'
                    ? 'COUNT(id) AS expenses_count'
                    : 'COALESCE(SUM(sum),0) AS total_sum';

            $expense = Expense::find()
                ->select([new Expression($expression)])
                ->where($where)
                ->asArray(true)
                ->all();
            if(!$part){
                $expenses['expenses_count'][] = (int) $expense[0]['expenses_count'];
                $expenses['total_sum'][] = (int) $expense[0]['total_sum'];
            }else{
                $expenses[$part][] = (int) $expense[0][$part];
            }

        }
        return !$part ? $expenses : $expenses[$part];
    }

    /**
     * @param $bills array
     * @return array
     */
    public static function getBillsTransferences($bills,$start_time = false,$end_time = false)
    {
        $bills_ids = array_keys($bills);

        $where = ['or',['in','from_bill',$bills_ids],['in','to_bill',$bills_ids]];
        if($start_time) $where = ['and',['>=','datetime', $start_time],$where];
        if($end_time) $where = ['and',['<=','datetime', $end_time],$where];

        $transferences = Transference::find()
            ->select(['sum','to_bill','from_bill'])
            ->where($where)
            ->asArray(true)
            ->all();

        $bills_transference = [];
        foreach ($transferences as $transference){
            if(empty($bills_transference[$bills[$transference['from_bill']]]))
                $bills_transference[$bills[$transference['from_bill']]] = [
                    'plus' => 0,
                    'minus' => 0,
                ];
            if(empty($bills_transference[$bills[$transference['to_bill']]]))
                $bills_transference[$bills[$transference['to_bill']]] = [
                    'plus' => 0,
                    'minus' => 0,
                ];
            $bills_transference[$bills[$transference['from_bill']]]['minus'] += $transference['sum'];
            $bills_transference[$bills[$transference['to_bill']]]['plus'] += $transference['sum'];
        }
        return $bills_transference;
    }

}
