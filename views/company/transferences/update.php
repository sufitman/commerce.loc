<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Backup */
/* @var $bills array */

$this->title = 'Обновить перевод: #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Переводы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => '#' .$model->id];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="user-table-index">

    <div class="panel panel-primary">
        <div class="panel-heading">Редактировать перевод</div>
        <div class="panel-body">
            <div class="col-lg-offset-2 col-lg-8 col-md-offset-2 col-md-8">
                <?= $this->render('_transference_form', [
                    'model' => $model,
                    'bills' => $bills,
                ]) ?>
            </div>
        </div>
    </div>
</div>
