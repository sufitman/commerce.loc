<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\BackupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $company_id  */

$this->title = 'Бэкапы';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs(' 
    $(document).on("click", ".action-link",function(e) {
        e.preventDefault();
        var url = $(this).attr("href");
        $.ajax({
            url:url,
            success: function(){
                 $.pjax.reload({container:"#bckps", async:true});
            }
        });
    });
')

?>
<div class="expense-index">
    <div class="panel panel-primary">
        <div class="panel-heading">Бэкапы системы
            <?=Html::a(
                '<span class="btn btn-xs btn-default pull-right">
                    <i class="fa fa-refresh"></i>
                        <span class="btn-title">&nbsp;Сделать бэкап</span>
                </span>&nbsp;',
                ['create','company_id' => $company_id],
                [
                    'title' => 'Сделать бэкап',
                    'id' => 'create-backup',
                    'class' => 'action-link',
                    'onclick' => 'confirm("Добавляя новый бэкап, система уничтожит наиболее ранний. Вы уверены?")'
                ]
            );?>
        </div>
        <div class="panel-body">
            <div class="alert alert-info alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-info"></i> Инфо</h4>
                <p>В целях безопасности система поддерживает сохранение <strong>не более <?=\app\models\Backup::BACKUP_COUNT?>
                резервных копий</strong> данных вашей компании.</p>
                <p>Каждая последующая резервная копия уничтожает наиболее раннюю.</p>
                <p>Система автоматически резервирует данные не чаще 4 раз в день.</p>
            </div>
            <?php Pjax::begin(['id' => 'bckps']); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        [
                            'attribute' => 'datetime',
                            'value' => function($data){
                                return date('d.m.Y H:i',$data['datetime']);
                            }
                        ],
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'buttons' => [
                                'restore' => function ($url, $model) {
                                    $custom_url = Yii::$app->getUrlManager()->createUrl([
                                        'company/backups/restore','id' => $model['id']
                                    ]);
                                    return Html::a(
                                        '<span class="btn btn-xs btn-default action-btn">
                                            <i class="fa fa-arrow-circle-down"></i>
                                                <span class="btn-title">&nbsp;Восстановить</span>
                                        </span>&nbsp;',
                                        $custom_url,
                                        [
                                            'title' => 'Восстановить',
                                            'class' => 'action-link',
                                            'onclick' => 'confirm("Все данные компании, добавленные после данного бэкапа будут удалены из системы. Вы уверены?")'
                                        ]
                                    );
                                },
                            ],
                            'template' => '{restore}',
                        ],
                    ],
                ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>