<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model \app\models\Category */
/* @var $form yii\widgets\ActiveForm */

$this->registerCss(".btn-100{width:100%}");
?>

<div class="user-table-form">

    <?php $form = ActiveForm::begin([
        'id' => 'add_category',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'validateOnChange' => true,
        'validationUrl' => ['validate'],
        'options' => [
            'class' => 'action-form',
        ],
    ]); ?>

    <div id="fields">

        <div class="users row">

            <div class="col-lg-12 text-center">
                <?= $form->field($model, 'type',['options' => ['id' => 'type']])
                    ->widget(\kartik\switchinput\SwitchInput::classname(), [
                        'pluginOptions' => [
                            'onText' => 'Приход',
                            'offText' => 'Расход',
                        ],
                        'pluginEvents' => [
                            "switchChange.bootstrapSwitch" => "function() { 
                                var hidden = $('#type').find('[type=\"hidden\"]');
                                var is_checked = $(this).prop('checked');
                                hidden.val(is_checked ? 1 : 0);   
                            }",
                        ]
                    ])
                    ->label(false);?>
            </div>

            <div class="col-lg-12 col-md-12">
                <?= $form->field($model, 'title')->textInput([
                'maxlength' => true,
                'placeholder' => $model->getAttributeLabel( 'title' )
                ])->label(false)?>
            </div>

            <div class="col-lg-12 col-md-12">
                <?= Html::submitButton('Добавить', ['class' => 'btn btn-100  btn-success']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
