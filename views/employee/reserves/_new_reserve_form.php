<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model \app\models\Incoming */
/* @var $form yii\widgets\ActiveForm */
/* @var $projects array */

$this->registerCss(".btn-100{width:100%}");

$model->datetime = time();
?>

<div class="user-table-form">

    <? if(!empty($projects)) :?>
        <?php $form = ActiveForm::begin([
            'id' => 'add_reserve',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'validateOnChange' => true,
            'validationUrl' => ['validate'],
            'options' => [
                'class' => 'action-form',
                'id' => 'add_reserve',
            ],
        ]); ?>

        <div id="fields">

            <div class="users row">
                <div class="col-lg-6 col-md-6">
                    <?= $form->field($model, 'project_id')->dropDownList([NULL => 'Проект'] + $projects)->label(false)?>
                </div>

                <div class="col-lg-6 col-md-6">
                    <?= $form->field($model, 'sum')->textInput([
                    'maxlength' => true,
                    'placeholder' => $model->getAttributeLabel( 'sum' )
                    ])->label(false)?>
                </div>

                <div class="col-lg-12 col-md-12">
                    <?= $form->field($model, 'datetime')->widget(DateControl::classname(), [
                        'ajaxConversion' => false,
                        'options' => [],
                        'type' => DateControl::FORMAT_DATETIME,
                        //'autoWidget' => false,
                        'displayFormat' => 'php:D, d-M-Y H:i:s',
                        'saveFormat' => 'php:U'
                    ])->label(false)?>

                 </div>
                <div class="col-lg-12 col-md-12">
                    <?= $form->field($model, 'description')->textarea([
                    'placeholder' => $model->getAttributeLabel( 'description' )
                    ])->label(false)?>
                </div>

                <div class="col-lg-12 col-md-12">
                    <?= Html::submitButton('Добавить', ['class' => 'btn btn-lg btn-100  btn-success']) ?>
                </div>
            </div>
        </div>

        <?php ActiveForm::end(); ?>

    <? else :?>

        <div class="note note-warning">
            <p>Чтобы добавлять резервы нужно создать по крайней мере один <a href="<?= Yii::$app->getUrlManager()->createUrl(['company/projects']);?>"><strong>проект</strong></a></p>
        </div>

    <? endif ?>
</div>
