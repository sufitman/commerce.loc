<?php

use yii\db\Migration;

class m160816_194815_drop_from_column extends Migration
{
    public function up()
    {
        $this->dropColumn('incoming','from');
    }

    public function down()
    {
        $this->addColumn('incoming','from',$this->string());
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
