<?
$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;

$uid = Yii::$app->user->id;
?>
<aside class="main-sidebar">

	<section class="sidebar">
		<ul class="sidebar-menu">

			<li <?= ($controller == 'employee/dashboard' && $action == 'index') ? ' class="active"' :'';?>>
				<a href="/">
					<i class="fa fa-dashboard"></i> <span>Главная</span>
				</a>
			</li>

			<li class="treeview<?= (in_array($controller,[
				'employee/incomes',
				'employee/expenses',
				'employee/transferences',
			]))
				? '  active'
				: '';?>">

				<a href="#">
					<i class="fa fa-rub"></i><span>Деньги</span>
				</a>
				<ul class="treeview-menu">

					<li <?= ($controller == 'employee/incomes') ? ' class="active"' :'';?>>
						<a href="<?= Yii::$app->getUrlManager()->createUrl(['employee/incomes/index']);?>">
							<i class="fa fa-plus-square"></i> <span>Приходы</span>
						</a>
					</li>

					<li <?= ($controller == 'employee/expenses') ? ' class="active"' :'';?>>
						<a href="<?= Yii::$app->getUrlManager()->createUrl(['employee/expenses/index']);?>">
							<i class="fa fa-minus-square"></i> <span>Расходы</span>
						</a>
					</li>

				</ul>
			</li>

			<li <?= ($controller == 'employee/reserves') ? ' class="active"' :'';?>>
				<a href="<?= Yii::$app->getUrlManager()->createUrl(['employee/reserves/index']);?>">
					<i class="fa fa-umbrella"></i> <span>Резервы</span>
				</a>
			</li>

			<li class="treeview<?= ($controller == 'employee/statistics')
				? '  active'
				: '';?>">

				<a href="#">
					<i class="fa fa-file-text-o"></i><span>Отчеты</span>
				</a>
				<ul class="treeview-menu">
					<li<?= ($controller == 'employee/statistics' && $action == 'balance') ? ' class="active"' :'';?>>
						<a href="<?= Yii::$app->getUrlManager()->createUrl(['employee/statistics/balance']);?>">
							<i class="fa fa-file-excel-o"></i> <span>Остаток</span>
						</a>
					</li>
					<li<?= ($controller == 'employee/statistics' && $action == 'reserve-balance') ? ' class="active"' :'';?>>
						<a href="<?= Yii::$app->getUrlManager()->createUrl(['employee/statistics/reserve-balance']);?>">
							<i class="fa fa-file-excel-o"></i> <span>Остаток (резерв)</span>
						</a>
					</li>
				</ul>
			</li>
			<li class="treeview<?= ((in_array($controller,[
					'employee/categories',
					'employee/projects',
				])) || in_array($action,['account','profile']))
				? '  active'
				: '';?>">

				<a href="#">
					<i class="fa fa-cogs"></i><span>Настройки</span>
				</a>
				<ul class="treeview-menu">
					<li<?= ($controller == 'employee/projects') ? ' class="active"' :'';?>>
						<a href="<?= Yii::$app->getUrlManager()->createUrl(['employee/projects/index']);?>">
							<i class="fa fa-folder-open"></i><span>Проекты</span>
						</a>
					</li>
					<li<?= ($controller == 'employee/categories') ? ' class="active"' :'';?>>
						<a href="<?= Yii::$app->getUrlManager()->createUrl(['employee/categories/index']);?>">
							<i class="fa fa-bars"></i> <span>Категории операций</span>
						</a>
					</li>
					<li<?= (in_array($action,['account','profile'])) ? ' class="active"' :'';?>>
						<a href="<?= Yii::$app->getUrlManager()->createUrl(['user/settings']);?>">
							<i class="fa fa-user"></i> <span>Профиль</span>
						</a>
					</li>
				</ul>
			</li>

		</ul>
	</section>
</aside>