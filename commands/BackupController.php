<?php

namespace app\commands;
use app\models\Backup;
use app\models\CompanyBackupFile;
use app\models\user\BaseUser;
use yii\console\Controller;
use Yii;
use yii\helpers\ArrayHelper;

class BackupController extends Controller
{
    public function actionIndex()
    {
        $companies = ArrayHelper::getColumn(BaseUser::find()
            ->select(['id'])
            ->where(['role_id' => BaseUser::ROLE_COMPANY])
            ->indexBy('id')
            ->asArray(true)
            ->all(),'id');
        foreach ($companies as $company_id)
        {
            $company_backup_file = CompanyBackupFile::instance();
            $filename = $company_backup_file->createCompanyBackup($company_id);

            $backup = new Backup();
            $backup->setAttributes([
                'company_id' => $company_id,
                'filename' => $filename,
                'datetime' => time(),
            ],false);
            $backup->save();
        }
    }

}