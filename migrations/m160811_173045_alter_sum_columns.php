<?php

use yii\db\Migration;

class m160811_173045_alter_sum_columns extends Migration
{
    public function up()
    {
        $this->alterColumn('incoming','sum',$this->double(2));
        $this->alterColumn('expense','sum',$this->double(2));
        $this->alterColumn('transference','sum',$this->double(2));
        $this->alterColumn('reserve','sum',$this->double(2));
        $this->alterColumn('reserve_history','sum',$this->double(2));
    }

    public function down()
    {
        $this->alterColumn('incoming','sum',$this->double(2));
        $this->alterColumn('expense','sum',$this->double(2));
        $this->alterColumn('transference','sum',$this->double(2));
        $this->alterColumn('reserve','sum',$this->double(2));
        $this->alterColumn('reserve_history','sum',$this->double(2));
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
