<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model \app\models\Category */
/* @var $form yii\widgets\ActiveForm */

$this->registerCss(".btn-100{width:100%}");
?>

<div class="user-table-form">

    <?php $form = ActiveForm::begin([
        'id' => 'add_bill',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'validateOnChange' => true,
        'validationUrl' => ['validate'],
        'options' => [
            'class' => 'action-form'
        ],
    ]); ?>

    <div id="fields">

        <div class="users row">

            <div class="col-lg-12 col-md-12">
                <?= $form->field($model, 'title')->textInput([
                'maxlength' => true,
                'placeholder' => $model->getAttributeLabel( 'title' )
                ])->label(false)?>
            </div>
            <div class="col-lg-12 col-md-12">
                <?= $form->field($model, 'checking_account')->textInput([
                    'maxlength' => true,
                    'placeholder' => $model->getAttributeLabel( 'checking_account' )
                ])->label(false)?>
                <?= $form->field($model, 'company_id')->hiddenInput(['value' => $model->company_id])->label(false)?>
            </div>

            <div class="col-lg-12 col-md-12">
                <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Применить', ['class' => 'btn btn-100  btn-success']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
