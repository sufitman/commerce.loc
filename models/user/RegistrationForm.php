<?php

namespace app\models\user;

use Yii;
/**
 * @property int role_id
 */
class RegistrationForm extends \dektrium\user\models\RegistrationForm
{
    public $first_name;

    public $second_name;

    public $role_id;

    public $company_name;

    public $password_confirmation;

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['create'][] = 'first_name';
        $scenarios['create'][] = 'second_name';
        $scenarios['update'][] = 'first_name';
        $scenarios['update'][] = 'second_name';
        $scenarios['register'][] = 'first_name';
        $scenarios['register'][] = 'second_name';
        $scenarios['register'][] = 'role_id';
        $scenarios['register'][] = 'company_name';
        return $scenarios;
    }

    public function attributeLabels()
    {
        $initial_labels = parent::attributeLabels();
        $initial_labels['username'] = 'Логин';
        return $initial_labels + [
            'first_name' => 'Имя',
            'second_name' => 'Фамилия',
            'password_confirmation' => 'Подтверждение пароля',
            'company_name' => 'Название компании',
        ];
    }

    public function rules()
    {
        $rules = parent::rules();
        $rules['company_nameRequired'] = ['company_name', 'required'];
        $rules['first_nameRequired'] = ['first_name', 'required'];
        $rules['second_nameRequired'] = ['second_name', 'required'];
        $rules['role_id_default'] = ['role_id', 'default', 'value' => BaseUser::ROLE_COMPANY];
        $rules['password_confirmationRequired'] = ['password_confirmation', 'required'];
        $rules['password_confirmationMatch'] = [
            'password_confirmation',
            'compare',
            'compareAttribute' => 'password',
            'message' => 'Не верный пароль'
        ];
        return $rules;
    }

}