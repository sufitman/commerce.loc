<?php

use yii\db\Migration;

class m160730_085653_base extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->addColumn('user','company_id',$this->integer());
        $this->addColumn('user','company_name',$this->string());
        $this->addColumn('user','role_id',$this->smallInteger(1));
        $this->addColumn('user','first_name',$this->string(45));
        $this->addColumn('user','second_name',$this->string(45));
        $this->addColumn('user','last_sign_in',$this->integer());
        $this->createIndex('role_id_idx','user','role_id');
        $this->createIndex('company_id','user','company_id');

        $this->createTable('bill', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'company_id' => $this->integer(),
            'current_sum' => $this->double(),
        ], $tableOptions);
        $this->createIndex('company_id_idx','bill','company_id');
        $this->addForeignKey('users_bills','bill','company_id','user','id','CASCADE');

        $this->createTable('project', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'description' => $this->string(),
            'company_id' => $this->integer(),
        ], $tableOptions);
        $this->createIndex('company_id_idx','project','company_id');
        $this->addForeignKey('company_projects','project','company_id','user','id','CASCADE');

        $this->createTable('transference', [
            'id' => $this->primaryKey(),
            'from_bill' => $this->integer(),
            'to_bill' => $this->integer(),
            'sum' => $this->double(),
            'comment' => $this->string(),
            'datetime' => $this->integer(),
        ], $tableOptions);
        $this->createIndex('from_bill_idx','transference','from_bill');
        $this->createIndex('to_bill_idx','transference','to_bill');
        $this->addForeignKey('bills_from_transferences','transference','from_bill','bill','id','CASCADE');
        $this->addForeignKey('bills_to_transferences','transference','to_bill','bill','id','CASCADE');

        $this->createTable('incoming', [
            'id'          => $this->primaryKey(),
            'company_id'  => $this->integer(),
            'project_id'  => $this->integer(),
            'created_by'  => $this->integer(),
            'from'        => $this->string(),
            'sum'         => $this->double(),
            'to_bill'     => $this->integer(),
            'description' => $this->string(),
            'comment'     => $this->string(),
            'datetime'    => $this->integer(),
        ], $tableOptions);
        $this->createIndex('users_incomings_idx','incoming','company_id');
        $this->createIndex('projects_incomings_idx','incoming','project_id');
        $this->createIndex('bills_incomings_idx','incoming','to_bill');
        $this->createIndex('employeess_incomings_idx','incoming','created_by');
        $this->addForeignKey('users_incomings','incoming','company_id','user','id','CASCADE');
        $this->addForeignKey('projects_incomings','incoming','project_id','project','id','CASCADE');
        $this->addForeignKey('bills_incomings','incoming','to_bill','bill','id','CASCADE');
        $this->addForeignKey('employees_incomings','incoming','created_by','user','id','CASCADE');

        $this->createTable('expense', [
            'id'          => $this->primaryKey(),
            'company_id'  => $this->integer(),
            'project_id'  => $this->integer(),
            'created_by'  => $this->integer(),
            'from_bill'        => $this->integer(),
            'category_id'     => $this->integer(),
            'sum'         => $this->double(),
            'description' => $this->string(),
            'datetime'    => $this->integer(),
        ], $tableOptions);
        $this->createIndex('users_expenses_idx','expense','company_id');
        $this->createIndex('projects_expenses_idx','expense','project_id');
        $this->createIndex('bills_expenses_idx','expense','from_bill');
        $this->createIndex('employees_expenses_idx','expense','created_by');
        $this->createIndex('categories_expenses_idx','expense','category_id');
        $this->addForeignKey('users_expenses','expense','company_id','user','id','CASCADE');
        $this->addForeignKey('projects_expenses','expense','project_id','project','id','CASCADE');
        $this->addForeignKey('bills_expenses','expense','from_bill','bill','id','CASCADE');
        $this->addForeignKey('employees_expenses','expense','created_by','user','id','CASCADE');

        $this->createTable('reserve', [
            'id'          => $this->primaryKey(),
            'company_id'  => $this->integer(),
            'project_id'  => $this->integer(),
            'created_by'  => $this->integer(),
            'sum'         => $this->double(),
            'description' => $this->string(),
            'datetime'    => $this->integer(),
            'deleted'     => $this->smallInteger(1)->defaultValue(0),
        ], $tableOptions);
        $this->createIndex('users_reserves_idx','reserve','company_id');
        $this->createIndex('projects_reserves_idx','reserve','project_id');
        $this->createIndex('employees_reserves_idx','reserve','created_by');
        $this->addForeignKey('users_reserves','reserve','company_id','user','id','CASCADE');
        $this->addForeignKey('projects_reserves','reserve','project_id','project','id','CASCADE');
        $this->addForeignKey('employees_reserves','reserve','created_by','user','id','CASCADE');

        $this->createTable('backup', [
            'id'          => $this->primaryKey(),
            'company_id'  => $this->integer(),
            'filename' => $this->string(),
            'datetime'    => $this->integer(),
        ], $tableOptions);
        $this->createIndex('company_backups_idx','reserve','company_id');
        $this->addForeignKey('company_backups','reserve','company_id','user','id','CASCADE');

    }

    public function down()
    {
        $this->dropIndex('company_backups_idx','reserve');
        $this->dropForeignKey('company_backups','reserve');
        $this->dropTable('backup');

        $this->dropForeignKey('users_reserves','reserve');
        $this->dropForeignKey('projects_reserves','reserve');
        $this->dropForeignKey('employees_reserves','reserve');
        $this->dropIndex('users_reserves_idx','reserve');
        $this->dropIndex('projects_reserves_idx','reserve');
        $this->dropIndex('employees_reserves_idx','reserve');
        $this->dropTable('reserve');

        $this->dropForeignKey('users_expenses','expense');
        $this->dropForeignKey('projects_expenses','expense');
        $this->dropForeignKey('bills_expenses','expense');
        $this->dropForeignKey('employees_expenses','expense');
        $this->dropIndex('users_expenses_idx','expense');
        $this->dropIndex('projects_expenses_idx','expense');
        $this->dropIndex('bills_expenses_idx','expense');
        $this->dropIndex('employees_expenses_idx','expense');
        $this->dropIndex('categories_expenses_idx','expense');
        $this->dropTable('expense');

        $this->dropForeignKey('users_incomings','incoming');
        $this->dropForeignKey('projects_incomings','incoming');
        $this->dropForeignKey('bills_incomings','incoming');
        $this->dropForeignKey('employees_incomings','incoming');
        $this->dropIndex('users_incomings_idx','incoming');
        $this->dropIndex('projects_incomings_idx','incoming');
        $this->dropIndex('bills_incomings_idx','incoming');
        $this->dropIndex('employeess_incomings_idx','incoming');
        $this->dropTable('incoming');

        $this->dropForeignKey('bills_from_transferences','transference');
        $this->dropForeignKey('bills_to_transferences','transference');
        $this->dropIndex('from_bill_idx','transference');
        $this->dropIndex('to_bill_idx','transference');
        $this->dropTable('transference');

        $this->dropForeignKey('company_projects','project');
        $this->dropIndex('company_id_idx','project');
        $this->dropTable('project');

        $this->dropForeignKey('users_bills','bill');
        $this->dropIndex('company_id_idx','bill');
        $this->dropTable('bill');

        $this->dropIndex('role_id_idx','user');
        $this->dropIndex('company_id','user');

        $this->dropColumn('user','company_id');
        $this->dropColumn('user','company_name');
        $this->dropColumn('user','role_id');
        $this->dropColumn('user','first_name');
        $this->dropColumn('user','second_name');
        $this->dropColumn('user','last_sign_in');

    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
