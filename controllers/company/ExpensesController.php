<?php

namespace app\controllers\company;

use app\controllers\SiteController;
use app\models\Bill;
use app\models\Category;
use app\models\Expense;
use app\models\Project;
use app\models\search\ExpenseSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\user\BaseUser;

class ExpensesController extends SiteController
{
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete'      => ['post'],
                    'edit'      => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'         => true,
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isCompany();
                        },
                    ],
                    [
                        'allow'         => true,
                        'actions'       => [
                            'delete',
                            'edit',
                        ],
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isCompany() ||  $identity->isAdmin();
                        },
                    ],
                    [
                        'allow'   => true,
                        'actions' => [],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ExpenseSearch();
        $params = Yii::$app->request->queryParams;
        $uid = Yii::$app->user->id;
        $dataProvider = $searchModel
            ->search($params);

        $projects = ArrayHelper::getColumn(Project::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('id')
            ->all(),'title');

        $bills = ArrayHelper::getColumn(Bill::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('id')
            ->all(),'title');

        $categories = ArrayHelper::getColumn(Category::find()
            ->select(['id','title'])
            ->where(['and',['and',['company_id' => $uid],['deleted' => false]],['type' => Category::TYPE_EXPENSE]])
            ->asArray(true)
            ->indexBy('id')
            ->all(),'title');

        $expense = new Expense();

        $request = Yii::$app->request;

        $loaded = $expense->load($request->post());

        if($loaded && $request->isAjax) {
            $expense->company_id = $expense->created_by = $uid;
            $expense->save();

            Yii::$app->end();
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'expense' => $expense,
            'projects' => $projects,
            'categories' => $categories,
            'bills' => $bills,
        ]);
    }

    public function actionDelete($id,$go_back = false)
    {
        $this->_findModel(Expense::className(),$id)->delete();

        return $this->redirect([!$go_back ? 'index' : $go_back]);
    }

    public function actionEdit($id)
    {
        $model = $this->_findModel(Expense::className(),$id);

        $uid = Yii::$app->user->id;
        $projects = ArrayHelper::getColumn(Project::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('id')
            ->all(),'title');

        $bills = ArrayHelper::getColumn(Bill::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('id')
            ->all(),'title');

        $categories = ArrayHelper::getColumn(Category::find()
            ->select(['id','title'])
            ->where(['and',['and',['company_id' => $uid],['deleted' => false]],['type' => Category::TYPE_EXPENSE]])
            ->asArray(true)
            ->indexBy('id')
            ->all(),'title');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'projects' => $projects,
                'bills' => $bills,
                'categories' => $categories,
            ]);
        }
    }

    public function actionValidate(){
        return $this->_validate(Expense::className());
    }
}