<?php

namespace app\controllers\employee;

use app\controllers\SiteController;
use app\models\Bill;
use app\models\Category;
use app\models\Expense;
use app\models\Project;
use app\models\Reserve;
use app\models\ReserveHistory;
use app\models\search\ReserveHistorySearch;
use app\models\search\ReserveSearch;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use app\models\user\BaseUser;

class ReservesController extends SiteController
{
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'         => true,
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isEmployee();
                        },
                    ],
                    [
                        'allow'         => true,
                        'actions'       => [
                            'index',
                        ],
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isEmployee() ||  $identity->isAdmin();
                        },
                    ],
                    [
                        'allow'   => true,
                        'actions' => [],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        if (Yii::$app->request->post('hasEditable')) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            $model = Reserve::findOne(Yii::$app->request->post('editableKey'));

            $output = $message = '';

            $posted = current($_POST['Reserve']);
            $post = ['Reserve' => $posted];

            if ($model->load($post)) {
                if (isset($posted['project_id'])){
                    $model->project_id = (int) $posted['project_id'];
                    $output = $model->project->title;
                }

                if (isset($posted['description'])){
                    $model->description = $posted['description'];
                    $output = $model->description;
                }


                if(!$model->save()){
                    $message = 'Не удалось обновить';
                }
            }
            $out = Json::encode(['reserve_title' => $output, 'message' => $message]);
            return $out;
        }

        $searchModel = new ReserveSearch();
        $params = Yii::$app->request->queryParams;
        $uid = Yii::$app->user->identity->company_id;
        $dataProvider = $searchModel
            ->search($params,true);

        $projects = ArrayHelper::getColumn(Project::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('id')
            ->all(),'title');

        $reserve = new Reserve();

        $request = Yii::$app->request;

        $loaded = $reserve->load($request->post());

        if($loaded && $request->isAjax) {

            $reserve->company_id = $uid;
            $reserve->created_by = Yii::$app->user->id;
            if ($reserve->save() && !empty($reserve->id)) {
                $first_reserve_action = new ReserveHistory();
                $first_reserve_action->setAttributes([
                    'reserve_id' => $reserve->id,
                    'sum' => $reserve->sum,
                    'datetime' => $reserve->datetime,
                ],false);
                $first_reserve_action->save();
            }
            Yii::$app->end();
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'reserve' => $reserve,
            'projects' => $projects,
        ]);
    }

    public function actionDelete($id)
    {
        $reserve = $this->_findModel(Reserve::className(),$id);
        if($reserve->created_by != Yii::$app->user->id)
            throw new NotFoundHttpException();

        $reserve->deleted = true;
        $reserve->save();

        return $this->redirect(['index']);
    }

    public function actionAdd($reserve_id)
    {
        if(!Yii::$app->request->isAjax)
            throw new MethodNotAllowedHttpException();

        $this->layout = 'ajax';
        $model = new ReserveHistory();

        $request = Yii::$app->request;
        $history = $request->get('history',false);

        $loaded = $model->load($request->post());

        if($loaded) {
            if(!$model->reserve_id) $model->reserve_id = $history ? $history : $reserve_id;
            $model->datetime = time();
            if ($model->save()) {
                $reserve = $this->_findModel(Reserve::className(),$model->reserve_id);
                $reserve->sum += $model->sum;
                $reserve->save();
            }
            Yii::$app->end();
        }

        $searchModel = new ReserveHistorySearch();
        if($history)
            $searchModel->reserve_id = $history;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(!$history){
            return $this->renderAjax('add-history-action', [
                'model' => $model,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }else{
            return $this->renderAjax('history-grid', [
                'dataProvider' => $dataProvider,
            ]);
        }

    }

    public function actionDebit($reserve_id)
    {
        if(!Yii::$app->request->isAjax)
            throw new MethodNotAllowedHttpException();

        $uid = Yii::$app->user->identity->company_id;
        $this->layout = 'ajax';
        $model = new ReserveHistory();
        $model->setScenario('debit');
        $request = Yii::$app->request;
        $history = $request->get('history',false);

        $loaded = $model->load($request->post());


        $categories = ArrayHelper::getColumn(Category::find()
            ->select(['id','title'])
            ->where(['and',['and',['company_id' => $uid],['deleted' => false]],['type' => Category::TYPE_EXPENSE]])
            ->asArray(true)
            ->indexBy('id')
            ->all(),'title');

        $bills = ArrayHelper::getColumn(Bill::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('id')
            ->all(),'title');

        if($loaded) {
            if(!$model->reserve_id) $model->reserve_id = $history ? $history : (int) $reserve_id;
            $sum = $model->sum;
            $model->sum *= -1;
            $model->datetime = time();

            if ($model->save()) {
                $reserve = $this->_findModel(Reserve::className(),$model->reserve_id);
                $reserve->sum += $model->sum;
                $reserve->save();

                $expense = new Expense();
                $expense->setScenario('reserve');
                $expense->setAttributes([
                    'company_id' => $reserve->company_id,
                    'project_id' => $reserve->project_id,
                    'created_by' => Yii::$app->user->id,
                    'from_bill' => $model->from_bill,
                    'from_reserve' => $model->reserve_id,
                    'category_id' => $model->category_id,
                    'sum' => $sum,
                    'description' => !empty($model->description) ? $model->description : $reserve->description,
                    'datetime' => $model->datetime,
                ],false);
                $expense->save();
            }
            Yii::$app->end();
        }

        $searchModel = new ReserveHistorySearch();
        if($history)
            $searchModel->reserve_id = $history;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if(!$history){
            return $this->renderAjax('debit-history-action', [
                'model' => $model,
                'categories' => $categories,
                'bills' => $bills,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }else{
            return $this->renderAjax('history-grid', [
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionHistory($reserve_id)
    {
        if(!Yii::$app->request->isAjax)
            throw new MethodNotAllowedHttpException();
        $this->layout = 'ajax';
        $searchModel = new ReserveHistorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('reserve-history', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionValidateReserveHistory(){
        return $this->_validate(ReserveHistory::className(),false,false,Yii::$app->request->getQueryParam('scenario',false));
    }

    public function actionValidate(){
        return $this->_validate(Reserve::className());
    }

}