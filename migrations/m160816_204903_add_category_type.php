<?php

use yii\db\Migration;

class m160816_204903_add_category_type extends Migration
{
    public function up()
    {
        $this->addColumn('category','type',$this->smallInteger(1)->defaultValue(0));
        $this->addColumn('incoming','category_id',$this->integer());
        $this->addForeignKey('incoming_category','incoming','category_id','category','id','RESTRICT');
    }

    public function down()
    {
        $this->dropForeignKey('incoming_category','incoming');
        $this->dropColumn('incoming','category_id');
        $this->dropColumn('category','type');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
