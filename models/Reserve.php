<?php

namespace app\models;

use Yii;
use app\models\user\BaseUser as User;
/**
 * This is the model class for table "reserve".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $project_id
 * @property integer $created_by
 * @property double $sum
 * @property string $description
 * @property integer $datetime
 *
 * @property User $company
 * @property User $createdBy
 * @property Project $project
 * @property User $company0
 */
class Reserve extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reserve';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'project_id', 'created_by', 'datetime'], 'integer'],
            [[ 'project_id', 'datetime','sum', 'description'], 'required'],
            [['sum'],  'match', 'pattern'=>'/^[0-9]{1,12}([.,][0-9]{0,2})?$/', 'message' => 'Значение должно быть числовым, с точностью до сотых'],
            [['description'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->sum = str_replace(",", ".", $this->sum);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'project_id' => 'Проект',
            'created_by' => 'Создан',
            'sum' => 'Сумма',
            'description' => 'Описание',
            'datetime' => 'Время',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(User::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany0()
    {
        return $this->hasOne(User::className(), ['id' => 'company_id']);
    }
}
