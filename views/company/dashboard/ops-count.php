<?php
use dosamigos\chartjs\ChartJs;
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 10.08.2016
 * Time: 22:28
 * @var $chart array
 */
?>

<div class="box-header with-border">Число операций за неделю</div>
<div class="box-body">
    <?= ChartJs::widget([
        'id' => 'b',
        'type' => 'bar',
        'options' => [
            'height' => 300,
            'width' => 651,
        ],
        'data' => [
            'labels' => $chart['days'],
            'datasets' => [
                [
                    'label' => 'Число приходов',
                    'backgroundColor' => "rgba(0,141,76,0.5)",
                    'borderColor' => "rgba(0,141,76,1)",
                    'data' => $chart['incomes_count']
                ],
                [
                    'label' => 'Число расходов',
                    'backgroundColor' => "rgba(216,27,96,0.5)",
                    'borderColor' => "rgba(216,27,96,1)",
                    'data' => $chart['expenses_count']
                ],
            ]
        ]
    ]);
    ?>
</div>
<div class="box-footer text-center">
    <a class="chart-count" href="<?= Yii::$app->urlManager->createUrl([
        'company/dashboard/get-operations-count',
        'period' => 7
    ])?>">За неделю</a> / <a class="chart-count" href="<?= Yii::$app->urlManager->createUrl([
        'company/dashboard/get-operations-count',
        'period' => 30
    ])?>">За месяц</a> / <a class="chart-count" href="<?= Yii::$app->urlManager->createUrl([
        'company/dashboard/get-operations-count',
        'period' => 183
    ])?>">За пол года</a>
</div>
