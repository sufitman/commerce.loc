<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transference".
 *
 * @property integer $id
 * @property integer $from_bill
 * @property integer $to_bill
 * @property double $sum
 * @property string $comment
 * @property integer $datetime
 *
 * @property Bill $toBill
 * @property Bill $fromBill
 */
class Transference extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transference';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_bill', 'to_bill', 'datetime'], 'integer'],
            [['from_bill', 'to_bill', 'datetime','sum'], 'required'],
            ['from_bill', 'compare', 'compareAttribute' => 'to_bill', 'operator' => '!=', 'message' => "Необходимо указать разные счета"],
            ['to_bill', 'compare', 'compareAttribute' => 'from_bill', 'operator' => '!=', 'message' => "Необходимо указать разные счета"],
            [['sum'],  'match', 'pattern'=>'/^[0-9]{1,12}([.,][0-9]{0,2})?$/', 'message' => 'Значение должно быть числовым, с точностью до сотых'],
            [['comment'], 'string', 'max' => 255],
            [['to_bill'], 'exist', 'skipOnError' => true, 'targetClass' => Bill::className(), 'targetAttribute' => ['to_bill' => 'id']],
            [['from_bill'], 'exist', 'skipOnError' => true, 'targetClass' => Bill::className(), 'targetAttribute' => ['from_bill' => 'id']],
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->sum = str_replace(",", ".", $this->sum);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from_bill' => 'Со счета',
            'to_bill' => 'На счет',
            'sum' => 'Сумма',
            'comment' => 'Комментарий',
            'datetime' => 'Время',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToBill()
    {
        return $this->hasOne(Bill::className(), ['id' => 'to_bill']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromBill()
    {
        return $this->hasOne(Bill::className(), ['id' => 'from_bill']);
    }
}
