<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $view */

/* @var $searchModel app\models\search\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/* @var $transference \app\models\Transference */
/* @var $bills array */

$this->title = 'Переводы';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss('
	.action-link,.action-link:hover{text-decoration:none;}
');

$this->registerJs('
    $(document).on("beforeSubmit",".action-form",function(e){
        var form = $(this);
        var url = form.attr("action");
        $.ajax({
            url : url,
            data : form.serialize(),
            method : "post",
            success : function(data){
                $.pjax.reload({container:"#trnsfrcs", async:true});
                form[0].reset();
            }
        });
    }).on("submit",".action-form",function(e){
        e.preventDefault();
    });
')
?>
<div class="user-table-index">

    <div class="panel panel-primary">
        <div class="panel-heading">Внутренние переводы
            <?=Html::a(
                '<span class="btn btn-xs btn-default pull-right">
                    <i class="fa fa-plus"></i>
                        <span class="btn-title">&nbsp;Добавить перевод</span>
                </span>&nbsp;',
                '#transference-modal',
                [
                    'title' => 'Добавить перевод',
                    'class' => 'transference_modal',
                    'data-toggle' => 'modal',
                    'data-target' => '#transference-modal',
                    'data-pjax' => '0',
                ]
            );?>
        </div>
        <div class="panel-body">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <?php Pjax::begin(['id' => 'trnsfrcs']); ?>    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'attribute' => 'from_bill',
                                'content' => function($data){
                                    return $data->fromBill->title;
                                }
                            ],
                            [
                                'attribute' => 'to_bill',
                                'content' => function($data){
                                    return $data->toBill->title;
                                }
                            ],
                            [
                                'attribute' => 'sum',
                                'content' => function($data){
                                    return Yii::$app->formatter->asDecimal($data->sum,2);
                                }
                            ],
                            'comment',
                            [
                                'attribute' => 'datetime',
                                'format' =>  ['date', 'dd.MM.Y HH:mm'],
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'buttons' => [
                                    'edit' => function ($url, $model) {
                                        $custom_url = Yii::$app->getUrlManager()->createUrl([
                                            'company/transferences/edit','id' => $model['id']
                                        ]);
                                        return Html::a(
                                            '<span class="btn btn-xs btn-default action-btn">
                                                <i class="fa fa-edit"></i>
                                                    <span class="btn-title">&nbsp;Редактировать</span>
                                            </span>&nbsp;',
                                            $custom_url,
                                            ['title' => 'Редактировать', 'class' => 'action-link', 'data-method' => 'post']
                                        );
                                    },
                                    'delete' => function ($url, $model) {
                                        $custom_url = Yii::$app->getUrlManager()->createUrl([
                                            'company/transferences/delete','id' => $model['id']
                                        ]);
                                        return Html::a(
                                            '<span class="btn btn-xs btn-default action-btn">
                                                <i class="fa fa-trash"></i>
                                                    <span class="btn-title">&nbsp;Удалить</span>
                                            </span>&nbsp;',
                                            $custom_url,
                                            ['title' => 'Удалить', 'class' => 'action-link', 'data-method' => 'post']
                                        );
                                    },
                                ],
                                'template' => '{edit}{delete}',
                            ],
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    'id' => 'transference-modal',
    'class' => 'modal',
    'size'=>'modal-md',
    'footer' => ' &emsp;'
]); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <h4>Перевести средства</h4>
                <hr>
                <?= $this->render('_transference_form', [
                    'model' => $transference,
                    'bills' => $bills,
                ]); ?>
            </div>
        </div>
    </div>
<?php Modal::end()?>