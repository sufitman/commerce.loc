<?php

namespace app\models\user;

/**
 * @property int role_id
 */
class Coach extends BaseUser
{
    public function beforeValidate()
    {
        $this->role_id = parent::ROLE_EMPLOYEE;

        return parent::beforeValidate();
    }
}
