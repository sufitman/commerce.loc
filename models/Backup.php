<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "backup".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $filename
 * @property integer $datetime
 */
class Backup extends \yii\db\ActiveRecord
{
    const BACKUP_COUNT = 3;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'backup';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'datetime'], 'integer'],
            [['filename'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'filename' => 'Название файла',
            'datetime' => 'Время создания',
        ];
    }

    public function beforeSave($insert)
    {
        $backups =  ArrayHelper::getColumn(Backup::find()
            ->select(['id','datetime'])
            ->where(['company_id' => $this->company_id])
            ->indexBy('datetime')
            ->orderBy('datetime')
            ->asArray(true)
            ->all(),'id');

        $backups_count = count($backups);

        if($backups_count > (Backup::BACKUP_COUNT - 1)){
            $backups_to_delete = array_slice($backups, 0, $backups_count - (Backup::BACKUP_COUNT - 1));

            foreach ($backups_to_delete as $backup){
                $backup_to_delete = Backup::findOne((int)$backup);

                CompanyBackupFile::instance()->deleteBackupFile($backup_to_delete->filename);

                $backup_to_delete->delete();
            }
        }
        
        return parent::beforeSave($insert);
    }
}
