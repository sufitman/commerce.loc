<?php

namespace app\controllers\employee;

use app\controllers\SiteController;
use app\models\Category;
use app\models\search\CategorySearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\user\BaseUser;

class CategoriesController extends SiteController
{
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'         => true,
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isCompany();
                        },
                    ],
                    [
                        'allow'   => true,
                        'actions' => [],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new CategorySearch();
        $params = Yii::$app->request->queryParams;
        $uid = Yii::$app->user->identity->company_id;
        $dataProvider = $searchModel
            ->search($params);

        $category = new Category();

        $request = Yii::$app->request;

        $loaded = $category->load($request->post());

        if($loaded && $request->isAjax) {
            $category->company_id = $uid;
            $category->save();
            Yii::$app->end();
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'category' => $category,
        ]);
    }

    public function actionValidate(){
        return $this->_validate(Category::className());
    }
}