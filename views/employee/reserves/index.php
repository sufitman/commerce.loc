<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use \yii\bootstrap\Modal;
use kartik\editable\Editable;

/* @var $this yii\web\View */
/* @var $view */

/* @var $searchModel app\models\search\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/* @var $reserve \app\models\Reserve */
/* @var $projects array  */

$this->title = 'Резервы';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss('
	.action-link,.action-link:hover{text-decoration:none;}
');


?>
<div class="user-table-index">

    <div class="panel panel-primary">
        <div class="panel-heading">Список резервов
            <?=Html::a(
                '<span class="btn btn-xs btn-default pull-right">
                    <i class="fa fa-plus"></i>
                        <span class="btn-title">&nbsp;Добавить резерв</span>
                </span>&nbsp;',
                '#reserve-modal',
                [
                    'title' => 'Добавить резерв',
                    'class' => 'reserve_modal',
                    'data-toggle' => 'modal',
                    'data-target' => '#reserve-modal',
                    'data-pjax' => '0',
                ]
            );?>
        </div>
        <div class="panel-body">

            <div class="col-lg-12">
                <div class="table-responsive">

                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        //'filterModel' => $searchModel,
                        'pjax' => true,
                        'pjaxSettings' => [
                            'options' => [
                                'id' => 'rsrvs',
                            ],
                        ],
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'class'=>'kartik\grid\EditableColumn',
                                'attribute' => 'project_id',
                                'editableOptions'=> [
                                    'inputType'=> Editable::INPUT_DROPDOWN_LIST ,
                                    'data' => $projects,
                                    'displayValueConfig' => $projects,
                                ],
                                'content' => function($data){
                                    return $data->project->title;
                                }
                            ],
                            [
                                'attribute' => 'sum',
                                'content' => function($data){
                                    return Yii::$app->formatter->asDecimal($data->sum,2);
                                }
                            ],
                            [
                                'class'=>'kartik\grid\EditableColumn',
                                'attribute' => 'description',
                                'editableOptions'=> [
                                    'inputType'=> Editable::INPUT_TEXTAREA
                                ],
                                'content' => function($data){
                                    return $data->description;
                                }
                            ],
                            [
                                'attribute' => 'datetime',
                                'format' =>  ['date', 'dd.MM.Y HH:mm'],
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'buttons' => [
                                    'add_history_action' => function ($url, $model) {
                                        $custom_url = Yii::$app->getUrlManager()->createUrl([
                                            'employee/reserves/add','reserve_id' => $model['id']
                                        ]);
                                        return Html::a(
                                            '<span class="btn btn-xs btn-success action-btn">
                                                <i class="fa fa-plus"></i>
                                                    <span class="btn-title">&nbsp;Добавить средства</span>
                                            </span>&nbsp;',
                                            $custom_url,
                                            [
                                                'title' => 'Добавить средства',
                                                'class' => 'action-link add_history_action',
                                                'data-reserve_id' => $model['id'],
                                                'data-toggle' => 'modal',
                                                'data-target' => '#add-modal',
                                                'data-pjax' => '0',
                                            ]
                                        );
                                    },
                                    'debit_history_action' => function ($url, $model) {
                                        $custom_url = Yii::$app->getUrlManager()->createUrl([
                                            'employee/reserves/debit','reserve_id' => $model['id']
                                        ]);
                                        return Html::a(
                                            '<span class="btn btn-xs btn-default action-btn">
                                                <i class="fa fa-minus"></i>
                                                    <span class="btn-title">&nbsp;Списать средства</span>
                                            </span>&nbsp;',
                                            $custom_url,
                                            [
                                                'title' => 'Списать средства',
                                                'class' => 'action-link debit_history_action',
                                                'data-reserve_id' => $model['id'],
                                                'data-toggle' => 'modal',
                                                'data-target' => '#debit-modal',
                                                'data-pjax' => '0',
                                            ]
                                        );
                                    },
                                    'reserve_history' => function ($url, $model) {
                                        $custom_url = Yii::$app->getUrlManager()->createUrl([
                                            'employee/reserves/history','reserve_id' => $model['id']
                                        ]);
                                        return Html::a(
                                            '<span class="btn btn-xs btn-default action-btn">
                                                <i class="fa fa-bars"></i>
                                                    <span class="btn-title">&nbsp;История</span>
                                            </span>&nbsp;',
                                            $custom_url,
                                            [
                                                'title' => 'История',
                                                'class' => 'action-link reserve_history',
                                                'data-reserve_id' => $model['id'],
                                                'data-toggle' => 'modal',
                                                'data-target' => '#history-modal',
                                                'data-pjax' => '0',
                                            ]
                                        );
                                    },
                                    'delete' => function ($url, $model) {
                                        $custom_url = Yii::$app->getUrlManager()->createUrl([
                                            'employee/reserves/delete','id' => $model['id']
                                        ]);
                                        return Html::a(
                                            '<span class="btn btn-xs btn-default action-btn">
                                                <i class="fa fa-trash"></i>
                                                    <span class="btn-title">&nbsp;Удалить</span>
                                            </span>&nbsp;',
                                            $custom_url,
                                            ['title' => 'Удалить', 'class' => 'action-link', 'data-method' => 'post']
                                        );
                                    },
                                ],
                                'template' => '{add_history_action}{debit_history_action}{reserve_history}{delete}',
                            ],
                        ],
                    ]); ?>

                    <?php $this->registerJs('
                        var add_modal_loaded = debit_modal_loaded = false;
                        
                        $(document).on("click", ".reserve_history,.debit_history_action,.add_history_action",function(e) {
                            var reserve_id = $(this).attr("data-reserve_id");
                            var url = $(this).attr("href");
                            if($(this).hasClass("add_history_action")){
                                if(!add_modal_loaded){
                                    $("#add-modal .modal-body").load(url,function(){
                                        $("#add_reserve_history").val(reserve_id);
                                    });
                                    add_modal_loaded = true;
                                }else{
                                    $("#add_reserve_history").val(reserve_id);
                                    url = setGetParameter(url,"reserve_id",reserve_id);
                                    $(".add-form").attr("action",url);
                                    $("#add-modal .history").load(url + "&history=" + reserve_id);
                                }
                                
                            }
                            if($(this).hasClass("debit_history_action")){
                                if(!debit_modal_loaded){
                                    $("#debit-modal .modal-body").load(url,function(){
                                        $("#debit_reserve_history").val(reserve_id);
                                        console.log(reserve_id);
                                    });
                                    debit_modal_loaded = true;
                                }else{
                                    $("#debit_reserve_history").val(reserve_id);
                                    url = setGetParameter(url,"reserve_id",reserve_id)
                                    $(".debit-form").attr("action",url);
                                    $("#debit-modal .history").load(url + "&history=" + reserve_id);
                                }
                                
                            }  
                            if($(this).hasClass("reserve_history")) 
                                $("#history-modal .modal-body").load(url);
                        });
                        
                        $(document).on("click", ".pagination a", function(e){
                            e.preventDefault();
                            var container = $(this).closest(".modal");
                            
                           
                                var reserve_id = (container.attr("id") == "add-modal") 
                                    ? $("#add_reserve_history").val() 
                                    : $("#debit_reserve_history").val();
                            
                            
                            console.log(reserve_id);
                            
                            container.find(".history").load($(this).attr("href") + "&history=" + reserve_id);
                        });
                        
                        
                        
                        $(document).on("beforeSubmit",".action-form",function(e){
                            var form = $(this);
                            var url = form.attr("action");
                            $.ajax({
                                url : url,
                                data : form.serialize(),
                                method : "post",
                                success : function(data){
                                    if(form.hasClass("add-form")){
                                        var reserve_id = $("#add_reserve_history").val();
                                        $("#add-modal .history").load(url + "&history=" + reserve_id);
                                    }
                                    
                                    if(form.hasClass("debit-form")){
                                        var reserve_id = $("#debit_reserve_history").val();
                                        $("#debit-modal .history").load(url + "&history=" + reserve_id);
                                    }
                                    
                                    $.pjax.reload({container:"#rsrvs", async:true});
                                    form[0].reset();
                                }
                            });
                        }).on("submit",".action-form",function(e){
                            e.preventDefault();
                        });
                        
                        function setGetParameter(url, paramName, paramValue)
                        {
                            var hash = location.hash;
                            url = url.replace(hash, \'\');
                            if (url.indexOf(paramName + "=") >= 0)
                            {
                                var prefix = url.substring(0, url.indexOf(paramName));
                                var suffix = url.substring(url.indexOf(paramName));
                                suffix = suffix.substring(suffix.indexOf("=") + 1);
                                suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")) : "";
                                url = prefix + paramName + "=" + paramValue + suffix;
                            }
                            else
                            {
                            if (url.indexOf("?") < 0)
                                url += "?" + paramName + "=" + paramValue;
                            else
                                url += "&" + paramName + "=" + paramValue;
                            }
                            return url + hash;
                        }
                        /*$.ajaxSetup({ cache: false });*/
                    ');?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    'id' => 'add-modal',
    'class' => 'modal',
    'size'=>'modal-md',
]); ?>

<?php Modal::end()?>
<?php Modal::begin([
    'id' => 'debit-modal',
    'class' => 'modal',
    'header' => ' ',
    'size'=>'modal-md'
]); ?>

<?php Modal::end()?>

<?php Modal::begin([
    'id' => 'history-modal',
    'class' => 'modal',
    'size'=>'modal-md',
]); ?>

<?php Modal::end()?>

<?php Modal::begin([
    'id' => 'reserve-modal',
    'class' => 'modal',
    'size'=>'modal-md',
    'footer' => ' &emsp;'
]); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <h4>Добавить новый резерв</h4>
                <?= $this->render('_new_reserve_form', [
                    'model' => $reserve,
                    'projects' => $projects,
                ]); ?>
            </div>
        </div>
    </div>
<?php Modal::end()?>