<?php

namespace app\controllers\company;

use app\controllers\SiteController;
use app\models\Bill;
use app\models\search\TransferenceSearch;
use app\models\Transference;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use app\models\user\BaseUser;

class TransferencesController extends SiteController
{
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'edit'   => ['post'],
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'         => true,
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isCompany();
                        },
                    ],
                    [
                        'allow'         => true,
                        'actions'       => [
                            'delete',
                            'edit',
                        ],
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isCompany() ||  $identity->isAdmin();
                        },
                    ],
                    [
                        'allow'   => true,
                        'actions' => [],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new TransferenceSearch();
        $params = Yii::$app->request->queryParams;

        $dataProvider = $searchModel
            ->search($params);

        $bills = ArrayHelper::getColumn(Bill::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => Yii::$app->user->id],['deleted' => false]])
            ->asArray(true)
            ->indexBy('id')
            ->all(),'title');

        $transference = new Transference();

        $request = Yii::$app->request;

        $loaded = $transference->load($request->post());

        if($loaded && $request->isAjax) {
            $transference->save();

            Yii::$app->end();
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'transference' => $transference,
            'bills' => $bills,
        ]);
    }

    public function actionEdit($id)
    {
        $model = $this->_findModel(Transference::className(),$id,false,['toBill','fromBill']);

        $uid = Yii::$app->user->id;

        $bills = ArrayHelper::getColumn(Bill::find()
            ->select(['id','title'])
            ->where(['and',['company_id' => $uid],['deleted' => false]])
            ->asArray(true)
            ->indexBy('id')
            ->all(),'title');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'bills' => $bills,
            ]);
        }
    }

    public function actionDelete($id)
    {
        $this->_findModel(Transference::className(),$id,false,['toBill','fromBill'])->delete();

        return $this->redirect(['index']);
    }

    public function actionValidate(){
        return $this->_validate(Transference::className());
    }




}