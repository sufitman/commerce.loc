<?php

use kartik\grid\GridView;
use app\models\Summary;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $view */

/* @var $searchModel app\models\search\ProjectBalanceSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $show_all bool */

$this->title = 'Отчеты';
$this->params['breadcrumbs'][] = $this->title;

$filter = Yii::$app->request->getQueryParam('ProjectSearch',false);
$start_time = $filter ? $filter['start_time'] : false;
$end_time = $filter ? $filter['end_time'] : false;

$count = count($dataProvider->models);

$url = ['company/statistics/projects'];
$columns = [
    [
        'class' => 'yii\grid\SerialColumn',
        'footer'=> 'Итого',
    ],
    [
        'attribute' => 'title',
        'content' => function($data){
            return Html::a($data->title,['company/balance','BalanceSearch' => [
                'project_title' => $data->title,
            ]], ['data-method' => 'POST']);
        }
    ],
    'description',
    [
        'label' => 'Приход',
        'format' => 'raw',
        'content' => function($data){
            $income = Yii::$app->formatter->asDecimal($data->total_incomes
                ? $data->total_incomes
                : 0,2);
            return Html::tag('span',$income,['class' => 'text-green']);
        },
        'footer'=> Yii::$app->formatter
                ->asDecimal(Summary::getTotalBalanceSum($dataProvider->models,'total_incomes'),2) . ' руб.',
    ],

    [
        'label' => 'Расход',
        'format' => 'raw',
        'content' => function($data){

            $expense = Yii::$app->formatter->asDecimal($data->total_expenses
                ? $data->total_expenses
                : 0,2);
            return Html::tag('span',$expense,['class' => 'text-red']);

        },
        'footer'=> Yii::$app->formatter
                ->asDecimal(Summary::getTotalBalanceSum($dataProvider->models,'total_expenses'),2) . ' руб.',
    ],
    [
        'label' => 'Прибыль',
        'content' => function($data){
            return Yii::$app->formatter->asDecimal((int)$data->balance,2);
        },
        'footer'=> Yii::$app->formatter
                ->asDecimal(Summary::getTotalBalanceSum($dataProvider->models,'balance'),2) . ' руб.',
    ],
    [
        'label' => 'Рентабельность',
        'content' => function($data){
            return Yii::$app->formatter->asDecimal((int)$data->profit,2);
        },
        'footer' => ($count)
            ? Yii::$app->formatter
                ->asDecimal(Summary::getTotalBalanceSum($dataProvider->models,'profit')/$count,2) . ' %'
            : Yii::$app->formatter->asDecimal(0,2),
    ],
];

?>

<div class="user-table-index">

    <div class="panel panel-primary">
        <div class="panel-heading">Отчет по проектам</div>
        <div class="panel-body">
            <?= $this->render('_search', [
                'model' => $searchModel,
            ]); ?>
            <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    //'filterModel' => $searchModel,
                    'columns' => $columns,
                    'showFooter' => TRUE,
                    'footerRowOptions' => ['style'=>'font-weight:bold;text-decoration: underline;'],
                    'panel' => [
                        'before' => Html::a('<i class="fa fa-angle-double-' . ($show_all ? 'up' : 'down') . '"></i> ' .
                                ($show_all ? 'Показать первую страницу' : 'Показать все'), $url, [
                                'class' => 'btn btn-default',
                                'title' => $show_all ? 'Показать первую страницу' : 'Показать все'
                            ]) . \kartik\export\ExportMenu::widget([
                                'dataProvider' => $dataProvider,
                                'columns' => $columns,
                                'target' => \kartik\export\ExportMenu::TARGET_BLANK,
                                'showConfirmAlert' => false,
                                'dropdownOptions' => [
                                    'label' => 'Экспорт'
                                ]
                            ]) . '<div class="pull-right">' . ($start_time || $end_time
                            ? 'В период ' . ($start_time ? 'c ' . date('d.m.Y H:i', $start_time) : '') .
                            ($end_time ? ' до ' . date('d.m.Y H:i', $end_time) : '')
                            : '') . '</div>',

                    ],
                    'pjax'=>true,
                    'toolbar' => [
                        [
                            'class' => 'pull-left',
                            'content' => ''
                        ],
                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>
