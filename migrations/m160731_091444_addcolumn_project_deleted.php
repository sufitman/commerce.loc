<?php

use yii\db\Migration;

class m160731_091444_addcolumn_project_deleted extends Migration
{
    public function up()
    {
        $this->dropColumn('bill','current_sum');
        $this->addColumn('project','deleted',$this->smallInteger(1)->defaultValue(0));
        $this->createIndex('projects_deleted_idx','project','deleted');
        $this->addColumn('bill','deleted',$this->smallInteger(1)->defaultValue(0));
        $this->createIndex('bills_deleted_idx','bill','deleted');
    }

    public function down()
    {
        $this->addColumn('bill','current_sum',$this->double());
        $this->dropIndex('bills_deleted_idx','bill');
        $this->dropColumn('bill','deleted');
        $this->dropIndex('projects_deleted_idx','project');
        $this->dropColumn('project','deleted');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
