<?php

namespace app\models;

use Yii;
use app\models\user\BaseUser as User;

/**
 * This is the model class for table "category".
 *
 * @property integer $id
 * @property string $title
 * @property integer $company_id
 * @property integer $type
 *
 * @property User $company
 */
class Category extends \yii\db\ActiveRecord
{
    const TYPE_EXPENSE = 0;

    const TYPE_INCOME = 1;

    public static function getTypes()
    {
        return [
            self::TYPE_EXPENSE => 'Приход',
            self::TYPE_INCOME => 'Расход',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['title','type'], 'required'],
            [['company_id','title'], 'unique','targetAttribute' => ['title'],'message' => 'Категория с таким названием уже существует'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'company_id' => 'Компания',
            'type' => 'Тип операции',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(User::className(), ['id' => 'company_id']);
    }
}
