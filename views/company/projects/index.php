<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use app\models\Summary;

/* @var $this yii\web\View */
/* @var $view */

/* @var $searchModel app\models\search\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/* @var $project \app\models\Project */

$this->title = 'Проекты';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss('
	.action-link,.action-link:hover{text-decoration:none;}
');
$this->registerJs('
    $(document).on("beforeSubmit",".action-form",function(e){
        var form = $(this);
        var url = form.attr("action");
        $.ajax({
            url : url,
            data : form.serialize(),
            method : "post",
            success : function(data){
                $.pjax.reload({container:"#prjcts", async:true});
                form[0].reset();
            }
        });
    }).on("submit",".action-form",function(e){
        e.preventDefault();
    });
')


?>

<div class="user-table-index">

    <div class="panel panel-primary">
        <div class="panel-heading">Проекты компании
            <?=Html::a(
                '<span class="btn btn-xs btn-default pull-right">
                    <i class="fa fa-plus"></i>
                        <span class="btn-title">&nbsp;Добавить проект</span>
                </span>&nbsp;',
                '#project-modal',
                [
                    'title' => 'Добавить проект',
                    'class' => 'project_modal',
                    'data-toggle' => 'modal',
                    'data-target' => '#project-modal',
                    'data-pjax' => '0',
                ]
            );?>
        </div>
        <div class="panel-body">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <?php Pjax::begin(['id' => 'prjcts']); ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        //'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'title',
                            'description',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'buttons' => [
                                    'delete' => function ($url, $model) {
                                        $custom_url = Yii::$app->getUrlManager()->createUrl([
                                            'company/projects/delete','id' => $model['id']
                                        ]);
                                        return Html::a(
                                            '<span class="btn btn-xs btn-default action-btn">
                                                <i class="fa fa-trash"></i>
                                                    <span class="btn-title">&nbsp;Удалить</span>
                                            </span>&nbsp;',
                                            $custom_url,
                                            ['title' => 'Удалить', 'class' => 'action-link', 'data-method' => 'post']
                                        );
                                    },
                                    'edit' => function ($url, $model) {
                                        $custom_url = Yii::$app->getUrlManager()->createUrl([
                                            'company/projects/edit','id' => $model['id']
                                        ]);
                                        return Html::a(
                                            '<span class="btn btn-xs btn-default action-btn">
                                                <i class="fa fa-edit"></i>
                                                    <span class="btn-title">&nbsp;Редактировать</span>
                                            </span>&nbsp;',
                                            $custom_url,
                                            ['title' => 'Редактировать', 'class' => 'action-link edit_project', 'data-method' => 'post']
                                        );
                                    },
                                ],
                                'template' => '{delete}{edit}',
                            ],
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    'id' => 'project-modal',
    'class' => 'modal',
    'size'=>'modal-sm',
    'footer' => ' &emsp;'
]); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <h4>Добавить новый проект</h4>
                <hr>
                <?= $this->render('_project_form', [
                    'model' => $project,
                ]); ?>
            </div>
        </div>
    </div>
<?php Modal::end()?>