<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model */

?>
<div class="projects-search row">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12">
        <?= $form->field($model, 'start_time',['options' => ['class' => 'time_picker']])->widget(DateControl::className(), [
            'ajaxConversion' => false,
            'options' => [
                'options' => ['placeholder' => 'Время с']
            ],
            'type' => DateControl::FORMAT_DATE,
            //'autoWidget' => false,
            'displayFormat' => 'php:D, d-M-Y',
            'saveFormat' => 'php:U'
        ])->label(false) ?>
    </div>

    <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12">
        <?= $form->field($model, 'end_time',['options' => ['class' => 'time_picker']])->widget(DateControl::className(), [
            'ajaxConversion' => false,
            'options' => [
                'options' => ['placeholder' => 'Время по']
            ],
            'type' => DateControl::FORMAT_DATE,
            //'autoWidget' => false,
            'displayFormat' => 'php:D, d-M-Y',
            'saveFormat' => 'php:U'
        ])->label(false) ?>

    </div>
</div>

<div class="row">

    <div class="col-lg-12">
        <div class="form-group text-right">
            <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Сброс', ['index'],['class' => 'btn btn-default']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>