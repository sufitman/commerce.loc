<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $view */

/* @var $searchModel app\models\search\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/* @var $category \app\models\Category */

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss('
	.action-link,.action-link:hover{text-decoration:none;}
');

$this->registerJs('
    $(document).on("beforeSubmit",".action-form",function(e){
        var form = $(this);
        var url = form.attr("action");
        $.ajax({
            url : url,
            data : form.serialize(),
            method : "post",
            success : function(data){
                $.pjax.reload({container:"#ctgrs", async:true});
                form[0].reset();
            }
        });
    }).on("submit",".action-form",function(e){
        e.preventDefault();
    });
');
$type = Yii::$app->request->getQueryParam('type',false);
?>
<div class="user-table-index">

    <div class="panel panel-primary">
        <div class="panel-heading">Категории операций
            <?=Html::a(
                '<span class="btn btn-xs btn-default pull-right">
                    <i class="fa fa-plus"></i>
                        <span class="btn-title">&nbsp;Добавить категорию</span>
                </span>&nbsp;',
                '#category-modal',
                [
                    'title' => 'Добавить категорию',
                    'class' => 'category_modal',
                    'data-toggle' => 'modal',
                    'data-target' => '#category-modal',
                    'data-pjax' => '0',
                ]
            );?>
        </div>
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li role="type" <?= !$type || $type != 1 ? 'class="active"' : '';?>>
                    <a href="<?= Yii::$app->urlManager->createUrl(['company/categories'])?>">Категории расходов</a>
                </li>
                <li role="type" <?= $type == 1 ? 'class="active"' : '';?>>
                    <a href="<?= Yii::$app->urlManager->createUrl(['company/categories', 'type' => 1])?>">Категории приходов</a>
                </li>
            </ul>
            <hr>

            <div class="col-lg-12">
                <div class="table-responsive">
                    <?php Pjax::begin(['id' => 'ctgrs']); ?>    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'title',
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'buttons' => [
                                    'delete' => function ($url, $model) {
                                        $type = Yii::$app->request->getQueryParam('type',false);
                                        $custom_url = Yii::$app->getUrlManager()->createUrl([
                                            'company/categories/delete','id' => $model['id'], 'type' => $type ? $type : ''
                                        ]);
                                        return Html::a(
                                            '<span class="btn btn-xs btn-default action-btn">
                                                <i class="fa fa-trash"></i>
                                                    <span class="btn-title">&nbsp;Удалить</span>
                                            </span>&nbsp;',
                                            $custom_url,
                                            ['title' => 'Удалить', 'class' => 'action-link', 'data-method' => 'post']
                                        );
                                    },
                                    'edit' => function ($url, $model) {
                                        $custom_url = Yii::$app->getUrlManager()->createUrl([
                                            'company/categories/edit','id' => $model['id']
                                        ]);
                                        return Html::a(
                                            '<span class="btn btn-xs btn-default action-btn">
                                                <i class="fa fa-edit"></i>
                                                    <span class="btn-title">&nbsp;Редактировать</span>
                                            </span>&nbsp;',
                                            $custom_url,
                                            ['title' => 'Редактировать', 'class' => 'action-link edit_category', 'data-method' => 'post']
                                        );
                                    },
                                ],
                                'template' => '{delete}{edit}',
                            ],
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    'id' => 'category-modal',
    'class' => 'modal',
    'size'=>'modal-sm',
    'footer' => ' &emsp;'
]); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <h4>Добавить новую категорию</h4><br>
                <?= $this->render('_category_form', [
                    'model' => $category,
                ]); ?>
            </div>
        </div>
    </div>
<?php Modal::end()?>