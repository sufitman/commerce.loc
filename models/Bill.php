<?php

namespace app\models;

use Yii;
use app\models\user\BaseUser as User;

/**
 * This is the model class for table "bill".
 *
 * @property integer $id
 * @property string $title
 * @property integer $company_id
 * @property integer $checking_account
 *
 * @property User $company
 * @property Expense[] $expenses
 * @property Incoming[] $incomings
 * @property Transference[] $transferences
 * @property Transference[] $transferences0
 */
class Bill extends \yii\db\ActiveRecord
{
    public $bill_incomes;

    public $bill_expenses;

    public $bill_transferences;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bill';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id','checking_account'], 'integer'],
            [['checking_account'], 'unique', 'on' => 'create'],
            [['company_id','title'], 'unique','targetAttribute' => ['title'],'message' => 'Счет с таким названием уже существует','on' => 'create'],
            [['title'], 'string', 'max' => 255],
            [['title'], 'required'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'company_id' => 'Компания',
            'checking_account' => 'Рассчетный счет',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(User::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenses()
    {
        return $this->hasMany(Expense::className(), ['from_bill' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomings()
    {
        return $this->hasMany(Incoming::className(), ['to_bill' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransferences()
    {
        return $this->hasMany(Transference::className(), ['to_bill' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransferences0()
    {
        return $this->hasMany(Transference::className(), ['from_bill' => 'id']);
    }
}
