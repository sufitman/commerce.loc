<?php

/* @var $this yii\web\View */
/* @var $stat array */
/* @var $chart array */

rmrevin\yii\ionicon\AssetBundle::register($this);

$this->registerJs("
   
    $(document).on('click','.chart-sum',function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        $('#ops-sum').load(url);
    }).on('click','.chart-count',function(e){
        e.preventDefault();
        var url = $(this).attr('href');
        $('#ops-count').load(url);
    });
",$this::POS_END);
$this->registerCss("
    .box{min-height:77px}
    .small-box{overflow:hidden}
");
$this->title = 'Главная';
?>

<div class="row">
    <div class="col-lg-6 col-md-12">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h3><?=$stat['expenses_sum']
                        ? Yii::$app->formatter->asDecimal($stat['expenses_sum'],2)
                        : '0,00'?> <i class="fa fa-rub"></i> / <?=$stat['day_expenses_sum']
                        ? Yii::$app->formatter->asDecimal($stat['day_expenses_sum'],2)
                        : '0,00'?> <i class="fa fa-rub"></i></h3>

                <p>Расходы (общие / за день)</p>
            </div>
            <div class="icon">
                <i class="ion ion-bag"></i>
            </div>
            <a href="<?= Yii::$app->urlManager->createUrl(['company/expenses'])?>" class="small-box-footer">Подробнее <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-6 col-md-12">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h3><?=$stat['incomes_sum']
                        ? Yii::$app->formatter->asDecimal($stat['incomes_sum'],2)
                        : '0,00' ?> <i class="fa fa-rub"></i> / <?= $stat['day_incomes_sum']
                        ? Yii::$app->formatter->asDecimal($stat['day_incomes_sum'],2)
                        : '0,00'?> <i class="fa fa-rub"></i></h3>

                <p>Приходы (общие / за день)</p>
            </div>
            <div class="icon">
                <i class="ion ion-cash"></i>
            </div>
            <a href="<?= Yii::$app->urlManager->createUrl(['company/incomings'])?>" class="small-box-footer">Подробнее <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-6 col-md-12">
        <!-- small box -->
        <div class="small-box bg-orange">
            <div class="inner">
                <h3><?=$stat['reserves_sum']
                        ? Yii::$app->formatter->asDecimal($stat['reserves_sum'],2)
                        : '0,00' ?> <i class="fa fa-rub"></i></h3>

                <p>Резервы</p>
            </div>
            <div class="icon">
                <i class="ion ion-umbrella"></i>
            </div>
            <a href="<?= Yii::$app->urlManager->createUrl(['company/reserves'])?>" class="small-box-footer">Подробнее <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>

    <div class="col-lg-6 col-md-12">
        <!-- small box -->
        <div class="small-box bg-blue">
            <div class="inner">
                <h3><?=$stat['balance']
                        ? Yii::$app->formatter->asDecimal($stat['balance'],2)
                        : '0,00'?> <i class="fa fa-rub"></i></h3>

                <p>Баланс</p>
            </div>
            <div class="icon">
                <i class="ion ion-<?= ($stat['balance'] > 0) ? 'happy' : 'sad'?>-outline"></i>
            </div>
            <a href="<?= Yii::$app->urlManager->createUrl(['company/balance'])?>" class="small-box-footer">Подробнее <i class="fa fa-arrow-circle-right"></i></a>
        </div>
    </div>
</div>
