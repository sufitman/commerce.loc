<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;


/* @var $this yii\web\View */
/* @var $view */

/* @var $searchModel app\models\search\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/* @var $incoming \app\models\Incoming */
/* @var $projects array  */
/* @var $bills array  */
/* @var $categories array  */

$this->title = 'Приходы';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss('
	.action-link,.action-link:hover{text-decoration:none;}
');

$this->registerJs('
    $(document).on("beforeSubmit",".action-form",function(e){
        var form = $(this);
        var url = form.attr("action");
        $.ajax({
            url : url,
            data : form.serialize(),
            method : "post",
            success : function(data){
                $.pjax.reload({container:"#incms", async:true});
                form[0].reset();
            }
        });
    }).on("submit",".action-form",function(e){
        e.preventDefault();
    });
')
?>

<div class="user-table-index">

    <div class="panel panel-primary">
        <div class="panel-heading">Список приходов
            <?=Html::a(
                '<span class="btn btn-xs btn-default pull-right">
                    <i class="fa fa-plus"></i>
                        <span class="btn-title">&nbsp;Добавить приход</span>
                </span>&nbsp;',
                '#incoming-modal',
                [
                    'title' => 'Добавить приход',
                    'class' => 'incoming_modal',
                    'data-toggle' => 'modal',
                    'data-target' => '#incoming-modal',
                    'data-pjax' => '0',
                ]
            );?>
        </div>
        <div class="panel-body">

            <div class="col-lg-12">
                <div class="table-responsive">
                    <?php Pjax::begin(['id' => 'incms']); ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        //'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'attribute' => 'project_id',
                                'content' => function($data){
                                    return $data->project->title;
                                }
                            ],
                            [
                                'attribute' => 'created_by',
                                'content' => function($data){
                                    $creator = $data->createdBy;
                                    return $creator->first_name . ' ' . $creator->second_name;
                                }
                            ],
                            [
                                'attribute' => 'category_id',
                                'content' => function($data){
                                    if ($data->category_id){
                                        $category = $data->category;
                                        return $category->title;
                                    }
                                    return '';
                                }
                            ],
                            [
                                'attribute' => 'sum',
                                'content' => function($data){
                                    return Yii::$app->formatter->asDecimal($data->sum,2);
                                }
                            ],
                            [
                                'attribute' => 'to_bill',
                                'content' => function($data){
                                    return $data->toBill->title;
                                }
                            ],
                            'description',
                            'comment',
                            [
                                'attribute' => 'datetime',
                                'format' =>  ['date', 'dd.MM.Y HH:mm'],
                            ],
                            [
                                'class' => 'yii\grid\ActionColumn',
                                'buttons' => [
                                    'edit' => function ($url, $model) {
                                        $custom_url = Yii::$app->getUrlManager()->createUrl([
                                            'company/incomes/edit','id' => $model['id']
                                        ]);
                                        return Html::a(
                                            '<span class="btn btn-xs btn-default action-btn">
                                                <i class="fa fa-edit"></i>
                                                    <span class="btn-title">&nbsp;Редактировать</span>
                                            </span>&nbsp;',
                                            $custom_url,
                                            ['title' => 'Редактировать ', 'class' => 'action-link', 'data-method' => 'post']
                                        );
                                    },
                                    'delete' => function ($url, $model) {
                                        $custom_url = Yii::$app->getUrlManager()->createUrl([
                                            'company/incomes/delete','id' => $model['id']
                                        ]);
                                        return Html::a(
                                            '<span class="btn btn-xs btn-default action-btn">
                                                <i class="fa fa-trash"></i>
                                                    <span class="btn-title">&nbsp;Удалить</span>
                                            </span>&nbsp;',
                                            $custom_url,
                                            ['title' => 'Удалить', 'class' => 'action-link', 'data-method' => 'post']
                                        );
                                    },
                                ],
                                'template' => '{edit}{delete}',
                            ],
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>

                </div>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    'id' => 'incoming-modal',
    'class' => 'modal',
    'size'=>'modal-md',
    'footer' => ' &emsp;'
]); ?>
    <div class="modal-body">
        <div class="row">
            <div class="col-lg-12">
                <h4>Добавить новый приход</h4>
                <?= $this->render('_incoming_form', [
                    'model' => $incoming,
                    'projects' => $projects,
                    'bills' => $bills,
                    'categories' => $categories,
                ]); ?>
            </div>
        </div>
    </div>
<?php Modal::end()?>