<?php
/**
 * Created by PhpStorm.
 * User: Тимур
 * Date: 10.08.2016
 * Time: 15:34
 * @var $this yii\web\View */
/* @var $projects_balances array */
?>


<div class="box-header with-border">
    <h3 class="box-title">Баланс по проектам</h3>

    <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
        </button>
        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
    </div>
</div>
<!-- /.box-header -->
<div class="box-body">
    <div class="table-responsive">
        <table class="table no-margin">
            <thead>
            <tr>
                <th>Проект</th>
                <th>Баланс</th>
            </tr>
            </thead>
            <tbody>
                <? foreach($projects_balances as $project => $project_balance) : ?>
                    <tr>
                        <td class="text-info"><?=$project?></td>
                        <td><?=Yii::$app->formatter->asDecimal($project_balance,2)?></td>
                    </tr>
                <? endforeach?>

            </tbody>
        </table>
    </div>
    <!-- /.table-responsive -->
</div>
<!-- /.box-body -->
<div class="box-footer clearfix">
    <a href="<?= Yii::$app->urlManager->createUrl('company/projects')?>" class="btn btn-sm btn-info btn-flat pull-left"><i class="fa fa-plus"></i> Добавить проект</a>
    <a href="<?= Yii::$app->urlManager->createUrl('company/statistics/projects')?>" class="btn btn-sm btn-default btn-flat pull-right">Просмотреть детально</a>
</div>
<!-- /.box-footer -->
