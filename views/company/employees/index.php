<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $view */

/* @var $searchModel app\models\search\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/* @var $employee \app\models\user\BaseUser */
/* @var $employees array */
$this->title = 'Сотрудники';
$this->params['breadcrumbs'][] = $this->title;
$this->registerCss('
	.action-link,.action-link:hover{text-decoration:none;}
');

$this->registerJs('
    $(document).on("beforeSubmit",".action-form",function(e){
        var form = $(this);
        var url = form.attr("action");
        $.ajax({
            url : url,
            data : form.serialize(),
            method : "post",
            success : function(data){
                $.pjax.reload({container:"#emps", async:true});
                form[0].reset();
            }
        });
    }).on("submit",".action-form",function(e){
        e.preventDefault();
    });
')
?>
<div class="user-table-index">

    <div class="panel panel-primary">
        <div class="panel-heading">Список сотрудников
            <?=Html::a(
                '<span class="btn btn-xs btn-default pull-right">
                    <i class="fa fa-plus"></i>
                        <span class="btn-title">&nbsp;Добавить сотрудников</span>
                </span>&nbsp;',
                '#employees-modal',
                [
                    'title' => 'Добавить сотрудников',
                    'class' => 'employees_modal',
                    'data-toggle' => 'modal',
                    'data-target' => '#employees-modal',
                    'data-pjax' => '0',
                ]
            );?></div>
        <div class="panel-body">

            <?php  echo $this->render('_search', [
                'model' => $searchModel,
                'employees' => $employees,
            ]); ?>
            <hr>
            <div class="col-lg-12">
                <div class="table-responsive">
                    <?php Pjax::begin([
                        'id' => 'emps'
                    ]); ?>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        //'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            [
                                'attribute' => 'coach.first_name',
                                'label' => 'Имя сотрудника',
                                'content' => function($data){
                                    return $data->first_name . ' ' . $data->second_name;
                                }
                            ],
                            'email:email',
                            [
                                'header' => 'Действия',
                                'class' => 'yii\grid\ActionColumn',
                                'buttons' => [
                                    'delete' => function ($url, $model) {
                                        $custom_url = Yii::$app->getUrlManager()->createUrl([
                                            'company/employees/delete','id' => $model['id']
                                        ]);
                                        return Html::a(
                                            '<span class="btn btn-xs btn-default action-btn">
                                                <i class="fa fa-trash"></i>
                                                    <span class="btn-title">&nbsp;Удалить</span>
                                            </span>&nbsp;',
                                            $custom_url,
                                            ['title' => 'Удалить', 'class' => 'action-link', 'data-method' => 'post']
                                        );
                                    },
                                ],
                                'template' => '{delete}',
                            ],
                        ],
                    ]); ?>
                    <?php Pjax::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Modal::begin([
    'id' => 'employees-modal',
    'class' => 'modal',
    'size'=>'modal-md',
    'footer' => ' &emsp;'
]); ?>
    <div class="modal-body">
        <div class="row">
            <h4 class="pull-left">Добавить сотрудника</h4>
            <p class="text-right">
                <span class="btn btn-success btn-lg add_more"><i class="fa fa-plus"></i></span>
            </p>


            <hr>
            <div class="col-lg-12">
                <?= $this->render('_new_employee_form', [
                    'model' => $employee,
                ]); ?>
            </div>
        </div>
    </div>
<?php Modal::end()?>