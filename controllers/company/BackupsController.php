<?php

namespace app\controllers\company;

use app\controllers\SiteController;
use app\models\Backup;
use app\models\CompanyBackupFile;
use app\models\search\BackupSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use app\models\user\BaseUser;

class BackupsController extends SiteController
{
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'         => true,
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isCompany();
                        },
                    ],
                    [
                        'allow'         => true,
                        'actions'       => [
                            'create',
                            'restore',
                        ],
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isCompany() ||  $identity->isAdmin();
                        },
                    ],
                    [
                        'allow'   => true,
                        'actions' => [],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new BackupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $user = Yii::$app->user->identity;
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'company_id' => isset($user->company_id) ? $user->company_id : $user->id,
        ]);
    }

    public function actionCreate($company_id)
    {
        $user = Yii::$app->user->identity;
        if($user->isAdmin() || in_array($company_id, [$user->id,$user->company_id])){
            $company_backup_file = CompanyBackupFile::instance();
            $filename = $company_backup_file->createCompanyBackup($company_id);
            $backup = new Backup();
            $backup->setAttributes([
                'company_id' => $company_id,
                'filename' => $filename,
                'datetime' => time(),
            ],false);
            $backup->save();
            Yii::$app->end();
        }
        throw new NotFoundHttpException();
    }

    public function actionRestore($id)
    {
        $backup = $this->_findModel(Backup::className(),$id);
        return CompanyBackupFile::instance()->restoreCompanyBackup($backup->filename);
        Yii::$app->end();
    }
}