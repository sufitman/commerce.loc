<?php

namespace app\controllers\company;

use app\controllers\SiteController;
use app\models\search\EmployeeSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use app\models\user\BaseUser;

class EmployeesController extends SiteController
{
    /** @inheritdoc */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'         => true,
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isCompany();
                        },
                    ],
                    [
                        'allow'         => true,
                        'actions'       => [
                            'delete',
                        ],
                        'roles'         => ['@'],
                        'matchCallback' => function () {
                            $identity = Yii::$app->user->identity;
                            /** @var $identity BaseUser */
                            return $identity->isCompany() ||  $identity->isAdmin();
                        },
                    ],
                    [
                        'allow'   => true,
                        'actions' => [],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new EmployeeSearch();
        $params = Yii::$app->request->queryParams;
        $uid = Yii::$app->user->id;
        $dataProvider = $searchModel
            ->search($params);

        $employee = new BaseUser();

        $employees = BaseUser::find()
            ->select([
                'uid' => 'id',
                'creator' => 'user.company_id',
                'email',
                'value' => 'CONCAT(user.first_name, \' \', user.second_name)',
                'label' => 'CONCAT(user.first_name, \' \', user.second_name, \' (\', email, \')\')',
            ]);

        $where = [
            'AND',
            ['role_id' => BaseUser::ROLE_EMPLOYEE],
            ['company_id' => $uid]
        ];
        /** @var $employees \yii\db\ActiveQuery */

        $employees = $employees
            ->where($where)
            ->asArray(true)
            ->all();

        $employee->setScenario('add_employee');
        $request = Yii::$app->request;

        $loaded = $employee->load($request->post());

        if($request->isAjax && $loaded) {
            foreach ($request->post()['BaseUser'] as $user) {
                $first_name = (isset($user['first_name'])) ? $user['first_name'] : false;
                $second_name = (isset($user['second_name'])) ? $user['second_name'] : false;
                $email = (isset($user['email'])) ? $user['email'] : false;

                if ($first_name && $second_name && $email) {
                    $employee::add_new_user(
                        $first_name . ' ' . $second_name,
                        $email,
                        $employee::ROLE_EMPLOYEE,
                        $uid
                    );
                }
            }
            Yii::$app->end();
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'employee' => $employee,
            'employees' => $employees,
        ]);
    }

    public function actionDelete($id)
    {
        $employee = BaseUser::findOne($id);
        if ($employee->company_id != Yii::$app->user->id)
            throw new NotFoundHttpException;

        $employee->blocked_at = time();

        if($employee->save())
            return $this->redirect('index');
        else throw new NotFoundHttpException;
    }

    public function actionValidate()
    {
        return $this->_validate(BaseUser::className(),true,false,'add_employee',true);
    }
}