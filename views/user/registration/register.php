<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\user\BaseUser as User;
/**
 * @var yii\web\View              $this
 * @var dektrium\user\Module      $module
 * @var User                      $model
 */

$this->title = Yii::t('user', 'Sign up');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJS("
$('#role_id').find('label').click(function(e) {
    var this_input = $(this).find('input');
    $('.role-id-btn').prop('checked', false);
    this_input.prop('checked', true);
    $('#role_id').find('label').removeClass('btn-success active');
    $(this).addClass('btn-success active');
});"
);
$this->registerCss('
    .btn-mtn{width:119px;}
');
?>

        <div class="login-box-body">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'id'                     => 'registration-form',
                    'enableAjaxValidation'   => true,
                    'enableClientValidation' => false,
                ]); ?>

                <?= $form->field($model, 'role_id', [
                    'options' => ['value'=> User::ROLE_COMPANY]
                ])->hiddenInput()->label(false)?>

                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'company_name') ?>

                <?= $form->field($model, 'username')->label('Логин') ?>

                <?= $form->field($model, 'first_name')->label('Имя') ?>

                <?= $form->field($model, 'second_name')->label('Фамилия') ?>

                <?php if ($module->enableGeneratingPassword == false): ?>
                    <?= $form->field($model, 'password')->passwordInput() ?>
                    <?= $form->field($model, 'password_confirmation')->passwordInput() ?>
                <?php endif ?>

                <?= Html::submitButton(Yii::t('user', 'Sign up'), ['class' => 'btn btn-success btn-block']) ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <hr>
        <p class="text-center">
            <?= Html::a(Yii::t('user', 'Already registered? Sign in!'), ['/user/security/login']) ?>
        </p>
    </div>
</div>
